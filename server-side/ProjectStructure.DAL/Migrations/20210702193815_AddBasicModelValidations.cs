﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ProjectStructure.DAL.Migrations
{
    public partial class AddBasicModelValidations : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "TeamName",
                table: "Teams",
                type: "nvarchar(40)",
                maxLength: 40,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.AlterColumn<string>(
                name: "TaskName",
                table: "Tasks",
                type: "nvarchar(70)",
                maxLength: 70,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)");

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "ProjectName", "TeamId" },
                values: new object[] { 6, new DateTime(2021, 2, 23, 18, 30, 25, 885, DateTimeKind.Unspecified).AddTicks(7221), new DateTime(2021, 10, 24, 11, 0, 26, 71, DateTimeKind.Unspecified).AddTicks(8336), "Intelligent Frozen Table", 6 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 61, new DateTime(2019, 6, 9, 5, 7, 56, 664, DateTimeKind.Unspecified).AddTicks(4873), new DateTime(2020, 1, 17, 4, 37, 44, 347, DateTimeKind.Unspecified).AddTicks(3434), "The Football Is Good For Training And Recreational Purposes", "Sleek Plastic Chair", 5 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName" },
                values: new object[] { 64, new DateTime(2019, 9, 11, 1, 37, 14, 266, DateTimeKind.Unspecified).AddTicks(3726), new DateTime(2019, 9, 27, 15, 57, 11, 759, DateTimeKind.Unspecified).AddTicks(1204), "The Apollotech B340 is an affordable wireless mouse with reliable connectivity, 12 months battery life and modern design", "Practical Granite Cheese" });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 46, new DateTime(2019, 12, 20, 3, 39, 32, 117, DateTimeKind.Unspecified).AddTicks(4579), new DateTime(2020, 3, 10, 9, 18, 9, 656, DateTimeKind.Unspecified).AddTicks(8655), "New range of formal shirts are designed keeping you in mind. With fits and styling that will make you stand apart", "Intelligent Granite Car", 7 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 31, new DateTime(2018, 9, 27, 22, 55, 26, 802, DateTimeKind.Unspecified).AddTicks(8148), new DateTime(2021, 5, 5, 20, 33, 54, 669, DateTimeKind.Unspecified).AddTicks(9692), "New ABC 13 9370, 13.3, 5th Gen CoreA5-8250U, 8GB RAM, 256GB SSD, power UHD Graphics, OS 10 Home, OS Office A & J 2016", "Handmade Fresh Bacon", 2 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 48, new DateTime(2018, 12, 11, 15, 48, 9, 662, DateTimeKind.Unspecified).AddTicks(2650), new DateTime(2020, 7, 1, 5, 36, 13, 159, DateTimeKind.Unspecified).AddTicks(8289), "The slim & simple Maple Gaming Keyboard from Dev Byte comes with a sleek body and 7- Color RGB LED Back-lighting for smart functionality", "Rustic Concrete Pants", 2 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "ProjectName", "TeamId" },
                values: new object[] { 8, new DateTime(2018, 2, 21, 0, 34, 27, 313, DateTimeKind.Unspecified).AddTicks(8228), new DateTime(2020, 6, 20, 6, 8, 53, 809, DateTimeKind.Unspecified).AddTicks(767), "Handcrafted Rubber Keyboard", 9 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 39, new DateTime(2019, 1, 9, 17, 59, 39, 86, DateTimeKind.Unspecified).AddTicks(9470), new DateTime(2021, 8, 21, 13, 22, 25, 307, DateTimeKind.Unspecified).AddTicks(77), "The Football Is Good For Training And Recreational Purposes", "Sleek Cotton Chicken", 2 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 48, new DateTime(2021, 1, 5, 17, 33, 18, 58, DateTimeKind.Unspecified).AddTicks(9832), new DateTime(2021, 4, 11, 17, 1, 23, 941, DateTimeKind.Unspecified).AddTicks(3810), "The beautiful range of Apple Naturalé that has an exciting mix of natural ingredients. With the Goodness of 100% Natural Ingredients", "Handmade Frozen Pizza", 3 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 17, new DateTime(2019, 7, 1, 16, 3, 48, 12, DateTimeKind.Unspecified).AddTicks(6090), new DateTime(2019, 9, 2, 4, 0, 28, 928, DateTimeKind.Unspecified).AddTicks(5614), "Carbonite web goalkeeper gloves are ergonomically designed to give easy fit", "Incredible Frozen Bike", 2 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 37, new DateTime(2018, 1, 26, 8, 5, 7, 822, DateTimeKind.Unspecified).AddTicks(2460), new DateTime(2021, 6, 10, 10, 27, 9, 685, DateTimeKind.Unspecified).AddTicks(9529), "Andy shoes are designed to keeping in mind durability as well as trends, the most stylish range of shoes & sandals", "Gorgeous Rubber Car", 4 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 36, new DateTime(2019, 3, 6, 23, 3, 54, 470, DateTimeKind.Unspecified).AddTicks(1787), new DateTime(2019, 3, 9, 18, 55, 12, 369, DateTimeKind.Unspecified).AddTicks(4514), "Andy shoes are designed to keeping in mind durability as well as trends, the most stylish range of shoes & sandals", "Refined Concrete Tuna", 7 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 21, new DateTime(2021, 3, 31, 22, 34, 30, 708, DateTimeKind.Unspecified).AddTicks(2037), new DateTime(2021, 7, 21, 15, 53, 44, 25, DateTimeKind.Unspecified).AddTicks(141), "Boston's most advanced compression wear technology increases muscle oxygenation, stabilizes active muscles", "Unbranded Granite Towels", 4 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 49, new DateTime(2019, 6, 14, 13, 7, 17, 57, DateTimeKind.Unspecified).AddTicks(9538), new DateTime(2020, 4, 11, 15, 29, 37, 783, DateTimeKind.Unspecified).AddTicks(6006), "The automobile layout consists of a front-engine design, with transaxle-type transmissions mounted at the rear of the engine and four wheel drive", "Handmade Fresh Salad", 5 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 8, new DateTime(2019, 7, 17, 12, 12, 0, 542, DateTimeKind.Unspecified).AddTicks(8266), new DateTime(2020, 6, 5, 9, 11, 45, 106, DateTimeKind.Unspecified).AddTicks(2658), "Andy shoes are designed to keeping in mind durability as well as trends, the most stylish range of shoes & sandals", "Tasty Rubber Gloves", 4 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 46, new DateTime(2020, 12, 15, 0, 42, 59, 530, DateTimeKind.Unspecified).AddTicks(6286), new DateTime(2021, 8, 3, 16, 37, 14, 148, DateTimeKind.Unspecified).AddTicks(2442), "The Apollotech B340 is an affordable wireless mouse with reliable connectivity, 12 months battery life and modern design", "Unbranded Frozen Car", 9 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 48, new DateTime(2020, 11, 9, 20, 6, 14, 467, DateTimeKind.Unspecified).AddTicks(5893), new DateTime(2021, 10, 11, 12, 20, 6, 437, DateTimeKind.Unspecified).AddTicks(6815), "Boston's most advanced compression wear technology increases muscle oxygenation, stabilizes active muscles", "Generic Concrete Chicken", 8 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 38, new DateTime(2018, 12, 15, 13, 40, 53, 204, DateTimeKind.Unspecified).AddTicks(4158), new DateTime(2020, 1, 22, 6, 12, 36, 661, DateTimeKind.Unspecified).AddTicks(2124), "Carbonite web goalkeeper gloves are ergonomically designed to give easy fit", "Generic Rubber Ball", 9 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 29, new DateTime(2021, 3, 11, 12, 15, 23, 459, DateTimeKind.Unspecified).AddTicks(5195), new DateTime(2021, 8, 20, 8, 18, 3, 442, DateTimeKind.Unspecified).AddTicks(8290), "New range of formal shirts are designed keeping you in mind. With fits and styling that will make you stand apart", "Licensed Plastic Chair", 5 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 27, new DateTime(2021, 1, 26, 6, 5, 8, 703, DateTimeKind.Unspecified).AddTicks(9776), new DateTime(2021, 10, 11, 0, 34, 25, 94, DateTimeKind.Unspecified).AddTicks(8216), "The slim & simple Maple Gaming Keyboard from Dev Byte comes with a sleek body and 7- Color RGB LED Back-lighting for smart functionality", "Small Concrete Bacon", 6 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 63, new DateTime(2020, 7, 30, 15, 25, 3, 962, DateTimeKind.Unspecified).AddTicks(1050), new DateTime(2020, 11, 17, 7, 19, 38, 801, DateTimeKind.Unspecified).AddTicks(9255), "The Apollotech B340 is an affordable wireless mouse with reliable connectivity, 12 months battery life and modern design", "Small Concrete Hat", 7 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 3, new DateTime(2021, 3, 3, 0, 18, 5, 919, DateTimeKind.Unspecified).AddTicks(324), new DateTime(2021, 3, 6, 6, 44, 56, 986, DateTimeKind.Unspecified).AddTicks(9431), "The Nagasaki Lander is the trademarked name of several series of Nagasaki sport bikes, that started with the 1984 ABC800J", "Licensed Cotton Gloves", 9 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 70, new DateTime(2019, 1, 14, 5, 46, 22, 827, DateTimeKind.Unspecified).AddTicks(1580), new DateTime(2019, 8, 18, 9, 32, 25, 161, DateTimeKind.Unspecified).AddTicks(6523), "New ABC 13 9370, 13.3, 5th Gen CoreA5-8250U, 8GB RAM, 256GB SSD, power UHD Graphics, OS 10 Home, OS Office A & J 2016", "Handmade Metal Soap", 1 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName" },
                values: new object[] { 77, new DateTime(2019, 11, 11, 6, 45, 18, 174, DateTimeKind.Unspecified).AddTicks(9945), new DateTime(2020, 1, 4, 10, 40, 3, 469, DateTimeKind.Unspecified).AddTicks(7993), "The Apollotech B340 is an affordable wireless mouse with reliable connectivity, 12 months battery life and modern design", "Incredible Steel Fish" });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 28, new DateTime(2020, 1, 18, 21, 21, 37, 522, DateTimeKind.Unspecified).AddTicks(6934), new DateTime(2020, 4, 5, 15, 24, 48, 291, DateTimeKind.Unspecified).AddTicks(9641), "New ABC 13 9370, 13.3, 5th Gen CoreA5-8250U, 8GB RAM, 256GB SSD, power UHD Graphics, OS 10 Home, OS Office A & J 2016", "Handmade Cotton Towels", 10 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 9, new DateTime(2020, 1, 9, 7, 32, 32, 125, DateTimeKind.Unspecified).AddTicks(7975), new DateTime(2020, 5, 12, 12, 19, 18, 806, DateTimeKind.Unspecified).AddTicks(7530), "Andy shoes are designed to keeping in mind durability as well as trends, the most stylish range of shoes & sandals", "Generic Soft Chair", 5 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 77, new DateTime(2021, 5, 2, 8, 13, 43, 139, DateTimeKind.Unspecified).AddTicks(2803), new DateTime(2021, 12, 2, 9, 36, 6, 551, DateTimeKind.Unspecified).AddTicks(9746), "Carbonite web goalkeeper gloves are ergonomically designed to give easy fit", "Incredible Soft Towels", 7 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { new DateTime(2018, 12, 9, 5, 54, 48, 698, DateTimeKind.Unspecified).AddTicks(3975), new DateTime(2019, 9, 1, 5, 12, 15, 287, DateTimeKind.Unspecified).AddTicks(6110), "Boston's most advanced compression wear technology increases muscle oxygenation, stabilizes active muscles", "Unbranded Plastic Car", 4 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 22, new DateTime(2020, 4, 23, 15, 51, 27, 450, DateTimeKind.Unspecified).AddTicks(3117), new DateTime(2021, 8, 8, 9, 28, 27, 867, DateTimeKind.Unspecified).AddTicks(9761), "The Football Is Good For Training And Recreational Purposes", "Unbranded Concrete Cheese", 7 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 79, new DateTime(2018, 5, 12, 7, 13, 53, 251, DateTimeKind.Unspecified).AddTicks(4554), new DateTime(2021, 7, 7, 22, 37, 15, 474, DateTimeKind.Unspecified).AddTicks(7966), "The automobile layout consists of a front-engine design, with transaxle-type transmissions mounted at the rear of the engine and four wheel drive", "Ergonomic Granite Tuna", 4 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 80, new DateTime(2018, 5, 20, 23, 23, 54, 93, DateTimeKind.Unspecified).AddTicks(8453), new DateTime(2018, 9, 17, 16, 39, 31, 465, DateTimeKind.Unspecified).AddTicks(5573), "New ABC 13 9370, 13.3, 5th Gen CoreA5-8250U, 8GB RAM, 256GB SSD, power UHD Graphics, OS 10 Home, OS Office A & J 2016", "Sleek Granite Computer", 9 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 69, new DateTime(2018, 7, 5, 9, 40, 55, 619, DateTimeKind.Unspecified).AddTicks(4604), new DateTime(2019, 4, 27, 18, 20, 3, 96, DateTimeKind.Unspecified).AddTicks(1491), "Ergonomic executive chair upholstered in bonded black leather and PVC padded seat and back for all-day comfort and support", "Sleek Rubber Ball", 9 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 55, new DateTime(2021, 3, 13, 3, 34, 12, 428, DateTimeKind.Unspecified).AddTicks(4074), new DateTime(2021, 8, 24, 12, 26, 47, 224, DateTimeKind.Unspecified).AddTicks(2412), "The beautiful range of Apple Naturalé that has an exciting mix of natural ingredients. With the Goodness of 100% Natural Ingredients", "Sleek Soft Chair", 10 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 79, new DateTime(2018, 1, 15, 19, 42, 47, 757, DateTimeKind.Unspecified).AddTicks(1434), new DateTime(2020, 3, 27, 6, 16, 27, 978, DateTimeKind.Unspecified).AddTicks(8297), "The Nagasaki Lander is the trademarked name of several series of Nagasaki sport bikes, that started with the 1984 ABC800J", "Rustic Soft Towels", 9 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName" },
                values: new object[] { 22, new DateTime(2018, 11, 17, 19, 7, 17, 28, DateTimeKind.Unspecified).AddTicks(7913), new DateTime(2018, 12, 6, 18, 18, 51, 445, DateTimeKind.Unspecified).AddTicks(687), "The Nagasaki Lander is the trademarked name of several series of Nagasaki sport bikes, that started with the 1984 ABC800J", "Intelligent Wooden Bike" });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "ProjectName", "TeamId" },
                values: new object[] { 32, new DateTime(2021, 4, 12, 10, 12, 18, 40, DateTimeKind.Unspecified).AddTicks(4390), new DateTime(2021, 6, 5, 5, 42, 45, 696, DateTimeKind.Unspecified).AddTicks(1629), "Rustic Metal Computer", 7 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 29, new DateTime(2018, 9, 19, 4, 38, 28, 943, DateTimeKind.Unspecified).AddTicks(7946), new DateTime(2020, 4, 11, 5, 36, 30, 784, DateTimeKind.Unspecified).AddTicks(4679), "New ABC 13 9370, 13.3, 5th Gen CoreA5-8250U, 8GB RAM, 256GB SSD, power UHD Graphics, OS 10 Home, OS Office A & J 2016", "Incredible Cotton Towels", 3 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 27, new DateTime(2018, 5, 26, 11, 59, 19, 247, DateTimeKind.Unspecified).AddTicks(2167), new DateTime(2020, 8, 5, 16, 59, 0, 660, DateTimeKind.Unspecified).AddTicks(7071), "New ABC 13 9370, 13.3, 5th Gen CoreA5-8250U, 8GB RAM, 256GB SSD, power UHD Graphics, OS 10 Home, OS Office A & J 2016", "Generic Granite Car", 7 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 51, new DateTime(2021, 5, 25, 16, 15, 20, 363, DateTimeKind.Unspecified).AddTicks(7711), new DateTime(2021, 8, 14, 14, 41, 56, 859, DateTimeKind.Unspecified).AddTicks(6040), "Boston's most advanced compression wear technology increases muscle oxygenation, stabilizes active muscles", "Awesome Metal Bacon", 8 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 73, new DateTime(2018, 10, 24, 4, 39, 12, 424, DateTimeKind.Unspecified).AddTicks(6845), new DateTime(2020, 4, 24, 8, 56, 41, 876, DateTimeKind.Unspecified).AddTicks(2046), "Ergonomic executive chair upholstered in bonded black leather and PVC padded seat and back for all-day comfort and support", "Awesome Steel Fish", 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 5, 3, 0, 10, 11, 666, DateTimeKind.Unspecified).AddTicks(8547), "Voluptatibus tenetur placeat illum quibusdam eaque sit.", new DateTime(2019, 6, 28, 14, 31, 6, 641, DateTimeKind.Unspecified).AddTicks(4484), "Quia nesciunt natus exercitationem vel.", 9, 28, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 7, 7, 17, 5, 42, 602, DateTimeKind.Unspecified).AddTicks(4124), "Ratione distinctio ut dolores sit culpa vel ut sunt quam.", new DateTime(2021, 8, 12, 7, 32, 30, 190, DateTimeKind.Unspecified).AddTicks(4992), "Eum labore mollitia et.", 61, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId" },
                values: new object[] { new DateTime(2019, 5, 18, 23, 24, 8, 270, DateTimeKind.Unspecified).AddTicks(5518), "Iure occaecati quia labore voluptatum odio magni.", new DateTime(2019, 7, 24, 5, 37, 34, 568, DateTimeKind.Unspecified).AddTicks(9571), "Qui placeat rerum saepe.", 44 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 8, 15, 5, 10, 18, 584, DateTimeKind.Unspecified).AddTicks(4250), "Corrupti assumenda ipsum odio error laudantium mollitia tempore ipsa asperiores eos aliquid voluptatem.", new DateTime(2021, 10, 5, 3, 35, 24, 411, DateTimeKind.Unspecified).AddTicks(3588), "Ratione sequi.", 22, 1, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2020, 5, 8, 20, 28, 39, 468, DateTimeKind.Unspecified).AddTicks(6190), "Nam et quo quis sint odio dolor id.", new DateTime(2020, 5, 11, 11, 17, 49, 966, DateTimeKind.Unspecified).AddTicks(3991), "Eos accusantium aliquam impedit eos.", 37, 26 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2018, 3, 4, 17, 20, 44, 205, DateTimeKind.Unspecified).AddTicks(2337), "Ullam dolor esse est sunt aut illum pariatur iure perferendis.", new DateTime(2018, 3, 27, 11, 18, 14, 375, DateTimeKind.Unspecified).AddTicks(5777), "Et et est adipisci.", 16, 34, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 8, 16, 15, 41, 14, 77, DateTimeKind.Unspecified).AddTicks(3462), "Placeat excepturi tempore excepturi repellat enim et voluptas eius autem inventore tempora quo totam.", new DateTime(2019, 8, 21, 4, 45, 19, 833, DateTimeKind.Unspecified).AddTicks(6068), "Perspiciatis omnis iure quia omnis.", 9, 10 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 3, 4, 6, 0, 19, 575, DateTimeKind.Unspecified).AddTicks(9853), "Nobis ut porro vel neque voluptatem architecto non laboriosam aperiam et reiciendis culpa perferendis.", new DateTime(2021, 3, 5, 20, 55, 31, 341, DateTimeKind.Unspecified).AddTicks(3821), "Autem officia quibusdam aspernatur.", 38, 22, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 6, 15, 12, 59, 8, 118, DateTimeKind.Unspecified).AddTicks(1685), "Est enim perferendis aut voluptatem sapiente dolorem nesciunt quo sunt deserunt ipsam.", new DateTime(2021, 6, 22, 16, 48, 20, 442, DateTimeKind.Unspecified).AddTicks(7676), "Fugiat consectetur.", 10, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 2, 2, 3, 36, 0, 455, DateTimeKind.Unspecified).AddTicks(3001), "Voluptatem iste qui dolore temporibus voluptas molestiae.", new DateTime(2020, 2, 4, 15, 55, 2, 461, DateTimeKind.Unspecified).AddTicks(2108), "Ullam dolores iusto nisi exercitationem.", 9, 4, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 3, 8, 17, 59, 26, 969, DateTimeKind.Unspecified).AddTicks(3694), "Quia officiis consectetur ea doloribus cumque perspiciatis quaerat vitae maxime ex amet provident optio.", new DateTime(2019, 3, 9, 15, 2, 37, 786, DateTimeKind.Unspecified).AddTicks(340), "Voluptatem ab.", 68, 12, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 12, 1, 6, 56, 39, 76, DateTimeKind.Unspecified).AddTicks(7308), "Est quas qui blanditiis accusamus ea et sapiente recusandae.", new DateTime(2019, 12, 21, 4, 35, 12, 927, DateTimeKind.Unspecified).AddTicks(8307), "Similique sint quis.", 72, 14, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2018, 6, 26, 6, 40, 54, 943, DateTimeKind.Unspecified).AddTicks(7000), "Neque quae ut id omnis quibusdam ut.", new DateTime(2018, 7, 21, 14, 38, 55, 481, DateTimeKind.Unspecified).AddTicks(1667), "Error perferendis.", 47, 31, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 1, 12, 1, 34, 8, 782, DateTimeKind.Unspecified).AddTicks(9635), "Atque debitis dicta reprehenderit est voluptas qui magnam velit quis porro nesciunt.", new DateTime(2020, 2, 7, 13, 42, 48, 312, DateTimeKind.Unspecified).AddTicks(366), "Aliquam et aut.", 35, 4, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 13, 3, 14, 18, 264, DateTimeKind.Unspecified).AddTicks(2047), "Aut incidunt animi expedita aliquid recusandae quasi.", new DateTime(2019, 9, 23, 6, 11, 47, 192, DateTimeKind.Unspecified).AddTicks(7333), "Voluptas esse voluptates hic accusantium.", 55, 3, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2020, 2, 11, 3, 28, 38, 461, DateTimeKind.Unspecified).AddTicks(2611), "Nihil unde animi eaque rerum sit magni ut et.", new DateTime(2020, 4, 20, 5, 56, 54, 397, DateTimeKind.Unspecified).AddTicks(9916), "Maxime odio quia in atque.", 69, 7 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 3, 1, 43, 7, 404, DateTimeKind.Unspecified).AddTicks(4748), "Corporis expedita vel a ut animi non nobis.", new DateTime(2021, 7, 10, 22, 45, 38, 771, DateTimeKind.Unspecified).AddTicks(1346), "Veniam dolores labore quod ut.", 48, 13, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 7, 11, 8, 43, 1, 175, DateTimeKind.Unspecified).AddTicks(3979), "Eius delectus sunt necessitatibus a veniam dolorem eum accusantium laudantium rerum.", new DateTime(2021, 7, 14, 9, 4, 56, 537, DateTimeKind.Unspecified).AddTicks(1321), "Rerum molestias adipisci a.", 10, 39 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 3, 7, 3, 28, 23, 45, DateTimeKind.Unspecified).AddTicks(3388), "Sit illo unde nihil recusandae magnam quaerat omnis.", new DateTime(2019, 3, 8, 22, 41, 37, 158, DateTimeKind.Unspecified).AddTicks(8874), "Temporibus vitae tempore esse animi.", 25, 12, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 3, 11, 10, 34, 38, 733, DateTimeKind.Unspecified).AddTicks(4548), "Quas modi quis nesciunt consequatur voluptas molestias deleniti maxime porro fugit quis.", new DateTime(2021, 3, 23, 13, 49, 45, 765, DateTimeKind.Unspecified).AddTicks(1170), "Assumenda at tempore et.", 67, 9 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 2, 23, 6, 10, 32, 237, DateTimeKind.Unspecified).AddTicks(1891), "Est facilis soluta id ut temporibus iure consequuntur itaque voluptate accusamus est.", new DateTime(2020, 3, 8, 17, 7, 14, 604, DateTimeKind.Unspecified).AddTicks(1345), "Eos incidunt culpa.", 44, 4, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 4, 13, 8, 57, 2, 634, DateTimeKind.Unspecified).AddTicks(7447), "Repellat debitis veritatis dolorum nisi recusandae dignissimos.", new DateTime(2021, 7, 20, 2, 6, 6, 323, DateTimeKind.Unspecified).AddTicks(2192), "Nihil est excepturi veritatis.", 43, 33 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 3, 30, 22, 13, 7, 445, DateTimeKind.Unspecified).AddTicks(8574), "Dolorum omnis nihil esse sunt occaecati.", new DateTime(2020, 4, 4, 21, 20, 8, 205, DateTimeKind.Unspecified).AddTicks(6599), "Voluptatum enim itaque quisquam.", 23, 25, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2020, 4, 2, 6, 28, 58, 355, DateTimeKind.Unspecified).AddTicks(3001), "Voluptas dolorem quae adipisci quos mollitia ullam blanditiis commodi non dicta quam enim.", new DateTime(2020, 5, 8, 5, 1, 54, 562, DateTimeKind.Unspecified).AddTicks(9661), "Aut aliquam et praesentium nobis.", 23, 26 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 4, 4, 23, 8, 52, 941, DateTimeKind.Unspecified).AddTicks(1839), "Assumenda asperiores in sed omnis eum.", new DateTime(2021, 9, 1, 12, 46, 28, 924, DateTimeKind.Unspecified).AddTicks(3910), "Culpa corporis ut.", 34, 20, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 1, 24, 15, 52, 55, 537, DateTimeKind.Unspecified).AddTicks(4778), "Quis odit sed sed sequi explicabo quos sint mollitia.", new DateTime(2020, 3, 4, 3, 28, 2, 928, DateTimeKind.Unspecified).AddTicks(7032), "Iste quae.", 28, 26, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 3, 17, 7, 51, 30, 954, DateTimeKind.Unspecified).AddTicks(9521), "Sint veritatis sed quasi rerum dolorum illum.", new DateTime(2020, 3, 25, 13, 36, 29, 230, DateTimeKind.Unspecified).AddTicks(3127), "Aut omnis ullam dolorem minima.", 51, 26, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 12, 20, 5, 12, 23, 112, DateTimeKind.Unspecified).AddTicks(9955), "Eum et ad voluptatum reiciendis voluptates expedita.", new DateTime(2020, 5, 24, 12, 51, 40, 835, DateTimeKind.Unspecified).AddTicks(708), "Asperiores autem laudantium.", 56, 11, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 5, 21, 23, 21, 26, 705, DateTimeKind.Unspecified).AddTicks(7312), "Culpa quae enim voluptatum tempore necessitatibus.", new DateTime(2021, 6, 13, 23, 8, 4, 837, DateTimeKind.Unspecified).AddTicks(2552), "Quibusdam iure assumenda tempora quis.", 72, 30 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2018, 11, 29, 0, 42, 28, 709, DateTimeKind.Unspecified).AddTicks(3971), "Quaerat alias quisquam itaque sequi aut consequatur est illo non earum asperiores possimus omnis.", new DateTime(2018, 12, 1, 13, 36, 0, 584, DateTimeKind.Unspecified).AddTicks(893), "Id quidem commodi.", 71, 35, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 23, 6, 21, 57, 156, DateTimeKind.Unspecified).AddTicks(7147), "Officia labore fugiat optio assumenda eius numquam est ab et aut.", new DateTime(2020, 1, 23, 2, 16, 21, 908, DateTimeKind.Unspecified).AddTicks(6989), "Fugit et.", 72, 6, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 4, 5, 7, 56, 18, 445, DateTimeKind.Unspecified).AddTicks(4003), "Maxime voluptatem sed fuga expedita est ad facere modi et dolorem in dolores optio.", new DateTime(2021, 6, 3, 12, 46, 13, 8, DateTimeKind.Unspecified).AddTicks(9453), "Ut et quam ratione.", 68, 13, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 4, 12, 23, 5, 30, 225, DateTimeKind.Unspecified).AddTicks(4818), "Adipisci iusto amet et et perferendis blanditiis rerum dolores excepturi dolores ratione.", new DateTime(2019, 5, 21, 0, 51, 35, 975, DateTimeKind.Unspecified).AddTicks(1433), "Necessitatibus dolores debitis harum aspernatur.", 22, 23, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 7, 14, 7, 53, 23, 906, DateTimeKind.Unspecified).AddTicks(8928), "Minus sunt excepturi adipisci rerum in nemo inventore dolore.", new DateTime(2020, 1, 6, 19, 19, 46, 692, DateTimeKind.Unspecified).AddTicks(6823), "Ullam consequatur.", 43, 18 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 10, 2, 16, 51, 47, 708, DateTimeKind.Unspecified).AddTicks(2915), "Sed consequatur est saepe maiores ex molestiae deserunt modi autem.", new DateTime(2019, 12, 29, 16, 35, 46, 945, DateTimeKind.Unspecified).AddTicks(4143), "Similique voluptatem sit vel rerum.", 70, 14, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 5, 7, 11, 30, 37, 663, DateTimeKind.Unspecified).AddTicks(9306), "Magnam quasi molestiae fuga facilis a sunt minus tenetur eveniet corrupti.", new DateTime(2021, 9, 4, 6, 34, 49, 571, DateTimeKind.Unspecified).AddTicks(4677), "Impedit libero odio ratione.", 66, 17, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2020, 5, 5, 1, 8, 16, 502, DateTimeKind.Unspecified).AddTicks(1501), "Accusantium est labore autem omnis architecto aut saepe laborum excepturi.", new DateTime(2020, 6, 3, 20, 51, 23, 54, DateTimeKind.Unspecified).AddTicks(2121), "A modi aut.", 60, 15 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 8, 27, 19, 48, 18, 47, DateTimeKind.Unspecified).AddTicks(7423), "Eveniet voluptas nemo corrupti molestiae aut pariatur ea.", new DateTime(2020, 10, 8, 7, 45, 46, 848, DateTimeKind.Unspecified).AddTicks(3952), "Ut nisi in at vero.", 18, 29, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 11, 23, 7, 31, 9, 315, DateTimeKind.Unspecified).AddTicks(3861), "Mollitia quo nisi iure fugit fuga aut culpa voluptatem inventore aperiam et dolorem.", new DateTime(2020, 1, 3, 15, 22, 42, 931, DateTimeKind.Unspecified).AddTicks(1556), "Est eum sunt.", 32, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 5, 19, 15, 3, 31, 281, DateTimeKind.Unspecified).AddTicks(9636), "Quia et nesciunt non repellendus est.", new DateTime(2021, 6, 4, 12, 14, 54, 844, DateTimeKind.Unspecified).AddTicks(5300), "Et voluptatem officiis.", 42, 13 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 41,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 3, 7, 0, 8, 27, 625, DateTimeKind.Unspecified).AddTicks(1394), "Soluta sint nisi beatae autem consequatur explicabo reprehenderit et excepturi totam libero.", new DateTime(2019, 3, 9, 1, 8, 22, 252, DateTimeKind.Unspecified).AddTicks(1426), "Sit molestiae molestias.", 58, 12, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 42,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 3, 10, 10, 55, 52, 460, DateTimeKind.Unspecified).AddTicks(8575), "Nihil voluptatibus mollitia commodi nemo assumenda officia eum et tempore aut omnis aut.", new DateTime(2019, 4, 13, 12, 27, 29, 15, DateTimeKind.Unspecified).AddTicks(785), "Dolores officia a aspernatur amet.", 62, 32 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 43,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2018, 5, 30, 18, 49, 25, 387, DateTimeKind.Unspecified).AddTicks(9014), "Explicabo tempora dolor provident tempora autem et assumenda qui voluptas corrupti autem vel.", new DateTime(2018, 7, 21, 22, 47, 41, 183, DateTimeKind.Unspecified).AddTicks(5031), "Enim quaerat et.", 75, 31, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 44,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 10, 29, 6, 53, 43, 223, DateTimeKind.Unspecified).AddTicks(5304), "Molestiae perspiciatis exercitationem aut consectetur consequatur repudiandae mollitia ab adipisci et eos non ut.", new DateTime(2020, 5, 15, 1, 37, 2, 908, DateTimeKind.Unspecified).AddTicks(5783), "Nihil dolorum ut rem.", 18, 15, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 45,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2018, 7, 31, 23, 10, 13, 446, DateTimeKind.Unspecified).AddTicks(93), "Et aut nobis vel ut dolorum incidunt dolorem iste.", new DateTime(2018, 9, 2, 11, 36, 36, 572, DateTimeKind.Unspecified).AddTicks(4473), "Voluptatem nihil.", 40, 31, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 46,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 7, 31, 10, 6, 44, 529, DateTimeKind.Unspecified).AddTicks(9510), "Necessitatibus quia dolorum enim deserunt iste labore error animi.", new DateTime(2020, 1, 7, 21, 29, 3, 408, DateTimeKind.Unspecified).AddTicks(6419), "In est.", 49, 5, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 47,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 11, 9, 22, 6, 52, 893, DateTimeKind.Unspecified).AddTicks(1006), "Similique reiciendis qui accusantium rerum eum tempore et soluta perspiciatis minima voluptas dignissimos consequatur.", new DateTime(2020, 1, 7, 3, 39, 59, 710, DateTimeKind.Unspecified).AddTicks(1310), "Odit tempora et molestias.", 31, 18, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 48,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 11, 18, 9, 29, 21, 138, DateTimeKind.Unspecified).AddTicks(6490), "Consequuntur qui incidunt quas dolore aut.", new DateTime(2019, 11, 21, 16, 58, 11, 362, DateTimeKind.Unspecified).AddTicks(9662), "Sunt dolor.", 63, 24, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 49,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 1, 3, 12, 11, 26, 858, DateTimeKind.Unspecified).AddTicks(3507), "Laborum rerum inventore qui reprehenderit aut odio dolor voluptate veniam et non vel.", new DateTime(2020, 1, 16, 11, 54, 29, 909, DateTimeKind.Unspecified).AddTicks(9187), "Repudiandae quas deserunt voluptas non.", 46, 18, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 50,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 5, 15, 20, 14, 7, 395, DateTimeKind.Unspecified).AddTicks(5768), "Ratione aliquam libero delectus sint mollitia tempora laudantium.", new DateTime(2020, 6, 16, 13, 34, 26, 890, DateTimeKind.Unspecified).AddTicks(1219), "Debitis molestiae non aut.", 78, 7, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 51,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 23, 16, 39, 2, 867, DateTimeKind.Unspecified).AddTicks(8264), "Eligendi nostrum quo est eligendi ut.", new DateTime(2019, 9, 23, 18, 29, 56, 760, DateTimeKind.Unspecified).AddTicks(8081), "Veritatis aliquid et.", 50, 3, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 52,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 3, 7, 14, 40, 49, 623, DateTimeKind.Unspecified).AddTicks(4431), "Tempore dolores non voluptatum nam consequatur inventore ut ea dolores totam saepe doloribus.", new DateTime(2021, 7, 6, 9, 6, 53, 654, DateTimeKind.Unspecified).AddTicks(5724), "Blanditiis dicta dolor.", 29, 16, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 53,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 5, 28, 5, 26, 59, 83, DateTimeKind.Unspecified).AddTicks(5471), "Iste repellendus quia pariatur fugiat temporibus officiis fugit sit harum molestiae qui deserunt ea.", new DateTime(2021, 5, 30, 5, 4, 0, 116, DateTimeKind.Unspecified).AddTicks(3391), "Vel odio ut error voluptatem.", 45, 36 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 54,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2018, 11, 27, 6, 2, 59, 860, DateTimeKind.Unspecified).AddTicks(703), "Explicabo magnam ut nisi dolores sunt saepe similique odit voluptatem.", new DateTime(2018, 11, 30, 12, 44, 5, 617, DateTimeKind.Unspecified).AddTicks(4434), "Dolorem delectus nihil molestias quaerat.", 48, 35, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 55,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 9, 19, 19, 20, 35, 91, DateTimeKind.Unspecified).AddTicks(2972), "Hic culpa sunt nostrum consequatur odio quod deserunt commodi cumque officia nemo eligendi dolorum.", new DateTime(2019, 12, 30, 6, 38, 37, 631, DateTimeKind.Unspecified).AddTicks(2436), "Consequatur facere officia numquam eius.", 20, 6 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 56,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 5, 4, 12, 41, 24, 70, DateTimeKind.Unspecified).AddTicks(1733), "Maxime quisquam numquam quis iure nesciunt tempore et ipsa.", new DateTime(2021, 6, 4, 1, 44, 35, 200, DateTimeKind.Unspecified).AddTicks(378), "Exercitationem eos.", 62, 13 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 57,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 7, 13, 19, 22, 30, 295, DateTimeKind.Unspecified).AddTicks(2433), "Provident eius et ut harum ut aliquam.", new DateTime(2019, 8, 16, 4, 20, 2, 795, DateTimeKind.Unspecified).AddTicks(5399), "Error qui eum sint distinctio.", 8, 10, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 58,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 2, 20, 23, 3, 59, 683, DateTimeKind.Unspecified).AddTicks(527), "Sequi aliquid et eligendi dolor perspiciatis alias omnis voluptatem facere veritatis expedita temporibus.", new DateTime(2019, 3, 6, 2, 58, 16, 63, DateTimeKind.Unspecified).AddTicks(4792), "Dolorem sunt.", 19, 32, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 59,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 25, 20, 45, 22, 97, DateTimeKind.Unspecified).AddTicks(5927), "Possimus amet esse est architecto aut voluptatum numquam.", new DateTime(2021, 3, 18, 4, 50, 27, 293, DateTimeKind.Unspecified).AddTicks(6181), "Quas natus.", 25, 8, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 60,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 8, 7, 2, 37, 4, 41, DateTimeKind.Unspecified).AddTicks(5422), "Eius aspernatur et debitis voluptates animi odio quia consequatur aut necessitatibus delectus laborum quasi.", new DateTime(2019, 8, 13, 13, 28, 6, 540, DateTimeKind.Unspecified).AddTicks(7539), "Vitae ut quis.", 22, 10, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 61,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 8, 10, 7, 29, 42, 643, DateTimeKind.Unspecified).AddTicks(4255), "Aut natus fugit eum perferendis quos.", new DateTime(2021, 6, 18, 19, 51, 24, 485, DateTimeKind.Unspecified).AddTicks(1802), "Rem saepe.", 19, 29, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 62,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 2, 18, 17, 49, 26, 83, DateTimeKind.Unspecified).AddTicks(3690), "Illum dolore ut aut deserunt voluptas incidunt laboriosam.", new DateTime(2021, 4, 5, 21, 17, 26, 1, DateTimeKind.Unspecified).AddTicks(446), "Laudantium nam aut.", 67, 9, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 63,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2018, 8, 25, 19, 49, 5, 125, DateTimeKind.Unspecified).AddTicks(5891), "Voluptatem quibusdam numquam molestiae soluta exercitationem sit expedita odit eum.", new DateTime(2019, 2, 16, 11, 4, 41, 685, DateTimeKind.Unspecified).AddTicks(552), "Minima sint voluptas sit.", 78, 32 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 64,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 11, 18, 12, 31, 27, 429, DateTimeKind.Unspecified).AddTicks(6741), "Omnis sit est dolor totam doloremque qui et sed culpa.", new DateTime(2020, 12, 18, 18, 54, 57, 546, DateTimeKind.Unspecified).AddTicks(7518), "Nulla sunt temporibus.", 45, 29, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 65,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 3, 5, 6, 2, 22, 410, DateTimeKind.Unspecified).AddTicks(3261), "Velit libero quia tenetur quam dolor nostrum quia eos labore iusto rerum sed.", new DateTime(2021, 3, 5, 6, 35, 47, 522, DateTimeKind.Unspecified).AddTicks(7255), "Animi qui eius.", 3, 22, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 66,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 3, 15, 27, 17, 357, DateTimeKind.Unspecified).AddTicks(6605), "Dolorem quia inventore voluptatem corrupti at voluptatem aliquid voluptatibus architecto odit quia.", new DateTime(2021, 8, 11, 2, 22, 22, 307, DateTimeKind.Unspecified).AddTicks(8918), "Est rem voluptatem quia cupiditate.", 52, 33, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 67,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 3, 7, 19, 6, 45, 990, DateTimeKind.Unspecified).AddTicks(4285), "Quam voluptates nisi in praesentium similique eos sunt suscipit explicabo unde animi.", new DateTime(2019, 3, 8, 6, 7, 25, 553, DateTimeKind.Unspecified).AddTicks(7985), "Ullam ratione.", 2, 12, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 68,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 3, 18, 6, 50, 11, 728, DateTimeKind.Unspecified).AddTicks(4628), "Praesentium nam dolor sed ad enim voluptates dolorem ipsam qui.", new DateTime(2021, 5, 14, 9, 38, 57, 796, DateTimeKind.Unspecified).AddTicks(5544), "Eum velit mollitia.", 58, 16, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 69,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 3, 11, 6, 44, 42, 430, DateTimeKind.Unspecified).AddTicks(5347), "Voluptatibus natus id ut dolorem incidunt excepturi fuga et sint id.", new DateTime(2020, 1, 22, 12, 49, 38, 361, DateTimeKind.Unspecified).AddTicks(6285), "Doloremque quis neque omnis.", 64, 7 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 70,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 8, 26, 13, 14, 12, 643, DateTimeKind.Unspecified).AddTicks(3680), "Omnis est tempora consequatur consectetur quae sint ducimus nisi explicabo.", new DateTime(2019, 8, 29, 16, 50, 1, 177, DateTimeKind.Unspecified).AddTicks(9673), "Exercitationem officia nam.", 9, 10, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 71,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 3, 3, 2, 15, 10, 288, DateTimeKind.Unspecified).AddTicks(3136), "Qui sed est et rerum maxime.", new DateTime(2021, 3, 5, 14, 5, 3, 899, DateTimeKind.Unspecified).AddTicks(3024), "Nam odit.", 63, 22, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 72,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 10, 12, 7, 29, 910, DateTimeKind.Unspecified).AddTicks(8589), "Quia reiciendis amet asperiores dolor veniam qui corrupti ut.", new DateTime(2021, 8, 1, 6, 15, 59, 224, DateTimeKind.Unspecified).AddTicks(414), "Sunt fugit eos quia fugiat.", 48, 39, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 73,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 6, 8, 9, 25, 23, 826, DateTimeKind.Unspecified).AddTicks(5463), "Sunt qui officia dolorum et vitae deleniti nam.", new DateTime(2019, 7, 28, 10, 23, 44, 569, DateTimeKind.Unspecified).AddTicks(5516), "Omnis ut.", 62, 23, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 74,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 12, 5, 20, 53, 39, 238, DateTimeKind.Unspecified).AddTicks(6286), "At in voluptatem ea vitae voluptatem in.", new DateTime(2019, 12, 10, 16, 11, 53, 267, DateTimeKind.Unspecified).AddTicks(7236), "Ad autem corrupti.", 15, 24, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 75,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 7, 22, 20, 46, 16, 765, DateTimeKind.Unspecified).AddTicks(285), "Iste voluptas aut sint excepturi porro quisquam vel facilis amet nihil ullam dolorem.", new DateTime(2021, 3, 28, 9, 44, 49, 776, DateTimeKind.Unspecified).AddTicks(9468), "Voluptatem nulla eum fugit aut.", 23, 5, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 76,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2018, 12, 23, 17, 55, 43, 579, DateTimeKind.Unspecified).AddTicks(728), "Sit quas eum nostrum et vero.", new DateTime(2018, 12, 24, 21, 31, 12, 665, DateTimeKind.Unspecified).AddTicks(7352), "Id iusto eius quasi.", 75, 38, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 77,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 3, 3, 15, 9, 44, 615, DateTimeKind.Unspecified).AddTicks(6827), "Possimus qui veniam neque laboriosam beatae placeat quia voluptatibus.", new DateTime(2021, 3, 4, 13, 51, 36, 139, DateTimeKind.Unspecified).AddTicks(2310), "Placeat rerum excepturi est ea.", 27, 22 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 78,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 5, 17, 21, 4, 48, 17, DateTimeKind.Unspecified).AddTicks(6672), "Molestiae neque repellendus et unde aperiam qui ipsum.", new DateTime(2021, 6, 5, 2, 32, 46, 445, DateTimeKind.Unspecified).AddTicks(7675), "Consequatur et aut.", 80, 11 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 79,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 3, 7, 8, 22, 2, 982, DateTimeKind.Unspecified).AddTicks(1955), "At neque nemo perspiciatis sit debitis in qui doloribus facilis mollitia perspiciatis quod.", new DateTime(2021, 7, 24, 14, 6, 45, 839, DateTimeKind.Unspecified).AddTicks(9605), "Aspernatur laudantium tempore.", 14, 16 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 80,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 12, 24, 9, 42, 0, 82, DateTimeKind.Unspecified).AddTicks(697), "Quis repellat assumenda molestiae eveniet aliquam officiis qui consectetur est.", new DateTime(2020, 3, 11, 10, 59, 28, 234, DateTimeKind.Unspecified).AddTicks(8730), "Sequi minus consequuntur sint voluptates.", 73, 34, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 81,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 2, 10, 6, 5, 37, 373, DateTimeKind.Unspecified).AddTicks(5456), "Mollitia enim rerum dignissimos molestiae quos occaecati.", new DateTime(2020, 2, 21, 19, 51, 32, 67, DateTimeKind.Unspecified).AddTicks(6772), "Qui quae dolor in quo.", 5, 4, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 82,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 8, 2, 13, 26, 31, 532, DateTimeKind.Unspecified).AddTicks(3200), "Sapiente unde id expedita sit ab nisi veritatis ipsam repudiandae vitae laboriosam.", new DateTime(2019, 11, 12, 8, 27, 15, 305, DateTimeKind.Unspecified).AddTicks(7581), "Debitis harum ullam explicabo iure.", 14, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 83,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 6, 4, 10, 19, 16, 512, DateTimeKind.Unspecified).AddTicks(4175), "Adipisci et eum officiis doloremque quia occaecati hic temporibus at facere distinctio vero et.", new DateTime(2021, 6, 24, 14, 0, 10, 157, DateTimeKind.Unspecified).AddTicks(2626), "Qui sit eum et aut.", 17, 13, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 84,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 25, 0, 48, 29, 630, DateTimeKind.Unspecified).AddTicks(104), "Ad autem voluptatum dolores excepturi rerum.", new DateTime(2019, 9, 26, 4, 22, 10, 262, DateTimeKind.Unspecified).AddTicks(3263), "Rerum enim.", 66, 3, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 85,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 11, 3, 9, 53, 32, 307, DateTimeKind.Unspecified).AddTicks(5794), "Odit reprehenderit dolores unde deleniti accusantium.", new DateTime(2020, 11, 10, 1, 1, 8, 413, DateTimeKind.Unspecified).AddTicks(7124), "Nihil eius totam.", 5, 21, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 86,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 1, 27, 14, 22, 42, 268, DateTimeKind.Unspecified).AddTicks(8130), "Error iste beatae iste architecto aut nemo minus sint vero modi ipsa sequi.", new DateTime(2020, 2, 23, 19, 5, 9, 157, DateTimeKind.Unspecified).AddTicks(3177), "A eos rerum minima.", 9, 25, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 87,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 6, 21, 1, 50, 53, 768, DateTimeKind.Unspecified).AddTicks(8090), "Aliquam aperiam qui nemo eum aperiam corrupti sed in eos nam reprehenderit voluptatum doloremque.", new DateTime(2021, 8, 2, 4, 1, 28, 503, DateTimeKind.Unspecified).AddTicks(3331), "Molestias voluptas qui magnam nostrum.", 14, 16 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 88,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 8, 11, 17, 9, 33, 477, DateTimeKind.Unspecified).AddTicks(4755), "Quidem error omnis quia est sed reiciendis corporis repellendus fuga qui reiciendis est.", new DateTime(2021, 9, 25, 8, 43, 44, 438, DateTimeKind.Unspecified).AddTicks(337), "Et est accusamus.", 41, 17, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 89,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 2, 18, 21, 14, 26, 75, DateTimeKind.Unspecified).AddTicks(2946), "Reprehenderit nobis hic dolorum vitae perferendis.", new DateTime(2019, 10, 15, 5, 14, 1, 982, DateTimeKind.Unspecified).AddTicks(1748), "Totam excepturi est sint.", 54, 18, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 90,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 6, 2, 9, 58, 55, 902, DateTimeKind.Unspecified).AddTicks(8150), "Id at non ea aut reprehenderit fugiat ut perspiciatis.", new DateTime(2019, 9, 29, 21, 46, 53, 500, DateTimeKind.Unspecified).AddTicks(1890), "Ad ipsam voluptas blanditiis neque.", 73, 40, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 91,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 12, 20, 16, 31, 29, 661, DateTimeKind.Unspecified).AddTicks(8876), "Modi est consectetur nostrum illum sit facere ipsam perspiciatis.", new DateTime(2021, 3, 20, 20, 2, 29, 677, DateTimeKind.Unspecified).AddTicks(6238), "Est dolor sunt ipsum.", 13, 17, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 92,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 3, 7, 5, 7, 53, 139, DateTimeKind.Unspecified).AddTicks(4245), "Culpa natus temporibus id rem odio vel possimus laboriosam omnis ipsum qui sed tempore.", new DateTime(2019, 3, 9, 8, 30, 30, 519, DateTimeKind.Unspecified).AddTicks(5118), "Veritatis vero.", 53, 12 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 93,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 1, 2, 14, 13, 30, 4, DateTimeKind.Unspecified).AddTicks(2863), "Dolore accusantium aspernatur non autem culpa fugiat.", new DateTime(2019, 3, 9, 12, 57, 43, 509, DateTimeKind.Unspecified).AddTicks(7118), "Officia dicta quidem sint et.", 39, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 94,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 4, 5, 16, 54, 40, 121, DateTimeKind.Unspecified).AddTicks(2311), "Asperiores qui a aut quasi quo quas perferendis quis ea itaque in.", new DateTime(2021, 9, 28, 9, 16, 20, 103, DateTimeKind.Unspecified).AddTicks(9358), "Placeat repudiandae inventore.", 28, 20, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 95,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 11, 12, 13, 16, 33, 32, DateTimeKind.Unspecified).AddTicks(7623), "Ut deserunt facere quod dolorem tempore tempora.", new DateTime(2021, 11, 12, 16, 13, 52, 489, DateTimeKind.Unspecified).AddTicks(3616), "Aliquid distinctio.", 43, 27, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 96,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 5, 7, 18, 58, 31, 685, DateTimeKind.Unspecified).AddTicks(9437), "Dolor amet deserunt velit quas dolor saepe reprehenderit.", new DateTime(2021, 8, 9, 17, 49, 48, 451, DateTimeKind.Unspecified).AddTicks(1375), "Facilis autem nulla.", 77, 1, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 97,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 3, 1, 0, 11, 6, 546, DateTimeKind.Unspecified).AddTicks(8096), "Quia eos et dolorem minima commodi possimus quos non accusamus.", new DateTime(2020, 3, 1, 20, 13, 45, 815, DateTimeKind.Unspecified).AddTicks(9760), "Natus qui unde voluptatem.", 9, 4, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 98,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 3, 29, 21, 40, 15, 686, DateTimeKind.Unspecified).AddTicks(9703), "Totam velit vero aliquid sequi reiciendis molestiae aspernatur in ex ducimus dolores eos fuga.", new DateTime(2021, 5, 9, 5, 53, 6, 366, DateTimeKind.Unspecified).AddTicks(9869), "Dolorem et.", 2, 33, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 99,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2018, 7, 1, 16, 47, 46, 23, DateTimeKind.Unspecified).AddTicks(8498), "Occaecati molestiae quam voluptate laudantium at perspiciatis qui sit et iste qui ut temporibus.", new DateTime(2018, 7, 22, 9, 40, 19, 478, DateTimeKind.Unspecified).AddTicks(5768), "Fugiat maxime est.", 15, 31, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 100,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 2, 15, 22, 13, 32, 479, DateTimeKind.Unspecified).AddTicks(5750), "Et quasi qui alias quod qui placeat.", new DateTime(2020, 3, 7, 21, 44, 19, 821, DateTimeKind.Unspecified).AddTicks(5493), "Atque id.", 67, 4, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 101,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 7, 9, 15, 56, 36, 819, DateTimeKind.Unspecified).AddTicks(3872), "Est consequatur ab et dolore et soluta nostrum ipsam occaecati fugiat.", new DateTime(2021, 7, 29, 13, 29, 38, 374, DateTimeKind.Unspecified).AddTicks(3370), "Natus occaecati.", 39, 39 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 102,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 1, 10, 15, 2, 19, 316, DateTimeKind.Unspecified).AddTicks(8556), "Magnam culpa sint fugiat quam qui a cupiditate et fugiat harum blanditiis voluptas.", new DateTime(2020, 1, 17, 10, 13, 0, 870, DateTimeKind.Unspecified).AddTicks(6095), "Nihil voluptas suscipit perferendis pariatur.", 32, 18, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 103,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 4, 22, 8, 27, 3, 123, DateTimeKind.Unspecified).AddTicks(5583), "Repellendus provident nesciunt dolorem esse ab occaecati autem.", new DateTime(2021, 4, 23, 11, 19, 20, 511, DateTimeKind.Unspecified).AddTicks(9034), "Nobis eius.", 64, 8 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 104,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 11, 17, 20, 15, 40, 504, DateTimeKind.Unspecified).AddTicks(282), "Quo ea repellat quia magni itaque odit tempora.", new DateTime(2019, 11, 28, 18, 45, 54, 743, DateTimeKind.Unspecified).AddTicks(1895), "Minus ut.", 76, 2, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 105,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 19, 9, 27, 9, 498, DateTimeKind.Unspecified).AddTicks(5793), "Aliquam eaque et consequatur fugiat est perferendis qui ipsam quidem quo et.", new DateTime(2019, 11, 25, 16, 2, 24, 647, DateTimeKind.Unspecified).AddTicks(2122), "Dolorem culpa rerum autem maiores.", 18, 14, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 106,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2018, 7, 10, 17, 48, 19, 526, DateTimeKind.Unspecified).AddTicks(7455), "Itaque quis sed temporibus laborum et.", new DateTime(2018, 9, 7, 17, 19, 38, 254, DateTimeKind.Unspecified).AddTicks(6144), "Et deserunt.", 35, 31 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 107,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 11, 26, 23, 27, 45, 499, DateTimeKind.Unspecified).AddTicks(1186), "Autem atque et cupiditate voluptates velit magni quos fugiat vel provident quo ea voluptas.", new DateTime(2020, 2, 3, 14, 29, 13, 874, DateTimeKind.Unspecified).AddTicks(8661), "Error consequatur et qui.", 6, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 108,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 6, 25, 7, 7, 43, 932, DateTimeKind.Unspecified).AddTicks(2433), "Necessitatibus dolor quis dolores repellendus quia occaecati officiis incidunt qui dolorem tempore non.", new DateTime(2019, 8, 25, 21, 19, 47, 631, DateTimeKind.Unspecified).AddTicks(3603), "Numquam eius voluptate dolore.", 63, 2, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 109,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2018, 7, 28, 16, 55, 13, 59, DateTimeKind.Unspecified).AddTicks(2238), "Quis ex rerum est dolores facere.", new DateTime(2019, 1, 28, 16, 59, 25, 775, DateTimeKind.Unspecified).AddTicks(3694), "Incidunt officiis quasi.", 53, 32 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 110,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2018, 10, 24, 21, 41, 56, 737, DateTimeKind.Unspecified).AddTicks(2755), "Quae est aut sapiente consequuntur fugit aut quisquam dicta quis nihil et sint iusto.", new DateTime(2020, 1, 16, 13, 48, 20, 240, DateTimeKind.Unspecified).AddTicks(8736), "Harum nulla.", 62, 40, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 111,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 8, 15, 22, 38, 18, 34, DateTimeKind.Unspecified).AddTicks(7565), "Iure quia blanditiis sunt sunt repellat aut consequatur sit.", new DateTime(2019, 8, 28, 15, 30, 59, 567, DateTimeKind.Unspecified).AddTicks(8977), "Eos voluptas.", 18, 10, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 112,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2018, 6, 27, 19, 48, 54, 843, DateTimeKind.Unspecified).AddTicks(9157), "Molestiae quia eos porro aspernatur expedita est sed et.", new DateTime(2018, 12, 7, 13, 22, 58, 254, DateTimeKind.Unspecified).AddTicks(4360), "Earum eveniet enim harum vero.", 50, 34, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 113,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2018, 11, 26, 21, 13, 24, 609, DateTimeKind.Unspecified).AddTicks(6829), "Autem in laborum cumque et quibusdam sunt debitis.", new DateTime(2018, 12, 6, 14, 51, 10, 437, DateTimeKind.Unspecified).AddTicks(4694), "Libero ut aut quisquam quia.", 3, 35 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 114,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 7, 8, 16, 10, 29, 178, DateTimeKind.Unspecified).AddTicks(1688), "Aut laudantium iure ducimus provident atque mollitia quasi.", new DateTime(2021, 4, 9, 12, 27, 16, 239, DateTimeKind.Unspecified).AddTicks(2269), "Alias mollitia vero doloribus nisi.", 52, 11, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 115,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 3, 10, 1, 40, 9, 819, DateTimeKind.Unspecified).AddTicks(3955), "Perferendis vero vero iste voluptas repellat sequi aliquid.", new DateTime(2021, 4, 6, 10, 24, 58, 781, DateTimeKind.Unspecified).AddTicks(7770), "Illo est.", 6, 9, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 116,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 7, 24, 3, 12, 22, 19, DateTimeKind.Unspecified).AddTicks(6919), "Perferendis neque ipsum possimus sed sit animi molestias quaerat deserunt laboriosam repudiandae ut ad.", new DateTime(2019, 11, 28, 5, 5, 7, 268, DateTimeKind.Unspecified).AddTicks(4929), "Saepe vel reiciendis voluptatum.", 54, 14, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 117,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2020, 4, 5, 15, 25, 31, 948, DateTimeKind.Unspecified).AddTicks(1439), "Quaerat molestiae ut velit cum sed provident libero.", new DateTime(2020, 4, 8, 16, 59, 54, 59, DateTimeKind.Unspecified).AddTicks(4252), "Totam incidunt aspernatur.", 68, 26 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 118,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 12, 26, 13, 36, 57, 296, DateTimeKind.Unspecified).AddTicks(3767), "Quos saepe possimus aut enim voluptas natus est voluptas omnis eveniet nemo.", new DateTime(2020, 1, 3, 9, 5, 7, 195, DateTimeKind.Unspecified).AddTicks(7586), "Et sed.", 69, 40, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 119,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 10, 23, 2, 50, 26, 903, DateTimeKind.Unspecified).AddTicks(1349), "Est veritatis libero ut possimus similique ut asperiores aperiam eum dolores facere aut.", new DateTime(2020, 4, 3, 2, 7, 50, 322, DateTimeKind.Unspecified).AddTicks(7510), "Ad repellendus ea.", 14, 14, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 120,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 9, 26, 21, 53, 44, 854, DateTimeKind.Unspecified).AddTicks(2726), "Facilis cumque molestiae sit doloribus animi autem facere quia.", new DateTime(2021, 10, 11, 1, 25, 42, 823, DateTimeKind.Unspecified).AddTicks(4440), "Voluptatum fugiat est harum.", 4, 27 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 121,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 1, 24, 18, 11, 56, 603, DateTimeKind.Unspecified).AddTicks(1985), "Qui et ipsum dolore totam ducimus quisquam quam unde aut fugit molestiae repellat.", new DateTime(2020, 1, 8, 2, 46, 57, 56, DateTimeKind.Unspecified).AddTicks(5670), "Asperiores praesentium.", 5, 38, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 122,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 8, 30, 23, 58, 55, 246, DateTimeKind.Unspecified).AddTicks(2973), "Et ipsam dolorem et eius voluptates nesciunt cum sit.", new DateTime(2019, 10, 15, 22, 19, 52, 998, DateTimeKind.Unspecified).AddTicks(5424), "Dolore alias et qui.", 32, 40, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 123,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 2, 3, 2, 20, 59, 683, DateTimeKind.Unspecified).AddTicks(575), "Illo voluptatem quidem qui distinctio modi voluptatem voluptates quam rerum aliquid.", new DateTime(2021, 3, 16, 12, 25, 2, 206, DateTimeKind.Unspecified).AddTicks(7881), "Deleniti exercitationem.", 63, 9, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 124,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 1, 3, 14, 34, 32, 484, DateTimeKind.Unspecified).AddTicks(4390), "Iusto voluptates perferendis et eos aut modi dolorem neque hic et vel consectetur placeat.", new DateTime(2020, 1, 8, 20, 42, 44, 550, DateTimeKind.Unspecified).AddTicks(8735), "Sequi veniam quasi velit rerum.", 25, 2, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 125,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 10, 26, 8, 48, 14, 937, DateTimeKind.Unspecified).AddTicks(8983), "Maxime tenetur voluptate qui architecto velit tempora.", new DateTime(2019, 12, 9, 18, 31, 28, 205, DateTimeKind.Unspecified).AddTicks(580), "Numquam voluptas aut.", 39, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 126,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 10, 12, 23, 35, 1, 566, DateTimeKind.Unspecified).AddTicks(4587), "Amet quaerat esse et nihil eaque omnis.", new DateTime(2020, 11, 13, 8, 41, 56, 422, DateTimeKind.Unspecified).AddTicks(766), "Ipsam ea quibusdam adipisci.", 13, 21, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 127,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 3, 7, 5, 10, 53, 851, DateTimeKind.Unspecified).AddTicks(3362), "Nihil quidem placeat delectus est veritatis delectus debitis unde unde consequatur et voluptatibus excepturi.", new DateTime(2019, 3, 8, 14, 54, 29, 609, DateTimeKind.Unspecified).AddTicks(7188), "Aut aut officiis neque.", 59, 12, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 128,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 1, 5, 13, 3, 35, 399, DateTimeKind.Unspecified).AddTicks(2641), "Atque quaerat sunt sunt eum est sequi dolores explicabo iste.", new DateTime(2020, 2, 28, 3, 5, 39, 13, DateTimeKind.Unspecified).AddTicks(5035), "Provident consequatur aut sunt eos.", 77, 15, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 129,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 11, 13, 17, 23, 44, 475, DateTimeKind.Unspecified).AddTicks(2354), "Autem vero facilis ut iste voluptatem suscipit autem suscipit eum dolor odit.", new DateTime(2021, 2, 6, 21, 56, 19, 992, DateTimeKind.Unspecified).AddTicks(952), "Quos incidunt dolores quo.", 37, 8, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 130,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 9, 24, 4, 15, 0, 632, DateTimeKind.Unspecified).AddTicks(3921), "Esse id est sint perferendis quis officiis maiores debitis in.", new DateTime(2019, 9, 24, 13, 11, 24, 664, DateTimeKind.Unspecified).AddTicks(117), "Ut rem.", 33, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 131,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 8, 7, 11, 1, 41, 230, DateTimeKind.Unspecified).AddTicks(4207), "Qui consectetur sed ut est dolore et doloremque veritatis voluptatem labore numquam.", new DateTime(2021, 8, 7, 20, 15, 9, 478, DateTimeKind.Unspecified).AddTicks(1161), "Quia dolorem fuga illum quia.", 44, 39, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 132,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 12, 20, 14, 52, 8, 474, DateTimeKind.Unspecified).AddTicks(1600), "Nisi rem corporis autem laboriosam laboriosam laborum expedita ea expedita aut provident quibusdam nam.", new DateTime(2020, 1, 2, 3, 28, 22, 262, DateTimeKind.Unspecified).AddTicks(2888), "Laudantium reprehenderit praesentium aliquam aut.", 42, 24, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 133,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 12, 20, 8, 10, 28, 230, DateTimeKind.Unspecified).AddTicks(5065), "Minima nulla ex minus ullam rem.", new DateTime(2020, 1, 13, 21, 53, 48, 55, DateTimeKind.Unspecified).AddTicks(1835), "Voluptates fuga ratione minima alias.", 76, 4, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 134,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2018, 7, 3, 7, 58, 5, 422, DateTimeKind.Unspecified).AddTicks(9472), "Fugit reprehenderit aliquam debitis dolor quo est et.", new DateTime(2020, 9, 2, 1, 10, 55, 188, DateTimeKind.Unspecified).AddTicks(5200), "Eligendi quisquam et qui at.", 26, 30, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 135,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 2, 14, 15, 24, 9, 275, DateTimeKind.Unspecified).AddTicks(3036), "Voluptas ullam et ea sed dignissimos.", new DateTime(2020, 4, 14, 19, 4, 16, 273, DateTimeKind.Unspecified).AddTicks(7899), "Quis enim.", 70, 26, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 136,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2020, 2, 17, 0, 25, 22, 182, DateTimeKind.Unspecified).AddTicks(2511), "Est pariatur dolorem laborum consequatur ea libero sint.", new DateTime(2020, 3, 5, 18, 13, 27, 396, DateTimeKind.Unspecified).AddTicks(5977), "Temporibus sequi.", 10, 4 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 137,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 5, 23, 11, 11, 52, 969, DateTimeKind.Unspecified).AddTicks(9815), "Distinctio necessitatibus veniam tempora earum in rerum dolorum numquam.", new DateTime(2021, 5, 29, 9, 23, 9, 259, DateTimeKind.Unspecified).AddTicks(2660), "Voluptatibus sint dolores.", 76, 36, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 138,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 9, 2, 20, 11, 49, 286, DateTimeKind.Unspecified).AddTicks(1388), "Vero omnis praesentium expedita aspernatur repellendus ut cum et et.", new DateTime(2020, 10, 30, 17, 6, 44, 605, DateTimeKind.Unspecified).AddTicks(1643), "Porro rerum.", 31, 21, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 139,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2018, 5, 16, 2, 55, 10, 47, DateTimeKind.Unspecified).AddTicks(267), "Officia libero amet amet repellendus commodi nemo eius repellendus expedita in suscipit quas veritatis.", new DateTime(2020, 11, 9, 19, 21, 15, 66, DateTimeKind.Unspecified).AddTicks(6890), "Magni ut quae perspiciatis.", 40, 30 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 140,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 4, 26, 20, 57, 19, 238, DateTimeKind.Unspecified).AddTicks(7830), "Similique iure in id qui accusamus libero in error ut explicabo ad rem.", new DateTime(2021, 6, 4, 22, 59, 48, 564, DateTimeKind.Unspecified).AddTicks(6098), "Voluptatem omnis nostrum odit.", 2, 13, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 141,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 8, 15, 21, 53, 45, 994, DateTimeKind.Unspecified).AddTicks(3374), "Consequuntur voluptatum unde eos facilis vel autem vitae ex numquam reiciendis voluptates.", new DateTime(2021, 9, 25, 22, 36, 48, 83, DateTimeKind.Unspecified).AddTicks(5365), "Fugit quae consequatur nihil facilis.", 75, 20, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 142,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2018, 8, 18, 11, 58, 56, 631, DateTimeKind.Unspecified).AddTicks(2974), "Qui consequatur omnis exercitationem doloribus temporibus.", new DateTime(2018, 9, 16, 17, 43, 57, 739, DateTimeKind.Unspecified).AddTicks(4693), "Autem rerum est provident voluptatum.", 56, 31, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 143,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 2, 13, 6, 51, 58, 784, DateTimeKind.Unspecified).AddTicks(742), "Porro consectetur fugit aspernatur dignissimos quia ut ab non eum.", new DateTime(2021, 2, 13, 13, 30, 0, 322, DateTimeKind.Unspecified).AddTicks(6844), "Sed ut et.", 7, 5, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 144,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 9, 11, 5, 26, 38, 950, DateTimeKind.Unspecified).AddTicks(5062), "Beatae sunt error blanditiis ullam sequi.", new DateTime(2020, 10, 22, 5, 57, 15, 273, DateTimeKind.Unspecified).AddTicks(1678), "Alias rem velit fugiat.", 32, 21, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 145,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2018, 9, 3, 13, 19, 22, 430, DateTimeKind.Unspecified).AddTicks(7729), "Esse autem harum consequuntur quo impedit dicta id cum necessitatibus.", new DateTime(2018, 9, 4, 23, 32, 36, 937, DateTimeKind.Unspecified).AddTicks(3065), "Suscipit et.", 32, 31, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 146,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 8, 5, 58, 4, 954, DateTimeKind.Unspecified).AddTicks(3422), "Unde facilis qui ipsum blanditiis dolor.", new DateTime(2021, 8, 17, 12, 36, 21, 882, DateTimeKind.Unspecified).AddTicks(7383), "Magni dicta ea rerum.", 47, 33, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 147,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 5, 20, 11, 20, 14, 175, DateTimeKind.Unspecified).AddTicks(3061), "Quo quis repellat nisi asperiores corrupti necessitatibus error sed ea et.", new DateTime(2020, 5, 28, 14, 17, 53, 301, DateTimeKind.Unspecified).AddTicks(458), "Qui quia.", 75, 7, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 148,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 20, 14, 48, 34, 13, DateTimeKind.Unspecified).AddTicks(1542), "Eum molestiae ut fugiat sapiente eaque sit libero nemo molestiae assumenda consequatur nulla sunt.", new DateTime(2019, 9, 25, 6, 47, 12, 306, DateTimeKind.Unspecified).AddTicks(3154), "Veniam qui accusamus qui quos.", 7, 3, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 149,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 3, 3, 18, 33, 27, 904, DateTimeKind.Unspecified).AddTicks(913), "Qui et et quibusdam amet nemo saepe ut sit laborum esse qui expedita.", new DateTime(2020, 3, 20, 2, 45, 46, 263, DateTimeKind.Unspecified).AddTicks(2122), "Voluptas eum laborum qui.", 26, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 150,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 4, 3, 13, 1, 39, 189, DateTimeKind.Unspecified).AddTicks(8642), "Voluptas accusamus quidem repellendus quia est aut consectetur iste.", new DateTime(2021, 5, 29, 0, 26, 6, 843, DateTimeKind.Unspecified).AddTicks(2738), "Tempora nesciunt.", 10, 13, 2 });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "TeamName" },
                values: new object[] { new DateTime(2019, 5, 1, 11, 48, 44, 738, DateTimeKind.Unspecified).AddTicks(843), "Albert Rau" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "TeamName" },
                values: new object[] { new DateTime(2021, 2, 15, 10, 52, 4, 365, DateTimeKind.Unspecified).AddTicks(53), "Kole Crona" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "TeamName" },
                values: new object[] { new DateTime(2021, 2, 1, 23, 48, 2, 178, DateTimeKind.Unspecified).AddTicks(9034), "Filomena Osinski" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "TeamName" },
                values: new object[] { new DateTime(2019, 12, 7, 4, 8, 24, 411, DateTimeKind.Unspecified).AddTicks(2556), "Madelyn Fahey" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "TeamName" },
                values: new object[] { new DateTime(2018, 12, 26, 14, 10, 51, 83, DateTimeKind.Unspecified).AddTicks(6758), "Alice Little" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "TeamName" },
                values: new object[] { new DateTime(2021, 1, 2, 7, 34, 15, 14, DateTimeKind.Unspecified).AddTicks(6453), "Deron Kirlin" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "TeamName" },
                values: new object[] { new DateTime(2020, 9, 30, 6, 7, 6, 772, DateTimeKind.Unspecified).AddTicks(6502), "Duncan Yost" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "TeamName" },
                values: new object[] { new DateTime(2019, 12, 23, 17, 8, 52, 818, DateTimeKind.Unspecified).AddTicks(148), "Rodolfo Waelchi" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "TeamName" },
                values: new object[] { new DateTime(2019, 7, 31, 10, 32, 10, 813, DateTimeKind.Unspecified).AddTicks(9008), "Ryder Rohan" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "TeamName" },
                values: new object[] { new DateTime(2020, 7, 16, 11, 25, 7, 332, DateTimeKind.Unspecified).AddTicks(8253), "Deonte Abbott" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1991, 8, 13, 8, 12, 42, 747, DateTimeKind.Unspecified).AddTicks(137), new DateTime(2013, 12, 2, 18, 54, 50, 471, DateTimeKind.Unspecified).AddTicks(7790), "Shanel_Hickle1@yahoo.com", "Julio", "Rath", 9 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2001, 3, 13, 21, 11, 24, 781, DateTimeKind.Unspecified).AddTicks(4320), new DateTime(2010, 5, 19, 11, 54, 23, 638, DateTimeKind.Unspecified).AddTicks(2063), "August7@yahoo.com", "Oswald", "Nienow", 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1988, 2, 18, 8, 37, 47, 716, DateTimeKind.Unspecified).AddTicks(8510), new DateTime(2007, 8, 11, 12, 2, 45, 290, DateTimeKind.Unspecified).AddTicks(2298), "Robin_Denesik72@hotmail.com", "Felix", "Durgan", 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1984, 3, 29, 17, 51, 10, 891, DateTimeKind.Unspecified).AddTicks(8267), new DateTime(2005, 12, 21, 16, 54, 43, 908, DateTimeKind.Unspecified).AddTicks(384), "Johnson17@gmail.com", "Marcelo", "Cronin", 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1990, 10, 14, 7, 37, 30, 597, DateTimeKind.Unspecified).AddTicks(630), new DateTime(2009, 5, 24, 21, 16, 37, 801, DateTimeKind.Unspecified).AddTicks(9027), "Alan.Dooley@gmail.com", "Gladys", "Muller", 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1977, 10, 4, 5, 24, 13, 643, DateTimeKind.Unspecified).AddTicks(4315), new DateTime(2020, 8, 16, 19, 8, 44, 882, DateTimeKind.Unspecified).AddTicks(6795), "Charlene.Labadie17@yahoo.com", "Kavon", "Turner", 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1978, 7, 27, 15, 44, 42, 397, DateTimeKind.Unspecified).AddTicks(6625), new DateTime(1990, 12, 5, 4, 50, 15, 525, DateTimeKind.Unspecified).AddTicks(4601), "Berta_Kreiger@hotmail.com", "Aric", "Douglas", 9 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2013, 11, 7, 12, 9, 36, 248, DateTimeKind.Unspecified).AddTicks(5258), new DateTime(2021, 8, 16, 15, 10, 21, 540, DateTimeKind.Unspecified).AddTicks(6172), "Vesta_McCullough@hotmail.com", "Geoffrey", "Murazik", 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1993, 9, 4, 0, 22, 28, 287, DateTimeKind.Unspecified).AddTicks(6188), new DateTime(2003, 12, 2, 18, 24, 20, 675, DateTimeKind.Unspecified).AddTicks(2886), "Kasey_Runolfsdottir0@gmail.com", "Deon", "Weimann", 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2002, 3, 14, 9, 50, 50, 199, DateTimeKind.Unspecified).AddTicks(4020), new DateTime(2011, 3, 27, 12, 38, 1, 431, DateTimeKind.Unspecified).AddTicks(8235), "Emely_Kassulke@gmail.com", "Buster", "Kunde", 10 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2011, 1, 12, 9, 8, 21, 601, DateTimeKind.Unspecified).AddTicks(292), new DateTime(2019, 5, 26, 20, 37, 20, 121, DateTimeKind.Unspecified).AddTicks(9393), "Celia.Gottlieb@yahoo.com", "Dorothy", "Heaney", 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1985, 6, 5, 17, 18, 50, 324, DateTimeKind.Unspecified).AddTicks(6940), new DateTime(2014, 7, 23, 16, 17, 26, 515, DateTimeKind.Unspecified).AddTicks(151), "Alicia15@yahoo.com", "Clay", "Senger", 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1998, 9, 6, 19, 12, 43, 456, DateTimeKind.Unspecified).AddTicks(6502), new DateTime(2009, 1, 13, 2, 33, 50, 422, DateTimeKind.Unspecified).AddTicks(6664), "Sofia72@gmail.com", "Kiara", "Olson", 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1986, 9, 12, 15, 25, 12, 515, DateTimeKind.Unspecified).AddTicks(950), new DateTime(2007, 10, 5, 13, 40, 3, 686, DateTimeKind.Unspecified).AddTicks(1488), "Heidi_Wolff@gmail.com", "Stuart", "Gibson", 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1974, 10, 13, 13, 22, 22, 418, DateTimeKind.Unspecified).AddTicks(6804), new DateTime(1989, 8, 17, 7, 2, 55, 982, DateTimeKind.Unspecified).AddTicks(747), "Shanny84@yahoo.com", "Mozell", "Bogan", 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1996, 7, 18, 13, 32, 45, 480, DateTimeKind.Unspecified).AddTicks(2034), new DateTime(2020, 6, 6, 19, 18, 31, 524, DateTimeKind.Unspecified).AddTicks(1462), "Maryam.Hartmann@hotmail.com", "Emilio", "Maggio", 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1972, 8, 24, 7, 27, 45, 537, DateTimeKind.Unspecified).AddTicks(3078), new DateTime(2002, 9, 14, 13, 21, 16, 638, DateTimeKind.Unspecified).AddTicks(4287), "Bennie_Hansen22@hotmail.com", "Rosalind", "Marquardt", 6 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName" },
                values: new object[] { new DateTime(1973, 10, 24, 23, 41, 29, 816, DateTimeKind.Unspecified).AddTicks(6123), new DateTime(2018, 1, 27, 0, 23, 41, 822, DateTimeKind.Unspecified).AddTicks(4477), "Marcelina12@yahoo.com", "Oswaldo", "Stokes" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName" },
                values: new object[] { new DateTime(1973, 12, 10, 20, 0, 5, 122, DateTimeKind.Unspecified).AddTicks(9955), new DateTime(1996, 5, 17, 12, 29, 40, 249, DateTimeKind.Unspecified).AddTicks(1614), "Mittie56@hotmail.com", "Levi", "Koss" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1994, 9, 10, 3, 37, 6, 378, DateTimeKind.Unspecified).AddTicks(7794), new DateTime(2019, 3, 15, 14, 43, 32, 497, DateTimeKind.Unspecified).AddTicks(7296), "Elwyn27@yahoo.com", "Wilford", "Kessler", 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2007, 8, 2, 0, 19, 50, 548, DateTimeKind.Unspecified).AddTicks(7276), new DateTime(2017, 7, 29, 6, 24, 12, 458, DateTimeKind.Unspecified).AddTicks(3742), "Monserrat.Leuschke2@yahoo.com", "Lester", "Beahan", 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1998, 6, 24, 8, 8, 56, 557, DateTimeKind.Unspecified).AddTicks(2112), new DateTime(2021, 6, 14, 10, 24, 55, 430, DateTimeKind.Unspecified).AddTicks(6362), "Dante.Braun98@hotmail.com", "Johnathon", "Bailey", 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1977, 5, 10, 11, 41, 23, 200, DateTimeKind.Unspecified).AddTicks(454), new DateTime(1985, 5, 24, 11, 25, 50, 217, DateTimeKind.Unspecified).AddTicks(9647), "Everette50@yahoo.com", "Maud", "Goldner", 6 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1998, 5, 11, 12, 57, 18, 226, DateTimeKind.Unspecified).AddTicks(7551), new DateTime(2020, 3, 25, 14, 19, 51, 523, DateTimeKind.Unspecified).AddTicks(4175), "Kenyatta18@hotmail.com", "Cordell", "Crona", 6 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1972, 6, 28, 4, 59, 48, 482, DateTimeKind.Unspecified).AddTicks(8999), new DateTime(1998, 2, 1, 4, 26, 57, 665, DateTimeKind.Unspecified).AddTicks(8888), "Jammie.Johns@gmail.com", "Dee", "Barrows", 9 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1978, 7, 23, 3, 41, 12, 158, DateTimeKind.Unspecified).AddTicks(4256), new DateTime(1993, 1, 30, 19, 22, 0, 873, DateTimeKind.Unspecified).AddTicks(4813), "Jared.Hermann@hotmail.com", "Keon", "Ferry", 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1982, 3, 10, 16, 52, 19, 269, DateTimeKind.Unspecified).AddTicks(7084), new DateTime(2013, 9, 21, 3, 32, 37, 444, DateTimeKind.Unspecified).AddTicks(6925), "Lisette.Gutmann66@yahoo.com", "Jordan", "Parisian", 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2011, 5, 1, 5, 50, 6, 989, DateTimeKind.Unspecified).AddTicks(2348), new DateTime(2019, 5, 25, 3, 33, 39, 706, DateTimeKind.Unspecified).AddTicks(1409), "Sophie11@yahoo.com", "Favian", "Reilly", 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1979, 7, 18, 5, 16, 56, 708, DateTimeKind.Unspecified).AddTicks(3228), new DateTime(2003, 5, 22, 22, 17, 18, 539, DateTimeKind.Unspecified).AddTicks(7703), "Ada.Wisozk@gmail.com", "Ivory", "Rippin", 10 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1994, 6, 5, 10, 40, 37, 192, DateTimeKind.Unspecified).AddTicks(9619), new DateTime(2008, 4, 5, 17, 2, 54, 118, DateTimeKind.Unspecified).AddTicks(5424), "Alexandro.Mann16@hotmail.com", "Zella", "Klein", 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2000, 8, 1, 11, 8, 18, 344, DateTimeKind.Unspecified).AddTicks(1610), new DateTime(2010, 8, 14, 20, 38, 15, 127, DateTimeKind.Unspecified).AddTicks(5870), "Daija57@hotmail.com", "Oswaldo", "Pagac", 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName" },
                values: new object[] { new DateTime(1984, 10, 19, 9, 49, 51, 939, DateTimeKind.Unspecified).AddTicks(5562), new DateTime(2006, 4, 6, 5, 31, 56, 711, DateTimeKind.Unspecified).AddTicks(6291), "Angie62@hotmail.com", "Diana", "Crona" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2013, 4, 20, 13, 2, 52, 515, DateTimeKind.Unspecified).AddTicks(9518), new DateTime(2021, 6, 15, 20, 57, 2, 854, DateTimeKind.Unspecified).AddTicks(6173), "Clotilde.Tromp33@hotmail.com", "Isabell", "Schoen", 10 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1990, 4, 23, 9, 21, 50, 578, DateTimeKind.Unspecified).AddTicks(2967), new DateTime(2000, 5, 1, 4, 4, 46, 230, DateTimeKind.Unspecified).AddTicks(1207), "Marcellus_Beer@hotmail.com", "Gennaro", "Reilly", 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1990, 9, 4, 14, 36, 56, 557, DateTimeKind.Unspecified).AddTicks(7632), new DateTime(2017, 5, 14, 1, 27, 31, 846, DateTimeKind.Unspecified).AddTicks(8628), "Moshe_Greenholt62@hotmail.com", "Julie", "Hagenes", 9 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1997, 5, 8, 8, 52, 36, 989, DateTimeKind.Unspecified).AddTicks(6351), new DateTime(2019, 10, 8, 7, 20, 2, 208, DateTimeKind.Unspecified).AddTicks(8106), "Wellington.Waelchi@hotmail.com", "Arlie", "Hansen", 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2009, 5, 9, 5, 7, 7, 630, DateTimeKind.Unspecified).AddTicks(2138), new DateTime(2020, 10, 6, 0, 7, 23, 149, DateTimeKind.Unspecified).AddTicks(978), "Shawn39@gmail.com", "Micah", "Hudson", 9 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2013, 1, 12, 20, 29, 4, 49, DateTimeKind.Unspecified).AddTicks(7470), new DateTime(2021, 3, 30, 10, 15, 40, 110, DateTimeKind.Unspecified).AddTicks(277), "Tess.Kuphal60@yahoo.com", "Selena", "Homenick", 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2010, 4, 3, 11, 33, 4, 934, DateTimeKind.Unspecified).AddTicks(4396), new DateTime(2019, 10, 8, 9, 22, 46, 187, DateTimeKind.Unspecified).AddTicks(8573), "Elinore_Jacobi13@gmail.com", "Joey", "White", 9 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1995, 9, 1, 23, 16, 19, 886, DateTimeKind.Unspecified).AddTicks(5007), new DateTime(2016, 10, 26, 23, 15, 33, 887, DateTimeKind.Unspecified).AddTicks(6059), "Tess_Terry81@hotmail.com", "Cleo", "Hoeger", 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 41,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1971, 8, 13, 22, 11, 45, 108, DateTimeKind.Unspecified).AddTicks(8975), new DateTime(2009, 8, 9, 19, 48, 9, 438, DateTimeKind.Unspecified).AddTicks(7005), "Walker.Beer@hotmail.com", "Winnifred", "Will", 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 42,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1984, 3, 9, 14, 39, 32, 270, DateTimeKind.Unspecified).AddTicks(4500), new DateTime(2012, 8, 27, 22, 24, 14, 766, DateTimeKind.Unspecified).AddTicks(2265), "Ila.Wisozk@gmail.com", "Irwin", "Lesch", 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 43,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2010, 12, 7, 10, 38, 8, 201, DateTimeKind.Unspecified).AddTicks(2698), new DateTime(2020, 7, 8, 14, 53, 49, 940, DateTimeKind.Unspecified).AddTicks(711), "Cicero57@gmail.com", "Terrance", "Hilll", 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 44,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1989, 1, 13, 12, 28, 20, 533, DateTimeKind.Unspecified).AddTicks(9686), new DateTime(2015, 2, 14, 20, 29, 43, 598, DateTimeKind.Unspecified).AddTicks(8705), "Samir60@hotmail.com", "Camron", "Strosin", 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 45,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2010, 5, 16, 19, 42, 18, 976, DateTimeKind.Unspecified).AddTicks(2954), new DateTime(2019, 3, 15, 14, 30, 31, 764, DateTimeKind.Unspecified).AddTicks(161), "Clementine_Rohan47@hotmail.com", "Adrain", "Stroman", 9 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 46,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2012, 5, 25, 12, 35, 34, 504, DateTimeKind.Unspecified).AddTicks(7574), new DateTime(2021, 5, 22, 12, 15, 55, 572, DateTimeKind.Unspecified).AddTicks(4849), "Mohammad_Mosciski99@hotmail.com", "Jerrod", "Thiel", 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 47,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2006, 1, 3, 20, 4, 12, 310, DateTimeKind.Unspecified).AddTicks(8210), new DateTime(2021, 5, 14, 8, 31, 32, 940, DateTimeKind.Unspecified).AddTicks(15), "Kieran.MacGyver@yahoo.com", "Heather", "Wiza", 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 48,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1992, 3, 13, 7, 22, 36, 549, DateTimeKind.Unspecified).AddTicks(6856), new DateTime(2002, 5, 14, 9, 17, 40, 805, DateTimeKind.Unspecified).AddTicks(7535), "Amani.Mosciski13@hotmail.com", "Korbin", "Kshlerin", 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 49,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1980, 8, 3, 7, 26, 41, 477, DateTimeKind.Unspecified).AddTicks(6256), new DateTime(1991, 8, 2, 3, 47, 34, 859, DateTimeKind.Unspecified).AddTicks(8406), "Zachary34@yahoo.com", "Felton", "Von", 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 50,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1981, 11, 20, 13, 30, 39, 764, DateTimeKind.Unspecified).AddTicks(6814), new DateTime(2021, 6, 28, 8, 5, 15, 834, DateTimeKind.Unspecified).AddTicks(2180), "Ismael.Sanford28@hotmail.com", "Pamela", "Hammes", 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 51,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName" },
                values: new object[] { new DateTime(1973, 4, 6, 5, 0, 38, 978, DateTimeKind.Unspecified).AddTicks(7949), new DateTime(2015, 12, 9, 18, 28, 50, 181, DateTimeKind.Unspecified).AddTicks(4179), "Granville_Gerlach8@hotmail.com", "Easton", "Ernser" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 52,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1978, 11, 20, 5, 10, 28, 296, DateTimeKind.Unspecified).AddTicks(719), new DateTime(1991, 6, 11, 22, 46, 41, 531, DateTimeKind.Unspecified).AddTicks(4687), "Anahi_Spencer@gmail.com", "Pete", "Dicki", 6 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 53,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1976, 4, 29, 17, 16, 29, 486, DateTimeKind.Unspecified).AddTicks(2613), new DateTime(1994, 1, 28, 20, 6, 42, 799, DateTimeKind.Unspecified).AddTicks(3733), "Carmen_Walker@hotmail.com", "Sigrid", "D'Amore", 9 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 54,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1990, 8, 28, 15, 58, 14, 647, DateTimeKind.Unspecified).AddTicks(2333), new DateTime(2006, 7, 29, 1, 50, 0, 0, DateTimeKind.Unspecified).AddTicks(1897), "Ansel_Powlowski72@hotmail.com", "Dario", "Brakus", 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 55,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "TeamId" },
                values: new object[] { new DateTime(2008, 2, 15, 3, 40, 1, 898, DateTimeKind.Unspecified).AddTicks(7318), new DateTime(2019, 4, 25, 22, 37, 22, 156, DateTimeKind.Unspecified).AddTicks(1373), "Judy_Morissette@gmail.com", "Ahmed", 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 56,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1984, 12, 1, 14, 2, 26, 174, DateTimeKind.Unspecified).AddTicks(2114), new DateTime(1995, 2, 5, 6, 19, 47, 888, DateTimeKind.Unspecified).AddTicks(1648), "Giuseppe23@yahoo.com", "Annamae", "Stroman", 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 57,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1986, 1, 26, 18, 11, 27, 481, DateTimeKind.Unspecified).AddTicks(8301), new DateTime(1996, 12, 3, 14, 42, 59, 786, DateTimeKind.Unspecified).AddTicks(1754), "Dameon99@gmail.com", "Trey", "Schroeder", 10 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 58,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1998, 3, 13, 19, 26, 13, 664, DateTimeKind.Unspecified).AddTicks(1259), new DateTime(2010, 11, 24, 12, 19, 47, 597, DateTimeKind.Unspecified).AddTicks(123), "Harvey.Swift@hotmail.com", "Ryann", "Rice", 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 59,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2000, 2, 19, 15, 35, 27, 122, DateTimeKind.Unspecified).AddTicks(862), new DateTime(2016, 10, 15, 7, 44, 29, 478, DateTimeKind.Unspecified).AddTicks(8130), "Calista38@hotmail.com", "Blake", "Grant", 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 60,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1974, 11, 29, 14, 34, 12, 825, DateTimeKind.Unspecified).AddTicks(7087), new DateTime(2015, 1, 9, 13, 0, 22, 301, DateTimeKind.Unspecified).AddTicks(7007), "Cicero56@yahoo.com", "Filiberto", "Howell", 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 61,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1974, 12, 26, 7, 4, 44, 19, DateTimeKind.Unspecified).AddTicks(3181), new DateTime(2002, 12, 21, 14, 53, 59, 49, DateTimeKind.Unspecified).AddTicks(5296), "Lelah8@hotmail.com", "Gerardo", "Hessel", 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 62,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2008, 5, 26, 0, 6, 4, 448, DateTimeKind.Unspecified).AddTicks(7200), new DateTime(2016, 9, 11, 3, 39, 35, 382, DateTimeKind.Unspecified).AddTicks(6863), "Ryann.Batz@gmail.com", "Malcolm", "Moen", 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 63,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1996, 8, 12, 10, 16, 32, 808, DateTimeKind.Unspecified).AddTicks(3202), new DateTime(2007, 12, 16, 6, 6, 47, 305, DateTimeKind.Unspecified).AddTicks(1643), "Jennifer.Nader@hotmail.com", "Sierra", "Hammes", 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 64,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1983, 8, 17, 8, 37, 27, 344, DateTimeKind.Unspecified).AddTicks(5152), new DateTime(2016, 6, 28, 11, 46, 42, 720, DateTimeKind.Unspecified).AddTicks(5608), "Vivianne_Skiles6@hotmail.com", "Aliyah", "Heller", 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 65,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1976, 7, 20, 10, 7, 45, 883, DateTimeKind.Unspecified).AddTicks(1456), new DateTime(2005, 5, 10, 11, 24, 28, 912, DateTimeKind.Unspecified).AddTicks(5445), "Ursula_Rogahn@gmail.com", "Dorthy", "Crona", 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 66,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2013, 3, 18, 11, 56, 2, 329, DateTimeKind.Unspecified).AddTicks(2892), new DateTime(2021, 4, 27, 0, 20, 34, 797, DateTimeKind.Unspecified).AddTicks(9382), "Isidro.Bergnaum@yahoo.com", "Max", "Bechtelar", 10 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 67,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName" },
                values: new object[] { new DateTime(1982, 6, 20, 13, 35, 52, 857, DateTimeKind.Unspecified).AddTicks(418), new DateTime(1994, 3, 24, 2, 51, 31, 861, DateTimeKind.Unspecified).AddTicks(5266), "Gwen.Haley2@hotmail.com", "Jazmin", "Volkman" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 68,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2009, 6, 1, 0, 9, 36, 231, DateTimeKind.Unspecified).AddTicks(4974), new DateTime(2017, 11, 15, 19, 31, 54, 485, DateTimeKind.Unspecified).AddTicks(5226), "Lester_Crooks@gmail.com", "Kaylie", "Fahey", 9 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 69,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2004, 3, 25, 17, 51, 50, 654, DateTimeKind.Unspecified).AddTicks(9516), new DateTime(2013, 4, 15, 4, 29, 12, 539, DateTimeKind.Unspecified).AddTicks(4379), "Evelyn_Gulgowski@yahoo.com", "Itzel", "Nicolas", 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 70,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName" },
                values: new object[] { new DateTime(1998, 4, 14, 2, 57, 1, 465, DateTimeKind.Unspecified).AddTicks(1038), new DateTime(2020, 8, 25, 16, 34, 18, 337, DateTimeKind.Unspecified).AddTicks(9815), "Manuel79@hotmail.com", "Augusta", "Weber" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 71,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2010, 2, 18, 18, 55, 44, 533, DateTimeKind.Unspecified).AddTicks(4546), new DateTime(2018, 7, 31, 7, 19, 33, 594, DateTimeKind.Unspecified).AddTicks(9385), "Wyatt.Connelly83@yahoo.com", "Joanne", "Stroman", 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 72,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1970, 9, 19, 10, 10, 52, 808, DateTimeKind.Unspecified).AddTicks(6684), new DateTime(1994, 10, 19, 18, 24, 25, 559, DateTimeKind.Unspecified).AddTicks(9816), "Alexis6@gmail.com", "Arvid", "Gerlach", 6 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 73,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1981, 3, 31, 22, 6, 45, 57, DateTimeKind.Unspecified).AddTicks(6933), new DateTime(2016, 11, 13, 14, 26, 51, 530, DateTimeKind.Unspecified).AddTicks(4538), "Winnifred_Mueller@hotmail.com", "Derrick", "Cummerata", 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 74,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1978, 12, 25, 9, 46, 10, 360, DateTimeKind.Unspecified).AddTicks(704), new DateTime(2003, 3, 31, 22, 5, 36, 445, DateTimeKind.Unspecified).AddTicks(345), "Elza61@hotmail.com", "Susie", "Weissnat", 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 75,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName" },
                values: new object[] { new DateTime(1973, 9, 1, 6, 50, 54, 80, DateTimeKind.Unspecified).AddTicks(6813), new DateTime(1983, 11, 16, 2, 11, 20, 798, DateTimeKind.Unspecified).AddTicks(9965), "Kathlyn_Adams63@hotmail.com", "Marlee", "Stroman" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 76,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1993, 2, 5, 12, 49, 3, 956, DateTimeKind.Unspecified).AddTicks(9223), new DateTime(2010, 4, 2, 20, 34, 50, 839, DateTimeKind.Unspecified).AddTicks(1283), "Delores.Prohaska@hotmail.com", "Lila", "Mante", 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 77,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1990, 1, 11, 21, 31, 43, 778, DateTimeKind.Unspecified).AddTicks(1166), new DateTime(2000, 1, 21, 14, 11, 5, 587, DateTimeKind.Unspecified).AddTicks(1502), "Rashawn_Conroy@yahoo.com", "Kristin", "Bayer", 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 78,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2013, 7, 6, 14, 1, 42, 515, DateTimeKind.Unspecified).AddTicks(4470), new DateTime(2021, 7, 6, 9, 16, 58, 851, DateTimeKind.Unspecified).AddTicks(225), "Russ29@gmail.com", "Sallie", "Little", 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 79,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1972, 11, 20, 3, 36, 26, 435, DateTimeKind.Unspecified).AddTicks(7554), new DateTime(2016, 10, 2, 15, 2, 31, 234, DateTimeKind.Unspecified).AddTicks(8366), "Zackary.Grant@hotmail.com", "Addie", "Dach", 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 80,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2008, 6, 12, 18, 6, 25, 483, DateTimeKind.Unspecified).AddTicks(9240), new DateTime(2017, 8, 12, 9, 52, 53, 238, DateTimeKind.Unspecified).AddTicks(1866), "Trever_Brakus73@gmail.com", "Lester", "Parker", 9 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "TeamName",
                table: "Teams",
                type: "nvarchar(max)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(40)",
                oldMaxLength: 40);

            migrationBuilder.AlterColumn<string>(
                name: "TaskName",
                table: "Tasks",
                type: "nvarchar(max)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(70)",
                oldMaxLength: 70);

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "ProjectName", "TeamId" },
                values: new object[] { 27, new DateTime(2019, 12, 16, 20, 13, 25, 973, DateTimeKind.Unspecified).AddTicks(7579), new DateTime(2021, 2, 27, 0, 9, 34, 616, DateTimeKind.Unspecified).AddTicks(6252), "Incredible Fresh Pizza", 3 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 9, new DateTime(2018, 10, 12, 17, 5, 0, 128, DateTimeKind.Unspecified).AddTicks(4779), new DateTime(2021, 11, 6, 17, 42, 0, 760, DateTimeKind.Unspecified).AddTicks(7051), "The slim & simple Maple Gaming Keyboard from Dev Byte comes with a sleek body and 7- Color RGB LED Back-lighting for smart functionality", "Practical Steel Fish", 2 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName" },
                values: new object[] { 67, new DateTime(2020, 1, 11, 2, 10, 15, 367, DateTimeKind.Unspecified).AddTicks(742), new DateTime(2021, 7, 5, 1, 8, 46, 703, DateTimeKind.Unspecified).AddTicks(5211), "The Football Is Good For Training And Recreational Purposes", "Fantastic Concrete Pants" });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 39, new DateTime(2018, 7, 30, 16, 54, 35, 671, DateTimeKind.Unspecified).AddTicks(7861), new DateTime(2019, 5, 19, 22, 17, 24, 713, DateTimeKind.Unspecified).AddTicks(8277), "The beautiful range of Apple Naturalé that has an exciting mix of natural ingredients. With the Goodness of 100% Natural Ingredients", "Ergonomic Frozen Shirt", 2 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 60, new DateTime(2019, 1, 14, 0, 34, 13, 424, DateTimeKind.Unspecified).AddTicks(5982), new DateTime(2021, 3, 12, 17, 36, 28, 273, DateTimeKind.Unspecified).AddTicks(1722), "The slim & simple Maple Gaming Keyboard from Dev Byte comes with a sleek body and 7- Color RGB LED Back-lighting for smart functionality", "Rustic Soft Towels", 4 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 33, new DateTime(2018, 6, 7, 6, 41, 46, 313, DateTimeKind.Unspecified).AddTicks(4500), new DateTime(2019, 3, 9, 6, 57, 2, 216, DateTimeKind.Unspecified).AddTicks(3791), "The Football Is Good For Training And Recreational Purposes", "Intelligent Granite Towels", 6 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "ProjectName", "TeamId" },
                values: new object[] { 50, new DateTime(2020, 12, 25, 8, 0, 21, 590, DateTimeKind.Unspecified).AddTicks(9295), new DateTime(2021, 9, 9, 3, 32, 19, 314, DateTimeKind.Unspecified).AddTicks(4173), "Intelligent Plastic Computer", 7 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 64, new DateTime(2018, 6, 19, 6, 15, 34, 673, DateTimeKind.Unspecified).AddTicks(8092), new DateTime(2019, 8, 14, 12, 17, 36, 998, DateTimeKind.Unspecified).AddTicks(264), "The Apollotech B340 is an affordable wireless mouse with reliable connectivity, 12 months battery life and modern design", "Gorgeous Steel Pizza", 7 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 49, new DateTime(2018, 10, 24, 8, 57, 35, 796, DateTimeKind.Unspecified).AddTicks(3886), new DateTime(2018, 11, 21, 2, 17, 32, 24, DateTimeKind.Unspecified).AddTicks(6753), "The slim & simple Maple Gaming Keyboard from Dev Byte comes with a sleek body and 7- Color RGB LED Back-lighting for smart functionality", "Unbranded Granite Mouse", 10 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 4, new DateTime(2019, 4, 29, 11, 5, 49, 511, DateTimeKind.Unspecified).AddTicks(4865), new DateTime(2020, 9, 14, 9, 2, 26, 578, DateTimeKind.Unspecified).AddTicks(7996), "The Apollotech B340 is an affordable wireless mouse with reliable connectivity, 12 months battery life and modern design", "Unbranded Rubber Pants", 1 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 3, new DateTime(2020, 2, 26, 23, 9, 47, 692, DateTimeKind.Unspecified).AddTicks(8973), new DateTime(2020, 11, 27, 1, 47, 29, 694, DateTimeKind.Unspecified).AddTicks(7062), "New ABC 13 9370, 13.3, 5th Gen CoreA5-8250U, 8GB RAM, 256GB SSD, power UHD Graphics, OS 10 Home, OS Office A & J 2016", "Refined Concrete Tuna", 10 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 41, new DateTime(2021, 4, 29, 10, 39, 2, 565, DateTimeKind.Unspecified).AddTicks(5327), new DateTime(2021, 8, 23, 14, 45, 30, 195, DateTimeKind.Unspecified).AddTicks(6132), "Boston's most advanced compression wear technology increases muscle oxygenation, stabilizes active muscles", "Tasty Soft Chips", 10 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 30, new DateTime(2020, 2, 24, 6, 0, 33, 532, DateTimeKind.Unspecified).AddTicks(5434), new DateTime(2020, 8, 1, 2, 48, 33, 961, DateTimeKind.Unspecified).AddTicks(1751), "The beautiful range of Apple Naturalé that has an exciting mix of natural ingredients. With the Goodness of 100% Natural Ingredients", "Fantastic Concrete Tuna", 9 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 29, new DateTime(2020, 6, 21, 14, 20, 29, 13, DateTimeKind.Unspecified).AddTicks(1111), new DateTime(2020, 11, 6, 22, 31, 17, 733, DateTimeKind.Unspecified).AddTicks(7510), "Ergonomic executive chair upholstered in bonded black leather and PVC padded seat and back for all-day comfort and support", "Ergonomic Steel Ball", 6 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 3, new DateTime(2019, 1, 28, 23, 12, 2, 8, DateTimeKind.Unspecified).AddTicks(9412), new DateTime(2021, 9, 8, 12, 43, 45, 752, DateTimeKind.Unspecified).AddTicks(5881), "Carbonite web goalkeeper gloves are ergonomically designed to give easy fit", "Handcrafted Plastic Towels", 10 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 80, new DateTime(2019, 10, 25, 20, 16, 49, 413, DateTimeKind.Unspecified).AddTicks(860), new DateTime(2019, 10, 28, 19, 12, 55, 789, DateTimeKind.Unspecified).AddTicks(1430), "New range of formal shirts are designed keeping you in mind. With fits and styling that will make you stand apart", "Refined Soft Pants", 6 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 29, new DateTime(2018, 7, 30, 15, 25, 55, 360, DateTimeKind.Unspecified).AddTicks(3552), new DateTime(2018, 8, 8, 5, 4, 21, 388, DateTimeKind.Unspecified).AddTicks(9638), "New range of formal shirts are designed keeping you in mind. With fits and styling that will make you stand apart", "Handcrafted Soft Chips", 5 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 57, new DateTime(2018, 4, 6, 10, 59, 57, 914, DateTimeKind.Unspecified).AddTicks(2838), new DateTime(2019, 11, 9, 9, 39, 39, 856, DateTimeKind.Unspecified).AddTicks(1816), "Andy shoes are designed to keeping in mind durability as well as trends, the most stylish range of shoes & sandals", "Awesome Cotton Computer", 6 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 30, new DateTime(2019, 9, 26, 20, 42, 44, 965, DateTimeKind.Unspecified).AddTicks(5702), new DateTime(2020, 8, 7, 1, 3, 32, 621, DateTimeKind.Unspecified).AddTicks(7287), "New ABC 13 9370, 13.3, 5th Gen CoreA5-8250U, 8GB RAM, 256GB SSD, power UHD Graphics, OS 10 Home, OS Office A & J 2016", "Ergonomic Metal Chips", 4 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 38, new DateTime(2018, 11, 23, 8, 31, 25, 947, DateTimeKind.Unspecified).AddTicks(197), new DateTime(2020, 6, 15, 21, 12, 2, 278, DateTimeKind.Unspecified).AddTicks(485), "Carbonite web goalkeeper gloves are ergonomically designed to give easy fit", "Unbranded Soft Chair", 9 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 42, new DateTime(2021, 2, 28, 10, 58, 41, 326, DateTimeKind.Unspecified).AddTicks(9090), new DateTime(2021, 4, 29, 23, 54, 40, 489, DateTimeKind.Unspecified).AddTicks(1027), "The Football Is Good For Training And Recreational Purposes", "Awesome Concrete Pants", 9 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 70, new DateTime(2018, 9, 20, 3, 9, 9, 990, DateTimeKind.Unspecified).AddTicks(8902), new DateTime(2019, 8, 11, 14, 51, 11, 109, DateTimeKind.Unspecified).AddTicks(2210), "Carbonite web goalkeeper gloves are ergonomically designed to give easy fit", "Rustic Granite Chair", 4 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 47, new DateTime(2020, 3, 27, 13, 33, 28, 387, DateTimeKind.Unspecified).AddTicks(4664), new DateTime(2020, 9, 28, 22, 10, 12, 6, DateTimeKind.Unspecified).AddTicks(5833), "The slim & simple Maple Gaming Keyboard from Dev Byte comes with a sleek body and 7- Color RGB LED Back-lighting for smart functionality", "Handcrafted Frozen Chicken", 4 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName" },
                values: new object[] { 36, new DateTime(2019, 11, 18, 21, 38, 14, 958, DateTimeKind.Unspecified).AddTicks(1692), new DateTime(2021, 5, 30, 18, 33, 33, 428, DateTimeKind.Unspecified).AddTicks(9915), "Carbonite web goalkeeper gloves are ergonomically designed to give easy fit", "Incredible Rubber Salad" });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 19, new DateTime(2021, 3, 5, 23, 12, 8, 908, DateTimeKind.Unspecified).AddTicks(4576), new DateTime(2021, 5, 4, 12, 50, 17, 84, DateTimeKind.Unspecified).AddTicks(9730), "The Nagasaki Lander is the trademarked name of several series of Nagasaki sport bikes, that started with the 1984 ABC800J", "Rustic Rubber Table", 6 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 26, new DateTime(2019, 11, 4, 22, 59, 59, 58, DateTimeKind.Unspecified).AddTicks(6965), new DateTime(2021, 1, 7, 16, 4, 59, 589, DateTimeKind.Unspecified).AddTicks(3403), "New range of formal shirts are designed keeping you in mind. With fits and styling that will make you stand apart", "Licensed Soft Car", 9 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 12, new DateTime(2021, 4, 11, 23, 38, 10, 888, DateTimeKind.Unspecified).AddTicks(5223), new DateTime(2021, 6, 2, 6, 48, 7, 388, DateTimeKind.Unspecified).AddTicks(3230), "The Football Is Good For Training And Recreational Purposes", "Awesome Soft Sausages", 10 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { new DateTime(2020, 9, 12, 3, 17, 21, 267, DateTimeKind.Unspecified).AddTicks(9675), new DateTime(2021, 6, 1, 17, 54, 13, 514, DateTimeKind.Unspecified).AddTicks(4314), "Andy shoes are designed to keeping in mind durability as well as trends, the most stylish range of shoes & sandals", "Generic Steel Bike", 3 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 74, new DateTime(2018, 2, 27, 1, 21, 26, 708, DateTimeKind.Unspecified).AddTicks(7175), new DateTime(2018, 9, 11, 1, 44, 21, 578, DateTimeKind.Unspecified).AddTicks(7657), "The slim & simple Maple Gaming Keyboard from Dev Byte comes with a sleek body and 7- Color RGB LED Back-lighting for smart functionality", "Small Plastic Shoes", 8 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 22, new DateTime(2021, 5, 31, 9, 30, 15, 558, DateTimeKind.Unspecified).AddTicks(6663), new DateTime(2021, 8, 21, 3, 26, 32, 272, DateTimeKind.Unspecified).AddTicks(1219), "New range of formal shirts are designed keeping you in mind. With fits and styling that will make you stand apart", "Small Granite Shoes", 9 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 50, new DateTime(2020, 2, 19, 14, 6, 31, 624, DateTimeKind.Unspecified).AddTicks(5939), new DateTime(2020, 6, 11, 6, 10, 1, 545, DateTimeKind.Unspecified).AddTicks(8413), "Boston's most advanced compression wear technology increases muscle oxygenation, stabilizes active muscles", "Ergonomic Plastic Car", 10 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 25, new DateTime(2018, 11, 17, 22, 30, 0, 532, DateTimeKind.Unspecified).AddTicks(4999), new DateTime(2020, 6, 8, 3, 48, 13, 746, DateTimeKind.Unspecified).AddTicks(8676), "New ABC 13 9370, 13.3, 5th Gen CoreA5-8250U, 8GB RAM, 256GB SSD, power UHD Graphics, OS 10 Home, OS Office A & J 2016", "Generic Concrete Bacon", 10 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 33, new DateTime(2020, 4, 4, 9, 47, 16, 373, DateTimeKind.Unspecified).AddTicks(8902), new DateTime(2021, 7, 24, 12, 53, 34, 813, DateTimeKind.Unspecified).AddTicks(5865), "Ergonomic executive chair upholstered in bonded black leather and PVC padded seat and back for all-day comfort and support", "Tasty Granite Bacon", 3 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 42, new DateTime(2021, 5, 1, 19, 34, 59, 793, DateTimeKind.Unspecified).AddTicks(2563), new DateTime(2021, 10, 20, 15, 16, 0, 685, DateTimeKind.Unspecified).AddTicks(9378), "Boston's most advanced compression wear technology increases muscle oxygenation, stabilizes active muscles", "Handcrafted Soft Hat", 2 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName" },
                values: new object[] { 19, new DateTime(2021, 5, 21, 10, 15, 33, 270, DateTimeKind.Unspecified).AddTicks(6416), new DateTime(2021, 5, 24, 12, 0, 23, 439, DateTimeKind.Unspecified).AddTicks(7759), "Carbonite web goalkeeper gloves are ergonomically designed to give easy fit", "Ergonomic Soft Pizza" });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "ProjectName", "TeamId" },
                values: new object[] { 75, new DateTime(2020, 11, 28, 2, 22, 10, 70, DateTimeKind.Unspecified).AddTicks(6703), new DateTime(2020, 12, 9, 15, 0, 51, 753, DateTimeKind.Unspecified).AddTicks(4084), "Handcrafted Plastic Tuna", 9 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 1, new DateTime(2018, 2, 19, 0, 48, 13, 665, DateTimeKind.Unspecified).AddTicks(7109), new DateTime(2021, 3, 31, 22, 54, 56, 476, DateTimeKind.Unspecified).AddTicks(2908), "New range of formal shirts are designed keeping you in mind. With fits and styling that will make you stand apart", "Intelligent Frozen Bacon", 10 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 36, new DateTime(2020, 4, 14, 4, 56, 40, 765, DateTimeKind.Unspecified).AddTicks(1903), new DateTime(2021, 2, 14, 12, 18, 33, 678, DateTimeKind.Unspecified).AddTicks(4780), "New range of formal shirts are designed keeping you in mind. With fits and styling that will make you stand apart", "Handcrafted Plastic Chips", 10 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 9, new DateTime(2018, 12, 25, 16, 17, 26, 458, DateTimeKind.Unspecified).AddTicks(2552), new DateTime(2021, 8, 31, 0, 53, 55, 927, DateTimeKind.Unspecified).AddTicks(6899), "The Football Is Good For Training And Recreational Purposes", "Gorgeous Steel Car", 7 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 48, new DateTime(2020, 12, 22, 20, 2, 27, 538, DateTimeKind.Unspecified).AddTicks(6979), new DateTime(2021, 10, 4, 17, 36, 45, 408, DateTimeKind.Unspecified).AddTicks(4393), "The automobile layout consists of a front-engine design, with transaxle-type transmissions mounted at the rear of the engine and four wheel drive", "Sleek Soft Shirt", 10 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 4, 25, 0, 45, 38, 153, DateTimeKind.Unspecified).AddTicks(5691), "Sequi placeat quae repudiandae rerum sunt ratione error quisquam rerum dolores praesentium ut.", new DateTime(2020, 5, 20, 17, 33, 43, 264, DateTimeKind.Unspecified).AddTicks(5880), "Molestias qui error nemo neque.", 54, 32, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2018, 8, 6, 17, 36, 49, 878, DateTimeKind.Unspecified).AddTicks(3165), "Et neque et earum corrupti sunt quia sunt labore molestiae omnis inventore.", new DateTime(2018, 8, 19, 6, 4, 4, 731, DateTimeKind.Unspecified).AddTicks(3756), "Nostrum numquam omnis recusandae tempora.", 19, 29 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId" },
                values: new object[] { new DateTime(2020, 8, 24, 13, 54, 49, 880, DateTimeKind.Unspecified).AddTicks(9781), "Est qui accusamus saepe qui accusamus.", new DateTime(2020, 9, 22, 13, 12, 37, 50, DateTimeKind.Unspecified).AddTicks(6101), "Laudantium praesentium rem hic.", 12 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 5, 6, 3, 18, 44, 900, DateTimeKind.Unspecified).AddTicks(4036), "Non quis distinctio accusamus sed et eos suscipit dicta itaque.", new DateTime(2021, 8, 30, 17, 7, 51, 29, DateTimeKind.Unspecified).AddTicks(8227), "Molestiae minima fuga incidunt.", 24, 40, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 4, 28, 6, 4, 42, 428, DateTimeKind.Unspecified).AddTicks(576), "Velit consequatur eius libero quia sapiente labore sunt qui eveniet architecto.", new DateTime(2021, 5, 1, 14, 5, 55, 83, DateTimeKind.Unspecified).AddTicks(4718), "Consequuntur eius ex.", 8, 25 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 1, 29, 14, 54, 23, 173, DateTimeKind.Unspecified).AddTicks(49), "Iste enim recusandae qui impedit nisi.", new DateTime(2021, 3, 22, 5, 31, 58, 413, DateTimeKind.Unspecified).AddTicks(917), "Dolorem fugit quisquam.", 5, 3, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 5, 22, 9, 33, 38, 507, DateTimeKind.Unspecified).AddTicks(3144), "Asperiores perspiciatis sunt fuga nostrum sint amet.", new DateTime(2021, 5, 23, 13, 53, 25, 197, DateTimeKind.Unspecified).AddTicks(1245), "Laborum ex sed.", 20, 35 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 3, 17, 0, 47, 48, 975, DateTimeKind.Unspecified).AddTicks(9778), "Quidem laborum rerum dolores quis dicta.", new DateTime(2021, 4, 20, 22, 49, 21, 383, DateTimeKind.Unspecified).AddTicks(6960), "Adipisci sapiente.", 63, 25, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 11, 25, 6, 10, 43, 957, DateTimeKind.Unspecified).AddTicks(4021), "Iste velit rerum exercitationem dolorum ut explicabo porro ipsa quia quasi sed labore.", new DateTime(2020, 5, 8, 2, 8, 14, 288, DateTimeKind.Unspecified).AddTicks(4850), "Perferendis suscipit.", 67, 32 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 9, 8, 20, 40, 56, 943, DateTimeKind.Unspecified).AddTicks(4851), "Non tenetur iusto sunt qui sunt pariatur quae expedita consequatur dolorem magnam eveniet nisi.", new DateTime(2020, 9, 20, 20, 23, 7, 839, DateTimeKind.Unspecified).AddTicks(9764), "Quidem alias porro fuga accusamus.", 30, 23, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 4, 27, 17, 30, 4, 799, DateTimeKind.Unspecified).AddTicks(246), "Autem veritatis rerum quaerat illo cumque consequatur quo temporibus architecto reprehenderit eligendi qui.", new DateTime(2021, 4, 28, 21, 59, 14, 13, DateTimeKind.Unspecified).AddTicks(9709), "Aut autem.", 18, 27, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 2, 12, 7, 47, 2, 690, DateTimeKind.Unspecified).AddTicks(7243), "Ea rerum dolores reprehenderit explicabo odit sed magnam et voluptatem omnis fugit inventore molestias.", new DateTime(2020, 3, 23, 17, 1, 53, 485, DateTimeKind.Unspecified).AddTicks(4270), "Qui est.", 68, 32, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 10, 25, 20, 18, 35, 140, DateTimeKind.Unspecified).AddTicks(6571), "Est voluptatibus aut facere veniam laboriosam repellat qui autem et.", new DateTime(2019, 10, 28, 2, 40, 10, 916, DateTimeKind.Unspecified).AddTicks(9907), "Eaque quia.", 54, 16, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 3, 19, 8, 30, 40, 752, DateTimeKind.Unspecified).AddTicks(3269), "Qui rem animi quidem non sed quam consequatur id.", new DateTime(2021, 4, 8, 12, 48, 50, 203, DateTimeKind.Unspecified).AddTicks(1138), "Nihil quisquam sed nihil.", 68, 25, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2018, 10, 24, 23, 3, 4, 891, DateTimeKind.Unspecified).AddTicks(2010), "Et exercitationem omnis ab libero pariatur repellendus veritatis magni quod eos ducimus quas facere.", new DateTime(2018, 12, 1, 19, 31, 19, 557, DateTimeKind.Unspecified).AddTicks(7752), "Numquam aliquid fugiat.", 10, 6, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 10, 27, 15, 59, 52, 842, DateTimeKind.Unspecified).AddTicks(77), "Et error et aliquam iure ex rerum dolorum assumenda quasi.", new DateTime(2019, 10, 28, 13, 25, 22, 902, DateTimeKind.Unspecified).AddTicks(9174), "Dolores culpa sint illo praesentium.", 60, 16 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 10, 26, 23, 53, 36, 224, DateTimeKind.Unspecified).AddTicks(1966), "Sequi odio voluptatum exercitationem laboriosam ut itaque ut beatae omnis architecto.", new DateTime(2019, 10, 27, 9, 33, 42, 118, DateTimeKind.Unspecified).AddTicks(7590), "Nostrum quasi.", 34, 16, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2020, 4, 20, 21, 25, 12, 229, DateTimeKind.Unspecified).AddTicks(1237), "Repellendus error aut maiores et consequuntur sunt ut voluptas.", new DateTime(2021, 1, 9, 23, 11, 42, 734, DateTimeKind.Unspecified).AddTicks(2449), "Qui debitis.", 37, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 2, 1, 5, 7, 8, 281, DateTimeKind.Unspecified).AddTicks(4848), "Aliquid qui sed consequatur voluptas optio eligendi cum fugiat impedit consequatur.", new DateTime(2019, 6, 18, 16, 32, 24, 745, DateTimeKind.Unspecified).AddTicks(2808), "Alias tenetur ut.", 66, 22, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2020, 12, 6, 1, 28, 21, 210, DateTimeKind.Unspecified).AddTicks(6706), "Officia voluptates et repellendus repellendus provident magnam veniam eius laudantium tenetur natus cupiditate.", new DateTime(2020, 12, 6, 4, 53, 5, 164, DateTimeKind.Unspecified).AddTicks(8492), "Natus quos incidunt numquam.", 24, 36 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 4, 6, 3, 27, 27, 192, DateTimeKind.Unspecified).AddTicks(1617), "Atque repellat officiis consequuntur rerum porro dignissimos laudantium eaque consequatur ut et non quod.", new DateTime(2020, 10, 15, 10, 50, 49, 334, DateTimeKind.Unspecified).AddTicks(9355), "Qui aliquam rerum.", 45, 3, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 4, 20, 8, 13, 44, 277, DateTimeKind.Unspecified).AddTicks(9242), "Illum ratione fugiat occaecati sed at omnis iste hic rerum qui repellendus voluptates ea.", new DateTime(2021, 5, 4, 12, 34, 34, 180, DateTimeKind.Unspecified).AddTicks(9177), "Ut sunt iusto.", 74, 25 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 5, 12, 6, 46, 52, 840, DateTimeKind.Unspecified).AddTicks(4716), "Sit voluptatibus autem numquam doloremque nihil voluptatem inventore repudiandae natus.", new DateTime(2021, 9, 17, 9, 33, 17, 283, DateTimeKind.Unspecified).AddTicks(2353), "Voluptatibus aperiam.", 75, 40, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 5, 1, 14, 34, 39, 328, DateTimeKind.Unspecified).AddTicks(148), "Voluptatem sequi ad alias facere consequatur maiores quo et expedita et et repellendus est.", new DateTime(2021, 6, 24, 3, 25, 32, 791, DateTimeKind.Unspecified).AddTicks(6260), "Sit est maiores.", 59, 33 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 2, 16, 11, 33, 17, 505, DateTimeKind.Unspecified).AddTicks(8192), "Sed nobis pariatur id minus qui.", new DateTime(2021, 9, 2, 20, 40, 11, 949, DateTimeKind.Unspecified).AddTicks(5129), "Quibusdam necessitatibus tempore modi molestiae.", 57, 40, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2018, 11, 2, 19, 44, 38, 700, DateTimeKind.Unspecified).AddTicks(8213), "Sequi aliquam ipsam beatae cupiditate aut veritatis iure iusto.", new DateTime(2018, 12, 6, 14, 48, 23, 558, DateTimeKind.Unspecified).AddTicks(3829), "Laudantium eius quisquam.", 30, 22, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 3, 21, 21, 0, 29, 188, DateTimeKind.Unspecified).AddTicks(8985), "Ullam et dolorem molestiae distinctio voluptates libero dolores tenetur eum ducimus quia.", new DateTime(2021, 4, 19, 16, 53, 9, 786, DateTimeKind.Unspecified).AddTicks(1218), "Ab architecto expedita.", 34, 21, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 10, 22, 0, 45, 57, 909, DateTimeKind.Unspecified).AddTicks(7350), "Amet quo eos repellat qui autem expedita ullam.", new DateTime(2021, 3, 28, 16, 25, 6, 894, DateTimeKind.Unspecified).AddTicks(1546), "Nulla sunt ipsam dolorum laudantium.", 79, 33, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2020, 5, 14, 13, 25, 50, 168, DateTimeKind.Unspecified).AddTicks(3915), "Odit iste doloribus quis ex consequatur omnis quidem.", new DateTime(2020, 6, 18, 17, 57, 23, 115, DateTimeKind.Unspecified).AddTicks(1185), "Expedita ipsum vero.", 31, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 8, 4, 4, 37, 58, 860, DateTimeKind.Unspecified).AddTicks(2760), "Libero tempore culpa quia ratione quis.", new DateTime(2021, 9, 19, 10, 9, 22, 742, DateTimeKind.Unspecified).AddTicks(4533), "Ea et id hic.", 7, 40, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2018, 7, 22, 11, 25, 18, 731, DateTimeKind.Unspecified).AddTicks(4739), "Et enim illum ex vero labore amet nemo sint quasi.", new DateTime(2018, 7, 31, 0, 58, 42, 111, DateTimeKind.Unspecified).AddTicks(5642), "Cum porro consequatur.", 10, 29, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 7, 2, 7, 16, 43, 824, DateTimeKind.Unspecified).AddTicks(8154), "Eum voluptas repellat consequatur ullam aut rerum maiores saepe iste.", new DateTime(2020, 8, 2, 19, 52, 10, 366, DateTimeKind.Unspecified).AddTicks(7339), "Impedit quia amet quidem.", 45, 15, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 5, 6, 4, 35, 13, 712, DateTimeKind.Unspecified).AddTicks(2922), "Beatae et voluptas sapiente et harum corrupti perspiciatis omnis non consequatur.", new DateTime(2021, 9, 7, 3, 2, 15, 81, DateTimeKind.Unspecified).AddTicks(4437), "Mollitia amet et atque enim.", 80, 40, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2020, 9, 22, 5, 30, 23, 761, DateTimeKind.Unspecified).AddTicks(6268), "Aliquid omnis qui tempore error tempore fugit itaque possimus sequi.", new DateTime(2020, 10, 15, 3, 13, 24, 959, DateTimeKind.Unspecified).AddTicks(7449), "Voluptatem corporis quia dolorum.", 42, 11 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 8, 26, 0, 7, 50, 613, DateTimeKind.Unspecified).AddTicks(7332), "Consequatur voluptas alias quia aut vitae aut id et.", new DateTime(2021, 8, 26, 12, 37, 17, 291, DateTimeKind.Unspecified).AddTicks(2858), "Ex magnam dolores.", 46, 7, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 8, 11, 12, 17, 4, 365, DateTimeKind.Unspecified).AddTicks(5501), "Illum molestiae et est numquam qui.", new DateTime(2019, 12, 29, 18, 13, 41, 743, DateTimeKind.Unspecified).AddTicks(3315), "Veritatis vel rerum.", 18, 32, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2020, 2, 2, 2, 13, 7, 288, DateTimeKind.Unspecified).AddTicks(7994), "Voluptate optio deleniti similique molestias nemo officia aut nemo est velit ab.", new DateTime(2020, 3, 6, 21, 25, 47, 677, DateTimeKind.Unspecified).AddTicks(5049), "Illo dolorem enim excepturi excepturi.", 55, 19 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 5, 28, 3, 33, 4, 232, DateTimeKind.Unspecified).AddTicks(1322), "Harum qui ea aliquam consequuntur quia voluptas ab temporibus.", new DateTime(2021, 7, 18, 22, 0, 33, 1, DateTimeKind.Unspecified).AddTicks(2820), "Iure et repellendus ducimus quia.", 20, 12, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 4, 5, 22, 5, 30, 335, DateTimeKind.Unspecified).AddTicks(2593), "Quam consequatur ut quas officiis ratione aliquid quia dignissimos doloribus.", new DateTime(2021, 7, 5, 18, 29, 41, 702, DateTimeKind.Unspecified).AddTicks(9103), "Nemo amet labore quam aut.", 30, 39 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2018, 10, 11, 9, 0, 15, 409, DateTimeKind.Unspecified).AddTicks(2769), "Quia maiores nesciunt officia et repellat vitae aut.", new DateTime(2019, 7, 1, 19, 43, 20, 456, DateTimeKind.Unspecified).AddTicks(7544), "Sit nulla amet et ratione.", 17, 22 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 41,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 4, 28, 3, 59, 0, 130, DateTimeKind.Unspecified).AddTicks(8889), "Aut esse non nisi exercitationem aut qui corrupti.", new DateTime(2020, 7, 18, 5, 15, 31, 2, DateTimeKind.Unspecified).AddTicks(4485), "Eos facere.", 32, 13, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 42,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2020, 9, 14, 18, 35, 56, 461, DateTimeKind.Unspecified).AddTicks(1286), "Eum minima nisi veniam porro aliquid ipsa.", new DateTime(2020, 9, 21, 5, 10, 36, 550, DateTimeKind.Unspecified).AddTicks(884), "Amet ea.", 20, 23 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 43,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 6, 29, 14, 27, 42, 30, DateTimeKind.Unspecified).AddTicks(7068), "Molestias eius sint debitis aut et aut.", new DateTime(2020, 7, 6, 17, 54, 41, 46, DateTimeKind.Unspecified).AddTicks(4640), "Incidunt voluptas in officia.", 13, 5, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 44,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 11, 10, 7, 16, 38, 504, DateTimeKind.Unspecified).AddTicks(2760), "Aut quaerat veritatis necessitatibus omnis pariatur vitae ad magnam.", new DateTime(2021, 1, 5, 1, 49, 16, 669, DateTimeKind.Unspecified).AddTicks(5349), "Debitis quas illo nemo eos.", 65, 24, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 45,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 5, 1, 2, 41, 890, DateTimeKind.Unspecified).AddTicks(2198), "Hic corporis repudiandae sit mollitia voluptates commodi sed.", new DateTime(2021, 8, 10, 8, 55, 45, 793, DateTimeKind.Unspecified).AddTicks(7763), "Soluta qui.", 54, 12, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 46,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 5, 16, 7, 18, 49, 94, DateTimeKind.Unspecified).AddTicks(4384), "Qui accusantium saepe nesciunt pariatur provident fugit aperiam.", new DateTime(2020, 5, 21, 9, 58, 46, 374, DateTimeKind.Unspecified).AddTicks(7477), "Ut dolorem et hic ducimus.", 29, 31, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 47,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2018, 7, 31, 11, 39, 29, 952, DateTimeKind.Unspecified).AddTicks(7678), "Cupiditate et natus tenetur optio sint sint.", new DateTime(2018, 8, 1, 22, 18, 57, 999, DateTimeKind.Unspecified).AddTicks(2919), "Ex iste et autem nam.", 6, 17, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 48,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 4, 26, 11, 27, 31, 674, DateTimeKind.Unspecified).AddTicks(9237), "Velit possimus sit et autem voluptas repellendus earum ex vel quis vero similique nisi.", new DateTime(2021, 5, 7, 8, 53, 57, 764, DateTimeKind.Unspecified).AddTicks(6293), "Vel aspernatur.", 65, 27, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 49,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 4, 8, 15, 26, 38, 709, DateTimeKind.Unspecified).AddTicks(6818), "Ullam rerum neque nihil delectus unde quo et ratione quod praesentium animi.", new DateTime(2021, 2, 22, 18, 59, 36, 933, DateTimeKind.Unspecified).AddTicks(269), "Totam earum laudantium sit et.", 48, 15, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 50,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 1, 20, 18, 10, 26, 641, DateTimeKind.Unspecified).AddTicks(3075), "Dignissimos commodi dolor itaque aut velit blanditiis rem exercitationem.", new DateTime(2020, 2, 2, 6, 6, 41, 105, DateTimeKind.Unspecified).AddTicks(5350), "Ratione inventore fugiat.", 12, 1, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 51,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 7, 17, 0, 59, 53, 550, DateTimeKind.Unspecified).AddTicks(3250), "Molestiae consequatur est itaque est dignissimos pariatur.", new DateTime(2020, 9, 18, 0, 41, 12, 289, DateTimeKind.Unspecified).AddTicks(4030), "Est esse dolorem ratione eos.", 71, 14, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 52,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 12, 7, 16, 28, 25, 907, DateTimeKind.Unspecified).AddTicks(7374), "Aut cum ducimus praesentium ratione minima neque enim quo hic.", new DateTime(2020, 12, 9, 0, 50, 54, 751, DateTimeKind.Unspecified).AddTicks(9536), "Adipisci et molestiae accusantium sed.", 16, 36, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 53,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2020, 6, 14, 20, 58, 12, 653, DateTimeKind.Unspecified).AddTicks(1669), "Est magni ratione illum saepe velit consequuntur hic reiciendis quisquam reprehenderit quis.", new DateTime(2020, 9, 21, 7, 27, 51, 171, DateTimeKind.Unspecified).AddTicks(8831), "Asperiores sunt repellat in.", 73, 26 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 54,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2018, 9, 16, 5, 51, 14, 696, DateTimeKind.Unspecified).AddTicks(7022), "Magnam et est amet quo cum aut fugiat doloremque.", new DateTime(2018, 12, 20, 17, 48, 8, 12, DateTimeKind.Unspecified).AddTicks(4324), "Distinctio dignissimos magni aut.", 25, 6, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 55,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 8, 16, 6, 0, 34, 156, DateTimeKind.Unspecified).AddTicks(1152), "Tempora molestias aperiam minima provident tempore ipsa sunt omnis suscipit a et mollitia.", new DateTime(2021, 8, 17, 10, 45, 3, 955, DateTimeKind.Unspecified).AddTicks(711), "Aliquam beatae eaque commodi.", 2, 30 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 56,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2020, 3, 8, 20, 37, 14, 412, DateTimeKind.Unspecified).AddTicks(6614), "Sequi debitis autem ut quia voluptatem veniam qui ea.", new DateTime(2020, 5, 18, 4, 38, 8, 990, DateTimeKind.Unspecified).AddTicks(9549), "Ad quia necessitatibus.", 67, 31 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 57,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 10, 20, 5, 48, 10, 51, DateTimeKind.Unspecified).AddTicks(8288), "Veniam eligendi dolor consequuntur est consequatur voluptate eligendi aut veritatis et sunt.", new DateTime(2021, 5, 5, 14, 47, 56, 55, DateTimeKind.Unspecified).AddTicks(5519), "Et eaque harum.", 33, 28, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 58,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 5, 19, 17, 56, 32, 933, DateTimeKind.Unspecified).AddTicks(6912), "Omnis eos dignissimos sunt voluptatem quia officiis commodi autem rerum praesentium est.", new DateTime(2021, 5, 20, 0, 54, 46, 798, DateTimeKind.Unspecified).AddTicks(7427), "Nihil accusamus dolorem ut.", 13, 27, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 59,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 3, 24, 18, 15, 4, 859, DateTimeKind.Unspecified).AddTicks(6857), "Dolorem animi hic ut repudiandae suscipit omnis saepe perferendis itaque.", new DateTime(2020, 6, 1, 7, 52, 22, 467, DateTimeKind.Unspecified).AddTicks(6026), "Sunt enim occaecati omnis cum.", 21, 26, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 60,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 31, 5, 9, 35, 512, DateTimeKind.Unspecified).AddTicks(1655), "Qui voluptatum ut est porro perspiciatis odit magnam.", new DateTime(2021, 8, 12, 4, 50, 3, 287, DateTimeKind.Unspecified).AddTicks(1461), "Eos cumque magnam ducimus.", 79, 40, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 61,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 1, 22, 2, 15, 29, 90, DateTimeKind.Unspecified).AddTicks(6730), "Aut similique deserunt minus aliquid atque.", new DateTime(2021, 1, 22, 10, 4, 19, 231, DateTimeKind.Unspecified).AddTicks(1307), "Unde ipsum et ducimus.", 11, 28, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 62,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2018, 3, 15, 3, 36, 11, 272, DateTimeKind.Unspecified).AddTicks(1813), "Voluptatem adipisci autem expedita rerum voluptatem.", new DateTime(2018, 5, 17, 8, 22, 25, 672, DateTimeKind.Unspecified).AddTicks(7331), "Cupiditate incidunt labore explicabo quia.", 40, 29, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 63,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2020, 5, 3, 21, 5, 34, 841, DateTimeKind.Unspecified).AddTicks(4025), "Distinctio eos maxime omnis neque accusantium dolorum animi magnam vero quam.", new DateTime(2020, 7, 29, 20, 59, 26, 654, DateTimeKind.Unspecified).AddTicks(454), "In incidunt magnam non.", 35, 10 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 64,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 4, 14, 13, 40, 59, 264, DateTimeKind.Unspecified).AddTicks(677), "Tempora molestiae sunt doloremque dolores ea nisi et.", new DateTime(2020, 5, 15, 10, 43, 26, 774, DateTimeKind.Unspecified).AddTicks(6950), "Qui aliquam.", 26, 32, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 65,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 1, 22, 19, 51, 13, 552, DateTimeKind.Unspecified).AddTicks(8103), "Ipsum ipsum illo reprehenderit neque veniam id.", new DateTime(2019, 2, 4, 17, 39, 8, 253, DateTimeKind.Unspecified).AddTicks(6900), "Culpa a vel dolores.", 23, 6, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 66,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 1, 6, 13, 32, 1, 645, DateTimeKind.Unspecified).AddTicks(318), "Quo inventore omnis nobis amet consequatur est tenetur at recusandae quis.", new DateTime(2021, 6, 9, 16, 26, 3, 919, DateTimeKind.Unspecified).AddTicks(5657), "Repellat sapiente laboriosam minus.", 35, 7, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 67,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 4, 14, 12, 34, 34, 553, DateTimeKind.Unspecified).AddTicks(8184), "Voluptate placeat saepe enim qui mollitia excepturi quia voluptatem similique.", new DateTime(2021, 6, 11, 7, 9, 54, 243, DateTimeKind.Unspecified).AddTicks(9291), "Beatae ipsum.", 54, 15, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 68,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 5, 27, 22, 56, 17, 323, DateTimeKind.Unspecified).AddTicks(9651), "Maxime non et libero nulla praesentium ratione ut.", new DateTime(2021, 6, 1, 16, 5, 38, 319, DateTimeKind.Unspecified).AddTicks(8612), "Qui corrupti maiores.", 14, 27, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 69,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2020, 5, 9, 9, 37, 46, 305, DateTimeKind.Unspecified).AddTicks(8344), "Corrupti tenetur deserunt ullam quia eligendi veniam.", new DateTime(2020, 9, 2, 3, 9, 0, 835, DateTimeKind.Unspecified).AddTicks(5012), "Nesciunt delectus.", 31, 24 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 70,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 5, 11, 17, 37, 4, 59, DateTimeKind.Unspecified).AddTicks(3948), "Molestiae temporibus adipisci ipsam modi nihil.", new DateTime(2020, 9, 23, 3, 16, 37, 295, DateTimeKind.Unspecified).AddTicks(1425), "Aperiam mollitia.", 4, 11, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 71,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 5, 21, 19, 6, 2, 838, DateTimeKind.Unspecified).AddTicks(7585), "Enim et provident reprehenderit hic nihil occaecati ratione dicta rerum voluptatem nulla.", new DateTime(2021, 5, 22, 15, 46, 17, 772, DateTimeKind.Unspecified).AddTicks(9437), "Laudantium sequi est ut.", 64, 35, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 72,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 5, 20, 18, 0, 23, 141, DateTimeKind.Unspecified).AddTicks(2902), "Rerum et est qui voluptatem est facilis et sed aut distinctio.", new DateTime(2021, 5, 26, 16, 43, 27, 731, DateTimeKind.Unspecified).AddTicks(3776), "Voluptate sapiente.", 6, 3, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 73,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 4, 24, 1, 38, 45, 4, DateTimeKind.Unspecified).AddTicks(5131), "Ut sed id quam numquam accusamus suscipit.", new DateTime(2021, 5, 4, 18, 14, 11, 556, DateTimeKind.Unspecified).AddTicks(6226), "Reiciendis consequatur cum ipsum.", 5, 27, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 74,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 31, 15, 44, 28, 927, DateTimeKind.Unspecified).AddTicks(8066), "Est debitis sint ut beatae corrupti dolor ipsum perspiciatis quae aliquam sunt ut eaque.", new DateTime(2021, 8, 12, 16, 45, 53, 913, DateTimeKind.Unspecified).AddTicks(5388), "Voluptas nulla magnam.", 46, 12, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 75,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 11, 18, 15, 53, 55, 881, DateTimeKind.Unspecified).AddTicks(49), "Saepe quam autem harum officia nostrum eius qui illo.", new DateTime(2020, 12, 20, 21, 6, 37, 923, DateTimeKind.Unspecified).AddTicks(8358), "Id iusto est exercitationem.", 37, 24, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 76,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 6, 16, 3, 43, 50, 301, DateTimeKind.Unspecified).AddTicks(4457), "Qui est sed maiores sunt occaecati.", new DateTime(2020, 6, 30, 20, 22, 39, 143, DateTimeKind.Unspecified).AddTicks(4096), "Dolorem dolore tempore dolorem.", 28, 13, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 77,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2018, 11, 6, 7, 39, 17, 80, DateTimeKind.Unspecified).AddTicks(3227), "Et doloribus ut tenetur possimus eaque rerum.", new DateTime(2018, 11, 18, 11, 4, 50, 135, DateTimeKind.Unspecified).AddTicks(867), "Modi cupiditate.", 52, 9 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 78,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 7, 20, 5, 12, 2, 890, DateTimeKind.Unspecified).AddTicks(1068), "Omnis hic numquam vero dolore eius aut inventore quasi.", new DateTime(2021, 9, 14, 10, 1, 56, 850, DateTimeKind.Unspecified).AddTicks(5082), "Itaque quo.", 21, 40 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 79,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 5, 24, 21, 38, 46, 879, DateTimeKind.Unspecified).AddTicks(2905), "Impedit quisquam quasi ipsa rerum iusto possimus quas facere debitis magnam laboriosam alias.", new DateTime(2019, 5, 30, 11, 50, 37, 825, DateTimeKind.Unspecified).AddTicks(9123), "Iusto facilis ad error.", 54, 39 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 80,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 3, 28, 22, 48, 21, 813, DateTimeKind.Unspecified).AddTicks(8484), "Et voluptatum consectetur temporibus iure repellat.", new DateTime(2020, 5, 14, 0, 56, 29, 643, DateTimeKind.Unspecified).AddTicks(7947), "Pariatur sit deleniti aut dolor.", 4, 32, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 81,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 14, 13, 56, 43, 614, DateTimeKind.Unspecified).AddTicks(3837), "Adipisci est id minus assumenda cum perspiciatis molestiae.", new DateTime(2021, 9, 6, 4, 22, 56, 163, DateTimeKind.Unspecified).AddTicks(748), "Ipsum repudiandae consequatur laudantium corporis.", 19, 40, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 82,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 12, 5, 7, 41, 55, 127, DateTimeKind.Unspecified).AddTicks(2133), "Commodi debitis praesentium sapiente consequuntur ipsam sed enim quo facere magni eum.", new DateTime(2020, 12, 5, 13, 22, 25, 481, DateTimeKind.Unspecified).AddTicks(6087), "Quis hic ipsa a similique.", 36, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 83,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2018, 11, 16, 2, 52, 59, 214, DateTimeKind.Unspecified).AddTicks(3853), "A voluptatem blanditiis distinctio modi quam sed deserunt.", new DateTime(2018, 11, 20, 13, 52, 36, 128, DateTimeKind.Unspecified).AddTicks(9783), "Nihil excepturi.", 47, 9, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 84,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 4, 21, 19, 38, 6, 187, DateTimeKind.Unspecified).AddTicks(3422), "Enim deserunt modi quia sit assumenda illo.", new DateTime(2021, 8, 21, 21, 3, 3, 754, DateTimeKind.Unspecified).AddTicks(8363), "Voluptatem voluptatem rerum id blanditiis.", 21, 7, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 85,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 10, 5, 15, 47, 41, 563, DateTimeKind.Unspecified).AddTicks(9345), "Consequatur corporis ut eveniet sit soluta laborum aut minima corrupti exercitationem ut consequatur.", new DateTime(2019, 12, 6, 15, 40, 48, 29, DateTimeKind.Unspecified).AddTicks(7200), "Rem ea.", 30, 32, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 86,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 9, 5, 16, 52, 42, 465, DateTimeKind.Unspecified).AddTicks(8381), "Illo ea voluptas alias mollitia voluptas fugit sed laudantium.", new DateTime(2020, 10, 21, 15, 18, 48, 376, DateTimeKind.Unspecified).AddTicks(9079), "Consequatur quibusdam magnam sit qui.", 65, 11, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 87,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2020, 4, 9, 19, 21, 53, 830, DateTimeKind.Unspecified).AddTicks(293), "Illum repellendus dignissimos est corporis alias soluta et eaque illo.", new DateTime(2020, 5, 12, 20, 13, 4, 445, DateTimeKind.Unspecified).AddTicks(1020), "Reiciendis laboriosam molestiae voluptas.", 23, 31 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 88,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 7, 27, 21, 22, 25, 663, DateTimeKind.Unspecified).AddTicks(431), "Sit optio ut provident molestias et eligendi error vel iure praesentium.", new DateTime(2020, 7, 30, 4, 59, 54, 583, DateTimeKind.Unspecified).AddTicks(8498), "Pariatur aut soluta.", 22, 13, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 89,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2018, 8, 9, 16, 26, 52, 287, DateTimeKind.Unspecified).AddTicks(121), "Velit quas consequatur magnam doloremque provident enim magni blanditiis dolore.", new DateTime(2019, 7, 1, 22, 46, 0, 880, DateTimeKind.Unspecified).AddTicks(5592), "Sequi optio mollitia neque laborum.", 73, 8, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 90,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 10, 14, 1, 22, 11, 503, DateTimeKind.Unspecified).AddTicks(3727), "Sed est mollitia et atque rem voluptatibus.", new DateTime(2020, 11, 8, 6, 34, 13, 304, DateTimeKind.Unspecified).AddTicks(5091), "Iusto nemo.", 7, 1, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 91,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 4, 23, 1, 54, 17, 490, DateTimeKind.Unspecified).AddTicks(5696), "Eos laborum nam doloremque aut aliquam.", new DateTime(2020, 11, 22, 20, 18, 28, 587, DateTimeKind.Unspecified).AddTicks(5430), "Sunt velit quos.", 35, 1, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 92,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 3, 26, 15, 31, 55, 524, DateTimeKind.Unspecified).AddTicks(1494), "Quae quos pariatur vel quasi rerum dolor quae.", new DateTime(2021, 4, 4, 23, 24, 1, 581, DateTimeKind.Unspecified).AddTicks(5441), "Consequatur inventore quaerat.", 13, 21 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 93,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 1, 2, 1, 11, 24, 282, DateTimeKind.Unspecified).AddTicks(3079), "Dolorem facilis dicta aut reprehenderit deserunt ex consectetur dolor iste fuga in quidem molestiae.", new DateTime(2019, 7, 4, 19, 59, 13, 782, DateTimeKind.Unspecified).AddTicks(886), "Porro et reprehenderit debitis.", 55, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 94,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2018, 11, 11, 23, 17, 26, 795, DateTimeKind.Unspecified).AddTicks(9126), "Qui repudiandae culpa ut quas asperiores consequuntur aspernatur iusto facilis a atque blanditiis.", new DateTime(2018, 11, 11, 23, 40, 45, 336, DateTimeKind.Unspecified).AddTicks(8454), "Aliquam eum et molestias dolore.", 40, 9, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 95,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 3, 9, 0, 22, 3, 440, DateTimeKind.Unspecified).AddTicks(3406), "Sunt quo et nihil asperiores accusantium culpa omnis in et dignissimos.", new DateTime(2021, 4, 13, 13, 12, 32, 128, DateTimeKind.Unspecified).AddTicks(5175), "Laudantium aut ut commodi ad.", 22, 25, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 96,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2018, 11, 5, 4, 1, 34, 759, DateTimeKind.Unspecified).AddTicks(7019), "Blanditiis et natus eum quia est repellendus aut eum unde corporis sit non veniam.", new DateTime(2018, 11, 15, 0, 8, 52, 608, DateTimeKind.Unspecified).AddTicks(9375), "Corrupti sequi deleniti mollitia.", 31, 9, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 97,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 7, 25, 7, 50, 9, 630, DateTimeKind.Unspecified).AddTicks(2150), "Odio enim consectetur ut vero non quibusdam.", new DateTime(2021, 2, 11, 22, 48, 24, 733, DateTimeKind.Unspecified).AddTicks(3256), "Debitis ex ad dicta.", 12, 2, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 98,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 1, 21, 1, 14, 4, 331, DateTimeKind.Unspecified).AddTicks(7743), "Error deleniti saepe nemo voluptatum quia nisi et ratione ab sed aut officiis sit.", new DateTime(2020, 10, 11, 23, 2, 28, 941, DateTimeKind.Unspecified).AddTicks(9406), "Ut ut.", 55, 5, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 99,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 8, 10, 5, 57, 15, 539, DateTimeKind.Unspecified).AddTicks(9931), "Quas nostrum aliquam doloremque non dolore ad porro ea.", new DateTime(2021, 8, 14, 9, 42, 56, 90, DateTimeKind.Unspecified).AddTicks(8815), "Mollitia maxime non.", 63, 30, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 100,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 4, 30, 16, 21, 31, 741, DateTimeKind.Unspecified).AddTicks(4978), "Tempore optio ipsum expedita sequi quo tempore non eius harum vel qui tenetur.", new DateTime(2020, 9, 8, 16, 6, 11, 568, DateTimeKind.Unspecified).AddTicks(5889), "Voluptatem molestiae quia quia.", 26, 23, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 101,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2020, 5, 9, 23, 38, 21, 635, DateTimeKind.Unspecified).AddTicks(5350), "Nihil modi qui eligendi sint commodi esse.", new DateTime(2020, 7, 8, 7, 56, 21, 535, DateTimeKind.Unspecified).AddTicks(8172), "Ut est qui optio.", 65, 19 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 102,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2018, 11, 16, 3, 20, 26, 487, DateTimeKind.Unspecified).AddTicks(1875), "Sunt maxime dolores est consequatur quos molestiae eum voluptatibus provident qui est qui.", new DateTime(2018, 11, 16, 3, 25, 11, 826, DateTimeKind.Unspecified).AddTicks(1558), "Animi officiis.", 4, 9, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 103,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2018, 10, 13, 13, 1, 36, 688, DateTimeKind.Unspecified).AddTicks(3593), "Voluptates adipisci incidunt rerum doloribus et ut.", new DateTime(2018, 10, 23, 15, 42, 36, 943, DateTimeKind.Unspecified).AddTicks(5192), "Recusandae consequuntur voluptatibus laborum vero.", 27, 18 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 104,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 10, 11, 9, 21, 3, 68, DateTimeKind.Unspecified).AddTicks(7921), "Voluptates consequatur rerum ea nobis vel consequatur quasi et.", new DateTime(2020, 10, 28, 10, 41, 30, 355, DateTimeKind.Unspecified).AddTicks(933), "Facere numquam exercitationem possimus impedit.", 52, 38, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 105,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 4, 20, 15, 49, 38, 933, DateTimeKind.Unspecified).AddTicks(894), "Totam amet neque vero saepe aut cupiditate beatae exercitationem id quidem consequatur enim tempora.", new DateTime(2020, 12, 14, 20, 39, 14, 942, DateTimeKind.Unspecified).AddTicks(1207), "Praesentium exercitationem eligendi earum.", 44, 38, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 106,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2020, 2, 19, 10, 47, 48, 265, DateTimeKind.Unspecified).AddTicks(7396), "Et eaque id necessitatibus est consequatur nostrum sint id illum porro.", new DateTime(2020, 3, 2, 8, 30, 19, 239, DateTimeKind.Unspecified).AddTicks(8589), "Sit asperiores.", 41, 37 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 107,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 5, 22, 9, 47, 14, 301, DateTimeKind.Unspecified).AddTicks(6458), "Voluptatem voluptatem voluptates est ratione odio quas ratione.", new DateTime(2021, 5, 23, 14, 34, 36, 840, DateTimeKind.Unspecified).AddTicks(7086), "Quis qui omnis sint.", 35, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 108,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2018, 12, 12, 13, 2, 52, 250, DateTimeKind.Unspecified).AddTicks(1864), "Doloremque omnis omnis nemo tempora voluptas doloribus quis officiis repellendus qui eius optio.", new DateTime(2019, 8, 9, 20, 42, 5, 293, DateTimeKind.Unspecified).AddTicks(2484), "Nesciunt sed quo quo alias.", 80, 8, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 109,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2020, 3, 21, 8, 1, 31, 949, DateTimeKind.Unspecified).AddTicks(2548), "Rerum voluptates dignissimos ratione reiciendis quia aliquid iste qui sit.", new DateTime(2020, 5, 23, 7, 49, 40, 520, DateTimeKind.Unspecified).AddTicks(5547), "At esse.", 62, 31 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 110,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 5, 27, 5, 1, 19, 252, DateTimeKind.Unspecified).AddTicks(451), "Dicta ea aliquam officiis totam odit id eos.", new DateTime(2021, 7, 25, 12, 21, 33, 165, DateTimeKind.Unspecified).AddTicks(5956), "Praesentium vel ducimus numquam.", 46, 34, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 111,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 6, 15, 19, 30, 6, 419, DateTimeKind.Unspecified).AddTicks(2546), "Itaque voluptatem sit et dignissimos animi sint rerum expedita vel sed quo ea.", new DateTime(2019, 7, 18, 14, 9, 35, 573, DateTimeKind.Unspecified).AddTicks(4069), "Facilis eos beatae repellendus.", 16, 8, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 112,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 4, 22, 18, 9, 5, 936, DateTimeKind.Unspecified).AddTicks(2897), "Laborum cupiditate fuga eligendi cumque neque sed quod eum.", new DateTime(2021, 5, 11, 23, 39, 4, 194, DateTimeKind.Unspecified).AddTicks(8219), "Excepturi aut laborum.", 61, 24, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 113,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2020, 12, 14, 5, 25, 50, 852, DateTimeKind.Unspecified).AddTicks(2224), "Corporis et maxime quia iure maxime aspernatur tempore aut ad nobis quia.", new DateTime(2021, 3, 12, 3, 24, 26, 162, DateTimeKind.Unspecified).AddTicks(6982), "Vel enim unde nesciunt vitae.", 73, 37 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 114,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2018, 12, 29, 23, 46, 34, 606, DateTimeKind.Unspecified).AddTicks(7024), "Sequi dolore quod sint ut odit dolorum autem alias sapiente earum.", new DateTime(2019, 7, 13, 22, 27, 51, 973, DateTimeKind.Unspecified).AddTicks(9441), "Repellendus ut.", 71, 22, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 115,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 5, 23, 13, 2, 41, 281, DateTimeKind.Unspecified).AddTicks(7136), "In voluptatem reiciendis tenetur ab doloremque.", new DateTime(2020, 8, 24, 4, 58, 44, 413, DateTimeKind.Unspecified).AddTicks(3614), "Ipsam esse voluptatem.", 31, 1, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 116,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 5, 16, 10, 14, 55, 792, DateTimeKind.Unspecified).AddTicks(2544), "Sequi quidem voluptatum exercitationem aut enim maxime autem.", new DateTime(2020, 8, 12, 17, 23, 24, 838, DateTimeKind.Unspecified).AddTicks(8608), "Quia ratione voluptate alias.", 34, 10, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 117,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2020, 11, 29, 19, 43, 29, 663, DateTimeKind.Unspecified).AddTicks(8308), "Sapiente sit architecto animi vel quidem harum error maiores sed dolorem laudantium.", new DateTime(2020, 12, 7, 11, 1, 22, 628, DateTimeKind.Unspecified).AddTicks(1581), "Consequatur sint similique culpa molestiae.", 44, 36 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 118,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 4, 29, 8, 18, 24, 441, DateTimeKind.Unspecified).AddTicks(1269), "Maiores et itaque et aut accusantium iste error atque labore quia.", new DateTime(2021, 8, 20, 13, 17, 1, 278, DateTimeKind.Unspecified).AddTicks(8334), "Assumenda consequatur ut neque consequatur.", 14, 39, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 119,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 1, 3, 20, 41, 7, 956, DateTimeKind.Unspecified).AddTicks(4578), "Ab tempora id officia ut autem vel sint non et quis.", new DateTime(2020, 6, 2, 23, 4, 52, 380, DateTimeKind.Unspecified).AddTicks(5498), "Dolor aut impedit et.", 17, 19, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 120,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2018, 8, 5, 0, 31, 49, 178, DateTimeKind.Unspecified).AddTicks(8202), "Officiis consectetur numquam tempore eos animi voluptas illo ex aspernatur autem.", new DateTime(2018, 8, 7, 5, 39, 46, 944, DateTimeKind.Unspecified).AddTicks(302), "Sapiente voluptatibus animi voluptatibus.", 34, 17 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 121,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 4, 27, 0, 16, 11, 180, DateTimeKind.Unspecified).AddTicks(6383), "Impedit cupiditate animi odit voluptatem animi rem deleniti et voluptas qui.", new DateTime(2020, 7, 26, 16, 46, 46, 151, DateTimeKind.Unspecified).AddTicks(1120), "Omnis facere.", 44, 26, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 122,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 10, 8, 9, 54, 47, 942, DateTimeKind.Unspecified).AddTicks(3413), "Rem dignissimos ad beatae adipisci corporis doloribus molestias consequuntur voluptates eius.", new DateTime(2021, 10, 12, 16, 16, 41, 640, DateTimeKind.Unspecified).AddTicks(1948), "Ullam recusandae non.", 48, 34, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 123,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 3, 20, 17, 14, 49, 993, DateTimeKind.Unspecified).AddTicks(8596), "Nulla molestiae excepturi quis minus eum est.", new DateTime(2021, 1, 28, 17, 36, 50, 356, DateTimeKind.Unspecified).AddTicks(478), "Voluptate quaerat.", 67, 15, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 124,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 3, 10, 16, 33, 30, 944, DateTimeKind.Unspecified).AddTicks(4643), "Et ullam atque aut et recusandae.", new DateTime(2021, 4, 3, 10, 3, 23, 878, DateTimeKind.Unspecified).AddTicks(6313), "Aut labore vitae quia.", 1, 21, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 125,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "State" },
                values: new object[] { new DateTime(2019, 6, 25, 15, 25, 44, 415, DateTimeKind.Unspecified).AddTicks(9241), "Non ut nisi consequatur eligendi harum.", new DateTime(2020, 5, 2, 9, 51, 21, 971, DateTimeKind.Unspecified).AddTicks(4982), "Voluptatem necessitatibus tempore vitae maxime.", 25, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 126,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 5, 22, 18, 55, 30, 244, DateTimeKind.Unspecified).AddTicks(3802), "Nisi sapiente est officiis molestiae culpa.", new DateTime(2021, 5, 23, 0, 30, 49, 502, DateTimeKind.Unspecified).AddTicks(8449), "Optio ut ut.", 29, 35, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 127,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 4, 11, 1, 50, 18, 595, DateTimeKind.Unspecified).AddTicks(9726), "Blanditiis tempora consectetur voluptatem nihil nobis cumque.", new DateTime(2021, 4, 21, 1, 22, 18, 871, DateTimeKind.Unspecified).AddTicks(5797), "Temporibus eos.", 63, 25, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 128,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 4, 25, 19, 56, 47, 834, DateTimeKind.Unspecified).AddTicks(5660), "Id nobis amet unde quo iusto accusamus quia aliquid ipsa velit impedit.", new DateTime(2020, 6, 9, 4, 7, 9, 533, DateTimeKind.Unspecified).AddTicks(4646), "Quos qui.", 24, 23, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 129,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2018, 11, 2, 3, 34, 25, 5, DateTimeKind.Unspecified).AddTicks(8714), "Sed nulla reprehenderit facilis ex quibusdam quaerat et quibusdam minus.", new DateTime(2019, 6, 18, 7, 25, 57, 684, DateTimeKind.Unspecified).AddTicks(597), "Pariatur eius.", 42, 18, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 130,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2020, 2, 26, 21, 59, 59, 656, DateTimeKind.Unspecified).AddTicks(6883), "Et vel ad doloribus quae similique.", new DateTime(2020, 7, 21, 3, 20, 53, 949, DateTimeKind.Unspecified).AddTicks(8520), "Ipsam ducimus voluptatem ex.", 52, 19 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 131,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 15, 1, 8, 16, 117, DateTimeKind.Unspecified).AddTicks(6717), "Ex rerum laborum dolorum omnis nemo delectus dolor adipisci libero facilis cum reiciendis dolor.", new DateTime(2021, 8, 14, 22, 0, 36, 802, DateTimeKind.Unspecified).AddTicks(9703), "Enim dolorum quo autem.", 38, 34, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 132,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 12, 28, 11, 1, 51, 544, DateTimeKind.Unspecified).AddTicks(1958), "Et ea enim quo et quam sunt asperiores.", new DateTime(2021, 1, 11, 16, 3, 20, 802, DateTimeKind.Unspecified).AddTicks(3332), "Et nemo.", 31, 28, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 133,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 4, 27, 6, 18, 14, 751, DateTimeKind.Unspecified).AddTicks(5678), "Pariatur eveniet vel laudantium molestias fugit illum.", new DateTime(2021, 5, 10, 13, 14, 57, 661, DateTimeKind.Unspecified).AddTicks(6540), "Qui magnam quasi nemo.", 55, 27, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 134,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 4, 15, 1, 9, 5, 751, DateTimeKind.Unspecified).AddTicks(1030), "Voluptatem labore totam dolorem ea odit cum delectus et similique est dignissimos fugit.", new DateTime(2021, 9, 1, 15, 2, 49, 351, DateTimeKind.Unspecified).AddTicks(1260), "Quae velit.", 36, 40, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 135,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 4, 24, 14, 42, 2, 217, DateTimeKind.Unspecified).AddTicks(6443), "Est maiores assumenda aliquam est enim dolores neque quae aut non consectetur aut.", new DateTime(2021, 5, 2, 0, 11, 20, 785, DateTimeKind.Unspecified).AddTicks(9251), "Sit maiores eveniet.", 18, 25, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 136,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 7, 19, 15, 26, 18, 549, DateTimeKind.Unspecified).AddTicks(4359), "Labore ut accusamus aut officia beatae eum commodi iusto maxime sit deserunt pariatur.", new DateTime(2021, 8, 20, 1, 30, 55, 731, DateTimeKind.Unspecified).AddTicks(2624), "Mollitia earum nihil quis sed.", 52, 40 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 137,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 6, 30, 8, 19, 10, 426, DateTimeKind.Unspecified).AddTicks(2202), "Libero mollitia quo odit dolores ullam eos corporis laborum ut ea qui laboriosam.", new DateTime(2020, 11, 11, 3, 11, 30, 665, DateTimeKind.Unspecified).AddTicks(2721), "Amet autem inventore.", 9, 11, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 138,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2018, 9, 13, 12, 29, 36, 410, DateTimeKind.Unspecified).AddTicks(2604), "Adipisci eos provident omnis ut ut inventore dolor.", new DateTime(2019, 7, 4, 6, 38, 11, 900, DateTimeKind.Unspecified).AddTicks(4547), "Sunt vero a aliquam eveniet.", 78, 8, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 139,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2020, 1, 1, 4, 14, 14, 146, DateTimeKind.Unspecified).AddTicks(5476), "Dignissimos sed ipsam totam ab consectetur in vitae ut.", new DateTime(2020, 2, 27, 5, 25, 43, 711, DateTimeKind.Unspecified).AddTicks(9898), "Et sunt dignissimos similique.", 66, 20 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 140,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 6, 29, 3, 13, 4, 558, DateTimeKind.Unspecified).AddTicks(3993), "Ut enim neque veritatis voluptatem culpa et voluptate possimus alias esse iure eveniet.", new DateTime(2020, 7, 14, 11, 45, 5, 232, DateTimeKind.Unspecified).AddTicks(6583), "Itaque iusto sit cupiditate quo.", 39, 19, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 141,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 5, 21, 15, 3, 31, 718, DateTimeKind.Unspecified).AddTicks(4267), "Quos eos veritatis aspernatur facere et et sint.", new DateTime(2021, 5, 22, 9, 14, 38, 635, DateTimeKind.Unspecified).AddTicks(7873), "Quia ut dicta optio.", 13, 35, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 142,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 4, 8, 12, 27, 14, 649, DateTimeKind.Unspecified).AddTicks(2894), "Non deleniti cupiditate voluptatem dignissimos consequatur ipsum veritatis ipsa pariatur itaque reprehenderit.", new DateTime(2021, 5, 2, 15, 56, 51, 442, DateTimeKind.Unspecified).AddTicks(4691), "Eveniet laborum expedita.", 46, 25, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 143,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 9, 26, 18, 39, 12, 307, DateTimeKind.Unspecified).AddTicks(8004), "Possimus dolores provident molestiae ex iusto est.", new DateTime(2021, 10, 2, 1, 56, 7, 323, DateTimeKind.Unspecified).AddTicks(4071), "Occaecati qui et.", 48, 40, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 144,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2018, 8, 7, 21, 51, 31, 982, DateTimeKind.Unspecified).AddTicks(7950), "Eligendi officia qui voluptatibus totam pariatur fugiat eligendi modi quo est explicabo fugiat voluptatem.", new DateTime(2019, 3, 20, 21, 17, 19, 789, DateTimeKind.Unspecified).AddTicks(2691), "Eius impedit ipsum quam eligendi.", 2, 4, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 145,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2018, 11, 16, 14, 7, 15, 612, DateTimeKind.Unspecified).AddTicks(1317), "Voluptatem sapiente est doloremque reprehenderit ex.", new DateTime(2018, 11, 20, 7, 13, 32, 434, DateTimeKind.Unspecified).AddTicks(7125), "Minus suscipit.", 72, 9, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 146,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 10, 27, 4, 20, 17, 479, DateTimeKind.Unspecified).AddTicks(2456), "Maxime et voluptatem aut aut fugiat similique temporibus quos soluta.", new DateTime(2019, 10, 27, 11, 51, 48, 948, DateTimeKind.Unspecified).AddTicks(3684), "Perspiciatis commodi sint veritatis in.", 29, 16, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 147,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 3, 26, 13, 5, 41, 114, DateTimeKind.Unspecified).AddTicks(7551), "Suscipit consequatur odio quod enim earum veritatis ut commodi.", new DateTime(2021, 4, 12, 10, 42, 14, 238, DateTimeKind.Unspecified).AddTicks(636), "Magni sit maxime qui.", 80, 21, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 148,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 4, 5, 11, 23, 30, 753, DateTimeKind.Unspecified).AddTicks(7568), "Eos sed perferendis laudantium qui cumque animi aut incidunt dolores voluptatem.", new DateTime(2020, 5, 8, 13, 0, 16, 331, DateTimeKind.Unspecified).AddTicks(9872), "Ut nostrum beatae necessitatibus laudantium.", 57, 31, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 149,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "State" },
                values: new object[] { new DateTime(2020, 7, 31, 17, 40, 59, 632, DateTimeKind.Unspecified).AddTicks(7187), "Magnam magni ipsum expedita delectus consectetur sed non et voluptas.", new DateTime(2021, 1, 14, 10, 44, 24, 951, DateTimeKind.Unspecified).AddTicks(3399), "Laborum saepe.", 38, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 150,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 6, 29, 9, 11, 1, 743, DateTimeKind.Unspecified).AddTicks(5274), "Laudantium provident magni sit voluptate soluta velit in.", new DateTime(2020, 8, 29, 8, 37, 52, 341, DateTimeKind.Unspecified).AddTicks(4457), "Molestiae minima magni.", 80, 14, 3 });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "TeamName" },
                values: new object[] { new DateTime(2019, 9, 20, 17, 7, 10, 510, DateTimeKind.Unspecified).AddTicks(736), "Chadd Cruickshank" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "TeamName" },
                values: new object[] { new DateTime(2019, 5, 30, 13, 19, 57, 837, DateTimeKind.Unspecified).AddTicks(5213), "Courtney Stanton" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "TeamName" },
                values: new object[] { new DateTime(2019, 5, 30, 7, 43, 51, 711, DateTimeKind.Unspecified).AddTicks(9405), "Bartholome Skiles" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "TeamName" },
                values: new object[] { new DateTime(2020, 10, 16, 17, 33, 27, 894, DateTimeKind.Unspecified).AddTicks(9856), "Ladarius Keebler" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "TeamName" },
                values: new object[] { new DateTime(2021, 5, 7, 13, 28, 9, 562, DateTimeKind.Unspecified).AddTicks(5725), "Walter Kshlerin" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "TeamName" },
                values: new object[] { new DateTime(2018, 5, 6, 22, 17, 7, 442, DateTimeKind.Unspecified).AddTicks(8856), "Marina Mosciski" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "TeamName" },
                values: new object[] { new DateTime(2019, 9, 7, 7, 14, 4, 735, DateTimeKind.Unspecified).AddTicks(6992), "Deven Gibson" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "TeamName" },
                values: new object[] { new DateTime(2019, 6, 19, 1, 8, 40, 283, DateTimeKind.Unspecified).AddTicks(2072), "Josue Hirthe" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "TeamName" },
                values: new object[] { new DateTime(2021, 2, 10, 10, 4, 3, 905, DateTimeKind.Unspecified).AddTicks(97), "Albert Maggio" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "TeamName" },
                values: new object[] { new DateTime(2018, 6, 24, 23, 22, 38, 522, DateTimeKind.Unspecified).AddTicks(5283), "Velva Prosacco" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1998, 6, 20, 3, 0, 41, 679, DateTimeKind.Unspecified).AddTicks(3787), new DateTime(2012, 5, 23, 18, 5, 36, 954, DateTimeKind.Unspecified).AddTicks(3951), "Rupert41@gmail.com", "Torrey", "Rodriguez", 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1997, 1, 30, 5, 43, 21, 391, DateTimeKind.Unspecified).AddTicks(5168), new DateTime(2010, 11, 6, 4, 11, 45, 727, DateTimeKind.Unspecified).AddTicks(6085), "Marielle85@gmail.com", "Gregg", "Pacocha", 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1973, 5, 4, 0, 27, 45, 12, DateTimeKind.Unspecified).AddTicks(9507), new DateTime(1988, 12, 3, 3, 15, 40, 811, DateTimeKind.Unspecified).AddTicks(1217), "Braxton_Rempel@yahoo.com", "Newton", "Kunde", 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1989, 5, 3, 18, 48, 42, 683, DateTimeKind.Unspecified).AddTicks(2046), new DateTime(2018, 8, 16, 4, 26, 1, 381, DateTimeKind.Unspecified).AddTicks(5057), "Donnell_OKon11@gmail.com", "Jillian", "Cormier", 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1976, 1, 20, 21, 32, 12, 645, DateTimeKind.Unspecified).AddTicks(2621), new DateTime(2016, 8, 2, 11, 34, 10, 920, DateTimeKind.Unspecified).AddTicks(1385), "Sabryna_Schuppe@yahoo.com", "Wilfrid", "Quigley", 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1975, 9, 2, 5, 46, 35, 527, DateTimeKind.Unspecified).AddTicks(7546), new DateTime(1998, 7, 16, 9, 14, 20, 54, DateTimeKind.Unspecified).AddTicks(1053), "Gilbert_Kiehn@hotmail.com", "Elisha", "Mitchell", 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1971, 1, 21, 9, 28, 40, 714, DateTimeKind.Unspecified).AddTicks(5153), new DateTime(2004, 11, 20, 0, 12, 13, 806, DateTimeKind.Unspecified).AddTicks(5608), "Charity_Funk@gmail.com", "Etha", "Klein", 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2010, 6, 25, 1, 37, 8, 38, DateTimeKind.Unspecified).AddTicks(962), new DateTime(2019, 3, 31, 21, 45, 54, 76, DateTimeKind.Unspecified).AddTicks(8075), "Jarrell_Rutherford26@yahoo.com", "Natalia", "Daniel", 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1979, 8, 1, 12, 50, 27, 128, DateTimeKind.Unspecified).AddTicks(5236), new DateTime(1997, 10, 8, 19, 47, 27, 495, DateTimeKind.Unspecified).AddTicks(7939), "Coleman88@yahoo.com", "Trevor", "Hammes", 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1997, 2, 3, 14, 2, 17, 987, DateTimeKind.Unspecified).AddTicks(6761), new DateTime(2009, 8, 27, 22, 55, 3, 270, DateTimeKind.Unspecified).AddTicks(787), "Travon.Lehner@yahoo.com", "Yadira", "Bauch", 6 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1997, 5, 24, 12, 38, 50, 394, DateTimeKind.Unspecified).AddTicks(94), new DateTime(2015, 12, 4, 20, 49, 33, 673, DateTimeKind.Unspecified).AddTicks(3272), "Vickie48@yahoo.com", "Margarette", "Legros", 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2013, 5, 21, 17, 18, 19, 736, DateTimeKind.Unspecified).AddTicks(7806), new DateTime(2021, 6, 20, 13, 6, 12, 367, DateTimeKind.Unspecified).AddTicks(5397), "Dakota52@gmail.com", "Modesto", "Emmerich", 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1994, 1, 16, 6, 15, 12, 565, DateTimeKind.Unspecified).AddTicks(7305), new DateTime(2006, 10, 16, 6, 16, 6, 665, DateTimeKind.Unspecified).AddTicks(3609), "Nils90@gmail.com", "Virginia", "Schowalter", 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1997, 8, 13, 8, 40, 20, 486, DateTimeKind.Unspecified).AddTicks(8343), new DateTime(2006, 10, 12, 17, 1, 40, 554, DateTimeKind.Unspecified).AddTicks(7651), "Mckayla_Johnson@yahoo.com", "Gage", "Okuneva", 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1993, 12, 26, 2, 10, 11, 196, DateTimeKind.Unspecified).AddTicks(18), new DateTime(2009, 10, 1, 5, 16, 58, 680, DateTimeKind.Unspecified).AddTicks(4154), "Maverick_Wolff34@yahoo.com", "Mauricio", "Gleichner", 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1979, 2, 10, 18, 37, 56, 379, DateTimeKind.Unspecified).AddTicks(840), new DateTime(2021, 4, 29, 6, 58, 8, 514, DateTimeKind.Unspecified).AddTicks(4784), "Kristy65@hotmail.com", "Hollie", "Turcotte", 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2005, 9, 22, 18, 59, 54, 216, DateTimeKind.Unspecified).AddTicks(1938), new DateTime(2021, 5, 20, 17, 18, 41, 51, DateTimeKind.Unspecified).AddTicks(8568), "Darien.Bergnaum@yahoo.com", "Tristian", "Hilpert", 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName" },
                values: new object[] { new DateTime(2004, 10, 11, 22, 16, 3, 786, DateTimeKind.Unspecified).AddTicks(8822), new DateTime(2021, 5, 16, 4, 58, 47, 97, DateTimeKind.Unspecified).AddTicks(1648), "Mara61@gmail.com", "Sydni", "Turcotte" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName" },
                values: new object[] { new DateTime(1994, 8, 7, 5, 12, 30, 9, DateTimeKind.Unspecified).AddTicks(2686), new DateTime(2006, 3, 26, 8, 54, 4, 192, DateTimeKind.Unspecified).AddTicks(2672), "Blaze_Weimann12@hotmail.com", "Kathleen", "Abbott" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2010, 7, 24, 10, 48, 19, 955, DateTimeKind.Unspecified).AddTicks(7926), new DateTime(2019, 2, 7, 11, 39, 18, 696, DateTimeKind.Unspecified).AddTicks(3871), "Nicolette46@gmail.com", "Hassie", "Block", 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2009, 5, 27, 20, 7, 34, 967, DateTimeKind.Unspecified).AddTicks(5630), new DateTime(2019, 12, 8, 17, 56, 44, 421, DateTimeKind.Unspecified).AddTicks(2747), "Carole.Lehner@gmail.com", "Kasandra", "Luettgen", 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1982, 12, 17, 3, 2, 22, 848, DateTimeKind.Unspecified).AddTicks(8554), new DateTime(2000, 5, 12, 6, 32, 26, 89, DateTimeKind.Unspecified).AddTicks(1423), "Marisol.Weimann@hotmail.com", "Santiago", "Erdman", 9 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2005, 9, 3, 23, 49, 30, 641, DateTimeKind.Unspecified).AddTicks(1982), new DateTime(2014, 11, 6, 14, 40, 52, 124, DateTimeKind.Unspecified).AddTicks(1737), "Vernon.Kulas91@hotmail.com", "Emerald", "Fritsch", 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1976, 9, 14, 20, 38, 27, 674, DateTimeKind.Unspecified).AddTicks(380), new DateTime(2002, 1, 5, 14, 58, 51, 970, DateTimeKind.Unspecified).AddTicks(2431), "Abdullah_King40@hotmail.com", "Sarah", "Schmidt", 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1986, 7, 4, 16, 54, 57, 330, DateTimeKind.Unspecified).AddTicks(4731), new DateTime(2014, 11, 1, 15, 50, 42, 522, DateTimeKind.Unspecified).AddTicks(8879), "Cleve_Haley@yahoo.com", "Travis", "McDermott", 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2001, 6, 24, 14, 40, 3, 317, DateTimeKind.Unspecified).AddTicks(6516), new DateTime(2015, 5, 25, 13, 20, 32, 508, DateTimeKind.Unspecified).AddTicks(6033), "Elfrieda66@gmail.com", "Leif", "Halvorson", 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1991, 11, 19, 16, 54, 41, 668, DateTimeKind.Unspecified).AddTicks(4724), new DateTime(2018, 6, 6, 16, 13, 34, 502, DateTimeKind.Unspecified).AddTicks(6157), "Zoie67@yahoo.com", "Aron", "Koss", 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1983, 3, 8, 22, 51, 33, 891, DateTimeKind.Unspecified).AddTicks(2709), new DateTime(2006, 8, 14, 9, 54, 50, 64, DateTimeKind.Unspecified).AddTicks(777), "Arlie56@yahoo.com", "Shannon", "Crona", 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2001, 6, 7, 14, 44, 6, 191, DateTimeKind.Unspecified).AddTicks(1556), new DateTime(2017, 9, 15, 19, 59, 52, 832, DateTimeKind.Unspecified).AddTicks(505), "Walker_Gleichner@yahoo.com", "Aurelia", "Keeling", 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1987, 7, 11, 14, 42, 45, 983, DateTimeKind.Unspecified).AddTicks(247), new DateTime(2015, 9, 18, 3, 7, 44, 286, DateTimeKind.Unspecified).AddTicks(4422), "Brian.Corwin@yahoo.com", "Elbert", "Prosacco", 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1973, 3, 4, 9, 29, 56, 570, DateTimeKind.Unspecified).AddTicks(599), new DateTime(2020, 10, 8, 6, 32, 42, 615, DateTimeKind.Unspecified).AddTicks(2465), "Frieda_Greenholt@yahoo.com", "Rebekah", "Hane", 6 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName" },
                values: new object[] { new DateTime(1991, 11, 27, 3, 41, 51, 641, DateTimeKind.Unspecified).AddTicks(3363), new DateTime(2015, 9, 19, 21, 29, 57, 363, DateTimeKind.Unspecified).AddTicks(3543), "Sean.Flatley@gmail.com", "Madelyn", "Hilpert" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1981, 9, 28, 6, 17, 19, 151, DateTimeKind.Unspecified).AddTicks(7490), new DateTime(2019, 3, 30, 6, 17, 36, 179, DateTimeKind.Unspecified).AddTicks(1520), "Halle49@hotmail.com", "Gianni", "Wiza", 9 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2000, 2, 29, 13, 34, 29, 69, DateTimeKind.Unspecified).AddTicks(8920), new DateTime(2019, 2, 7, 3, 39, 24, 402, DateTimeKind.Unspecified).AddTicks(5581), "Cleora_Schmeler2@yahoo.com", "Ashleigh", "D'Amore", 6 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2009, 9, 25, 13, 16, 58, 845, DateTimeKind.Unspecified).AddTicks(3134), new DateTime(2019, 8, 12, 5, 55, 10, 260, DateTimeKind.Unspecified).AddTicks(3052), "Pearl_Waelchi@gmail.com", "Kara", "Reilly", 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1982, 5, 19, 23, 45, 44, 206, DateTimeKind.Unspecified).AddTicks(7027), new DateTime(1993, 11, 25, 0, 30, 46, 846, DateTimeKind.Unspecified).AddTicks(5499), "Tierra.Stamm@hotmail.com", "Logan", "Olson", 10 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1991, 1, 25, 14, 17, 8, 249, DateTimeKind.Unspecified).AddTicks(5566), new DateTime(2015, 7, 26, 1, 51, 22, 30, DateTimeKind.Unspecified).AddTicks(4417), "Jameson.Kub7@gmail.com", "Madaline", "Conroy", 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2000, 2, 2, 3, 16, 41, 362, DateTimeKind.Unspecified).AddTicks(1736), new DateTime(2017, 9, 5, 13, 18, 24, 333, DateTimeKind.Unspecified).AddTicks(6288), "Rosario_Friesen17@yahoo.com", "Rashad", "Veum", 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1981, 1, 30, 9, 15, 42, 918, DateTimeKind.Unspecified).AddTicks(2075), new DateTime(2015, 7, 7, 12, 56, 35, 804, DateTimeKind.Unspecified).AddTicks(2856), "Johnathan95@yahoo.com", "Hilton", "Klein", 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1992, 4, 11, 8, 35, 46, 468, DateTimeKind.Unspecified).AddTicks(1991), new DateTime(2015, 4, 13, 8, 26, 12, 968, DateTimeKind.Unspecified).AddTicks(8470), "Grayce.McLaughlin6@hotmail.com", "Kendrick", "Swaniawski", 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 41,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2007, 8, 21, 7, 56, 28, 286, DateTimeKind.Unspecified).AddTicks(3366), new DateTime(2018, 6, 24, 11, 49, 17, 289, DateTimeKind.Unspecified).AddTicks(995), "Nellie59@hotmail.com", "Nora", "Herman", 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 42,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1977, 11, 15, 13, 18, 19, 261, DateTimeKind.Unspecified).AddTicks(7246), new DateTime(2012, 1, 15, 16, 50, 9, 517, DateTimeKind.Unspecified).AddTicks(931), "Merlin97@gmail.com", "Hugh", "Feest", 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 43,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1999, 4, 1, 20, 40, 51, 474, DateTimeKind.Unspecified).AddTicks(7248), new DateTime(2007, 5, 8, 1, 23, 43, 384, DateTimeKind.Unspecified).AddTicks(1586), "Delphine.Batz@gmail.com", "Dana", "Waelchi", 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 44,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2003, 11, 9, 13, 39, 44, 123, DateTimeKind.Unspecified).AddTicks(2894), new DateTime(2013, 10, 6, 12, 15, 57, 208, DateTimeKind.Unspecified).AddTicks(574), "Van64@gmail.com", "Barton", "Dare", 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 45,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2000, 8, 1, 10, 53, 18, 712, DateTimeKind.Unspecified).AddTicks(5188), new DateTime(2020, 4, 30, 2, 27, 7, 410, DateTimeKind.Unspecified).AddTicks(1998), "Lucious_Champlin@gmail.com", "Ferne", "Jacobson", 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 46,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2001, 8, 1, 3, 53, 10, 775, DateTimeKind.Unspecified).AddTicks(5774), new DateTime(2009, 11, 29, 6, 43, 29, 33, DateTimeKind.Unspecified).AddTicks(2793), "Kristina.Dietrich56@hotmail.com", "Eugene", "Toy", 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 47,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1971, 12, 18, 1, 2, 0, 317, DateTimeKind.Unspecified).AddTicks(4270), new DateTime(2004, 2, 21, 23, 44, 1, 32, DateTimeKind.Unspecified).AddTicks(9937), "Abigale33@gmail.com", "Triston", "Smitham", 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 48,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1975, 6, 5, 15, 18, 4, 855, DateTimeKind.Unspecified).AddTicks(8091), new DateTime(1998, 9, 3, 13, 18, 36, 89, DateTimeKind.Unspecified).AddTicks(1429), "Olen_Schuster@yahoo.com", "Coty", "Wuckert", 9 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 49,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2003, 8, 30, 19, 10, 26, 139, DateTimeKind.Unspecified).AddTicks(6350), new DateTime(2019, 10, 12, 16, 16, 14, 32, DateTimeKind.Unspecified).AddTicks(9703), "Waino.Effertz@yahoo.com", "Noe", "Rogahn", 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 50,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1998, 5, 5, 7, 37, 49, 215, DateTimeKind.Unspecified).AddTicks(9418), new DateTime(2019, 6, 23, 16, 13, 53, 951, DateTimeKind.Unspecified).AddTicks(7636), "Kayden.Jerde@gmail.com", "Ariel", "Bechtelar", 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 51,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName" },
                values: new object[] { new DateTime(1986, 8, 23, 11, 50, 25, 411, DateTimeKind.Unspecified).AddTicks(7024), new DateTime(2001, 6, 14, 15, 27, 45, 804, DateTimeKind.Unspecified).AddTicks(9384), "Eleazar.Cassin81@gmail.com", "Eliza", "Nolan" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 52,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2005, 4, 18, 15, 23, 49, 841, DateTimeKind.Unspecified).AddTicks(9042), new DateTime(2020, 4, 15, 14, 23, 37, 561, DateTimeKind.Unspecified).AddTicks(1674), "Scot.Dickinson@gmail.com", "Imelda", "Hagenes", 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 53,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1981, 6, 2, 14, 56, 56, 412, DateTimeKind.Unspecified).AddTicks(5388), new DateTime(2003, 5, 21, 13, 9, 42, 267, DateTimeKind.Unspecified).AddTicks(1884), "Edgar_Cronin35@yahoo.com", "Kieran", "Erdman", 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 54,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1983, 5, 5, 15, 52, 2, 765, DateTimeKind.Unspecified).AddTicks(7160), new DateTime(1997, 1, 19, 18, 44, 21, 635, DateTimeKind.Unspecified).AddTicks(5548), "Beulah84@yahoo.com", "Deron", "Yost", 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 55,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "TeamId" },
                values: new object[] { new DateTime(1973, 4, 11, 9, 22, 8, 110, DateTimeKind.Unspecified).AddTicks(8076), new DateTime(2019, 5, 20, 15, 35, 42, 484, DateTimeKind.Unspecified).AddTicks(6976), "Lorna_Robel21@yahoo.com", "Geovanni", 9 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 56,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1974, 8, 2, 13, 7, 47, 130, DateTimeKind.Unspecified).AddTicks(254), new DateTime(1991, 6, 23, 5, 29, 16, 579, DateTimeKind.Unspecified).AddTicks(5182), "Henry96@hotmail.com", "Adriana", "Morissette", 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 57,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1989, 6, 30, 12, 53, 31, 797, DateTimeKind.Unspecified).AddTicks(646), new DateTime(2006, 3, 17, 23, 35, 2, 387, DateTimeKind.Unspecified).AddTicks(2451), "Cielo_Hane77@yahoo.com", "Bianka", "Schuppe", 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 58,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2007, 10, 11, 8, 36, 14, 407, DateTimeKind.Unspecified).AddTicks(6448), new DateTime(2017, 7, 22, 15, 2, 29, 42, DateTimeKind.Unspecified).AddTicks(7222), "Luz.Ullrich@gmail.com", "Makayla", "Deckow", 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 59,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1971, 12, 4, 5, 43, 53, 941, DateTimeKind.Unspecified).AddTicks(9503), new DateTime(2016, 6, 5, 0, 15, 53, 204, DateTimeKind.Unspecified).AddTicks(4739), "Dayton_Hills63@gmail.com", "Christa", "Kuhlman", 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 60,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1981, 10, 16, 21, 47, 28, 454, DateTimeKind.Unspecified).AddTicks(1430), new DateTime(2019, 1, 24, 23, 53, 15, 345, DateTimeKind.Unspecified).AddTicks(5284), "Nicholaus.Franecki@gmail.com", "Victor", "Cruickshank", 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 61,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2009, 3, 21, 5, 45, 11, 838, DateTimeKind.Unspecified).AddTicks(6826), new DateTime(2018, 10, 5, 4, 45, 53, 91, DateTimeKind.Unspecified).AddTicks(9400), "Felipa_Kemmer@yahoo.com", "Lorna", "McDermott", 6 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 62,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2012, 6, 18, 6, 4, 13, 561, DateTimeKind.Unspecified).AddTicks(3856), new DateTime(2021, 5, 28, 22, 15, 27, 776, DateTimeKind.Unspecified).AddTicks(6256), "Tracey94@gmail.com", "Deonte", "Von", 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 63,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2009, 10, 22, 15, 49, 51, 862, DateTimeKind.Unspecified).AddTicks(4994), new DateTime(2020, 3, 30, 0, 29, 30, 139, DateTimeKind.Unspecified).AddTicks(9479), "Elenora92@yahoo.com", "Brigitte", "Huels", 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 64,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2011, 9, 2, 10, 15, 43, 279, DateTimeKind.Unspecified).AddTicks(9220), new DateTime(2020, 2, 17, 11, 6, 38, 569, DateTimeKind.Unspecified).AddTicks(5735), "Mateo_Lockman88@yahoo.com", "Aaron", "McDermott", 10 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 65,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1971, 4, 1, 10, 27, 5, 945, DateTimeKind.Unspecified).AddTicks(7871), new DateTime(2000, 6, 18, 9, 14, 10, 978, DateTimeKind.Unspecified).AddTicks(2900), "Turner.Baumbach42@gmail.com", "Nichole", "Paucek", 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 66,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1988, 5, 22, 3, 52, 13, 583, DateTimeKind.Unspecified).AddTicks(4464), new DateTime(2011, 9, 11, 5, 3, 29, 706, DateTimeKind.Unspecified).AddTicks(1892), "Dayna64@gmail.com", "Flo", "Stiedemann", 9 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 67,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName" },
                values: new object[] { new DateTime(1988, 12, 25, 19, 57, 22, 663, DateTimeKind.Unspecified).AddTicks(8093), new DateTime(2006, 9, 12, 9, 29, 54, 383, DateTimeKind.Unspecified).AddTicks(5530), "Marcia.Lehner@yahoo.com", "Damaris", "Crona" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 68,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1999, 4, 19, 5, 23, 37, 701, DateTimeKind.Unspecified).AddTicks(8270), new DateTime(2009, 9, 13, 15, 44, 23, 867, DateTimeKind.Unspecified).AddTicks(376), "Gerald35@gmail.com", "Ahmed", "O'Kon", 6 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 69,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1983, 8, 27, 22, 13, 2, 682, DateTimeKind.Unspecified).AddTicks(4743), new DateTime(2015, 8, 30, 9, 8, 44, 324, DateTimeKind.Unspecified).AddTicks(3145), "Kailey.Farrell@hotmail.com", "Janiya", "Murray", 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 70,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName" },
                values: new object[] { new DateTime(1982, 2, 13, 9, 21, 34, 450, DateTimeKind.Unspecified).AddTicks(5530), new DateTime(2006, 3, 29, 13, 13, 49, 515, DateTimeKind.Unspecified).AddTicks(7386), "Alexys_Lemke@yahoo.com", "Dayana", "Ledner" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 71,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1993, 7, 20, 4, 43, 56, 678, DateTimeKind.Unspecified).AddTicks(2935), new DateTime(2020, 5, 28, 2, 51, 22, 647, DateTimeKind.Unspecified).AddTicks(7455), "Isabel_King92@yahoo.com", "Tyra", "Zieme", 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 72,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1989, 9, 26, 5, 33, 57, 261, DateTimeKind.Unspecified).AddTicks(4601), new DateTime(1997, 9, 26, 21, 6, 21, 245, DateTimeKind.Unspecified).AddTicks(619), "Halie.McLaughlin@gmail.com", "Yvonne", "Frami", 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 73,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2007, 12, 3, 18, 34, 47, 466, DateTimeKind.Unspecified).AddTicks(3878), new DateTime(2016, 12, 17, 6, 23, 10, 991, DateTimeKind.Unspecified).AddTicks(175), "Ray11@yahoo.com", "Arlie", "Mueller", 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 74,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1993, 6, 8, 3, 12, 50, 952, DateTimeKind.Unspecified).AddTicks(8045), new DateTime(2017, 11, 26, 21, 19, 21, 33, DateTimeKind.Unspecified).AddTicks(9028), "Mack.Krajcik9@hotmail.com", "Arnaldo", "Considine", 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 75,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName" },
                values: new object[] { new DateTime(2011, 11, 7, 7, 28, 39, 405, DateTimeKind.Unspecified).AddTicks(7546), new DateTime(2019, 12, 17, 13, 52, 36, 607, DateTimeKind.Unspecified).AddTicks(1147), "Delores_Goyette@gmail.com", "Orion", "Stokes" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 76,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2012, 5, 6, 6, 21, 2, 640, DateTimeKind.Unspecified).AddTicks(7682), new DateTime(2021, 2, 19, 17, 23, 43, 932, DateTimeKind.Unspecified).AddTicks(4552), "Salma62@hotmail.com", "Rhoda", "Langworth", 6 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 77,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2011, 8, 11, 13, 35, 2, 698, DateTimeKind.Unspecified).AddTicks(7582), new DateTime(2020, 9, 23, 15, 20, 29, 249, DateTimeKind.Unspecified).AddTicks(1162), "Liam36@yahoo.com", "Aracely", "Heathcote", 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 78,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1979, 8, 3, 14, 28, 56, 648, DateTimeKind.Unspecified).AddTicks(630), new DateTime(2000, 9, 18, 12, 41, 35, 809, DateTimeKind.Unspecified).AddTicks(7486), "Palma_Gutmann56@gmail.com", "Thaddeus", "Von", 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 79,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2014, 4, 22, 18, 3, 1, 972, DateTimeKind.Unspecified).AddTicks(6906), new DateTime(2022, 1, 2, 5, 35, 45, 456, DateTimeKind.Unspecified).AddTicks(9549), "Dennis_Murray10@yahoo.com", "Tania", "Carter", 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 80,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2013, 5, 27, 17, 38, 49, 255, DateTimeKind.Unspecified).AddTicks(6700), new DateTime(2021, 6, 6, 9, 33, 14, 993, DateTimeKind.Unspecified).AddTicks(2999), "Houston.Graham@yahoo.com", "Ron", "Farrell", 7 });
        }
    }
}
