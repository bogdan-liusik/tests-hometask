﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ProjectStructure.DAL.Migrations
{
    public partial class AddNormalNaming : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "TeamName",
                table: "Teams",
                newName: "Name");

            migrationBuilder.RenameColumn(
                name: "TaskName",
                table: "Tasks",
                newName: "Name");

            migrationBuilder.RenameColumn(
                name: "ProjectName",
                table: "Projects",
                newName: "Name");

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 13, new DateTime(2019, 6, 7, 9, 21, 34, 842, DateTimeKind.Unspecified).AddTicks(6850), new DateTime(2020, 4, 28, 17, 41, 8, 488, DateTimeKind.Unspecified).AddTicks(2706), "Boston's most advanced compression wear technology increases muscle oxygenation, stabilizes active muscles", "Tasty Wooden Chips", 9 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 57, new DateTime(2019, 9, 18, 11, 41, 54, 714, DateTimeKind.Unspecified).AddTicks(9476), new DateTime(2020, 11, 12, 8, 12, 30, 315, DateTimeKind.Unspecified).AddTicks(4628), "Ergonomic executive chair upholstered in bonded black leather and PVC padded seat and back for all-day comfort and support", "Handmade Plastic Car", 3 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 26, new DateTime(2018, 3, 10, 17, 41, 22, 970, DateTimeKind.Unspecified).AddTicks(1365), new DateTime(2020, 3, 11, 22, 48, 50, 218, DateTimeKind.Unspecified).AddTicks(9427), "The Nagasaki Lander is the trademarked name of several series of Nagasaki sport bikes, that started with the 1984 ABC800J", "Handmade Metal Shoes", 10 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 62, new DateTime(2018, 4, 11, 5, 0, 7, 0, DateTimeKind.Unspecified).AddTicks(1413), new DateTime(2019, 5, 4, 23, 14, 51, 663, DateTimeKind.Unspecified).AddTicks(8979), "The automobile layout consists of a front-engine design, with transaxle-type transmissions mounted at the rear of the engine and four wheel drive", "Refined Cotton Pants", 9 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 12, new DateTime(2020, 2, 16, 5, 16, 17, 430, DateTimeKind.Unspecified).AddTicks(7240), new DateTime(2020, 9, 12, 11, 10, 23, 317, DateTimeKind.Unspecified).AddTicks(9761), "Ergonomic executive chair upholstered in bonded black leather and PVC padded seat and back for all-day comfort and support", "Practical Wooden Soap", 6 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 26, new DateTime(2019, 8, 24, 23, 9, 50, 388, DateTimeKind.Unspecified).AddTicks(6214), new DateTime(2020, 5, 16, 14, 12, 13, 510, DateTimeKind.Unspecified).AddTicks(7337), "The beautiful range of Apple Naturalé that has an exciting mix of natural ingredients. With the Goodness of 100% Natural Ingredients", "Sleek Granite Towels", 4 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 51, new DateTime(2018, 8, 14, 1, 39, 15, 29, DateTimeKind.Unspecified).AddTicks(2224), new DateTime(2018, 9, 3, 12, 59, 43, 389, DateTimeKind.Unspecified).AddTicks(1055), "Carbonite web goalkeeper gloves are ergonomically designed to give easy fit", "Incredible Concrete Bike", 7 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 37, new DateTime(2020, 1, 30, 1, 54, 17, 700, DateTimeKind.Unspecified).AddTicks(3408), new DateTime(2021, 10, 2, 5, 27, 7, 223, DateTimeKind.Unspecified).AddTicks(6620), "New range of formal shirts are designed keeping you in mind. With fits and styling that will make you stand apart", "Small Cotton Bacon", 4 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 51, new DateTime(2019, 4, 20, 10, 1, 36, 9, DateTimeKind.Unspecified).AddTicks(9438), new DateTime(2019, 9, 17, 15, 21, 45, 519, DateTimeKind.Unspecified).AddTicks(2093), "The Football Is Good For Training And Recreational Purposes", "Gorgeous Soft Soap", 5 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name" },
                values: new object[] { 34, new DateTime(2018, 10, 11, 1, 57, 17, 985, DateTimeKind.Unspecified).AddTicks(7147), new DateTime(2020, 7, 1, 19, 13, 32, 654, DateTimeKind.Unspecified).AddTicks(4992), "New range of formal shirts are designed keeping you in mind. With fits and styling that will make you stand apart", "Small Wooden Sausages" });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 49, new DateTime(2020, 4, 24, 1, 19, 13, 866, DateTimeKind.Unspecified).AddTicks(7350), new DateTime(2021, 8, 9, 0, 7, 23, 665, DateTimeKind.Unspecified).AddTicks(5226), "The Football Is Good For Training And Recreational Purposes", "Unbranded Steel Cheese", 5 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 60, new DateTime(2021, 1, 2, 8, 55, 10, 116, DateTimeKind.Unspecified).AddTicks(1601), new DateTime(2021, 2, 22, 21, 9, 7, 573, DateTimeKind.Unspecified).AddTicks(106), "Boston's most advanced compression wear technology increases muscle oxygenation, stabilizes active muscles", "Tasty Rubber Chair", 9 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 60, new DateTime(2020, 6, 1, 20, 20, 25, 394, DateTimeKind.Unspecified).AddTicks(2804), new DateTime(2021, 10, 19, 0, 36, 7, 421, DateTimeKind.Unspecified).AddTicks(7056), "The Football Is Good For Training And Recreational Purposes", "Tasty Metal Soap", 9 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 55, new DateTime(2020, 2, 6, 10, 16, 9, 691, DateTimeKind.Unspecified).AddTicks(9386), new DateTime(2021, 8, 27, 0, 22, 34, 920, DateTimeKind.Unspecified).AddTicks(6092), "The Nagasaki Lander is the trademarked name of several series of Nagasaki sport bikes, that started with the 1984 ABC800J", "Refined Steel Soap", 4 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 46, new DateTime(2019, 1, 23, 4, 59, 21, 561, DateTimeKind.Unspecified).AddTicks(5329), new DateTime(2021, 6, 23, 17, 1, 49, 460, DateTimeKind.Unspecified).AddTicks(3011), "The Nagasaki Lander is the trademarked name of several series of Nagasaki sport bikes, that started with the 1984 ABC800J", "Intelligent Concrete Pizza", 1 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 32, new DateTime(2019, 6, 19, 6, 12, 52, 387, DateTimeKind.Unspecified).AddTicks(7907), new DateTime(2019, 12, 17, 4, 43, 0, 423, DateTimeKind.Unspecified).AddTicks(6897), "Ergonomic executive chair upholstered in bonded black leather and PVC padded seat and back for all-day comfort and support", "Ergonomic Soft Soap", 7 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 47, new DateTime(2020, 10, 26, 22, 43, 1, 907, DateTimeKind.Unspecified).AddTicks(2751), new DateTime(2020, 11, 1, 14, 7, 56, 54, DateTimeKind.Unspecified).AddTicks(3971), "New range of formal shirts are designed keeping you in mind. With fits and styling that will make you stand apart", "Incredible Soft Bacon", 7 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 65, new DateTime(2018, 6, 2, 12, 50, 26, 389, DateTimeKind.Unspecified).AddTicks(1737), new DateTime(2020, 5, 12, 8, 5, 18, 438, DateTimeKind.Unspecified).AddTicks(3567), "The Football Is Good For Training And Recreational Purposes", "Incredible Fresh Fish", 3 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name" },
                values: new object[] { 79, new DateTime(2020, 1, 22, 13, 4, 45, 532, DateTimeKind.Unspecified).AddTicks(7183), new DateTime(2021, 9, 29, 0, 39, 52, 773, DateTimeKind.Unspecified).AddTicks(1777), "The Nagasaki Lander is the trademarked name of several series of Nagasaki sport bikes, that started with the 1984 ABC800J", "Tasty Wooden Shoes" });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 23, new DateTime(2019, 11, 4, 22, 31, 9, 623, DateTimeKind.Unspecified).AddTicks(7044), new DateTime(2020, 3, 7, 17, 52, 51, 761, DateTimeKind.Unspecified).AddTicks(8325), "Andy shoes are designed to keeping in mind durability as well as trends, the most stylish range of shoes & sandals", "Incredible Soft Table", 7 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 46, new DateTime(2018, 12, 4, 11, 25, 44, 299, DateTimeKind.Unspecified).AddTicks(2718), new DateTime(2020, 8, 9, 10, 43, 47, 598, DateTimeKind.Unspecified).AddTicks(869), "The beautiful range of Apple Naturalé that has an exciting mix of natural ingredients. With the Goodness of 100% Natural Ingredients", "Handcrafted Cotton Keyboard", 9 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 26, new DateTime(2018, 2, 14, 12, 11, 31, 988, DateTimeKind.Unspecified).AddTicks(2166), new DateTime(2021, 6, 28, 22, 0, 54, 128, DateTimeKind.Unspecified).AddTicks(291), "Boston's most advanced compression wear technology increases muscle oxygenation, stabilizes active muscles", "Practical Fresh Car", 8 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Name", "TeamId" },
                values: new object[] { 17, new DateTime(2018, 10, 5, 14, 59, 26, 810, DateTimeKind.Unspecified).AddTicks(970), new DateTime(2021, 7, 13, 15, 43, 25, 677, DateTimeKind.Unspecified).AddTicks(9060), "Handcrafted Steel Car", 10 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 17, new DateTime(2020, 7, 25, 22, 3, 7, 99, DateTimeKind.Unspecified).AddTicks(9254), new DateTime(2021, 7, 7, 4, 22, 33, 960, DateTimeKind.Unspecified).AddTicks(4832), "New range of formal shirts are designed keeping you in mind. With fits and styling that will make you stand apart", "Intelligent Frozen Table", 9 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 7, new DateTime(2021, 6, 12, 23, 33, 41, 408, DateTimeKind.Unspecified).AddTicks(7274), new DateTime(2021, 7, 9, 14, 49, 32, 925, DateTimeKind.Unspecified).AddTicks(270), "The Apollotech B340 is an affordable wireless mouse with reliable connectivity, 12 months battery life and modern design", "Incredible Rubber Pizza", 3 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 30, new DateTime(2020, 1, 11, 1, 9, 57, 534, DateTimeKind.Unspecified).AddTicks(648), new DateTime(2021, 1, 31, 4, 42, 44, 183, DateTimeKind.Unspecified).AddTicks(6195), "The automobile layout consists of a front-engine design, with transaxle-type transmissions mounted at the rear of the engine and four wheel drive", "Unbranded Frozen Chair", 9 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 48, new DateTime(2021, 5, 8, 9, 3, 55, 338, DateTimeKind.Unspecified).AddTicks(4565), new DateTime(2021, 10, 5, 15, 26, 56, 801, DateTimeKind.Unspecified).AddTicks(2830), "New ABC 13 9370, 13.3, 5th Gen CoreA5-8250U, 8GB RAM, 256GB SSD, power UHD Graphics, OS 10 Home, OS Office A & J 2016", "Handmade Rubber Ball", 4 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 32, new DateTime(2019, 3, 20, 17, 49, 47, 332, DateTimeKind.Unspecified).AddTicks(6103), new DateTime(2020, 11, 5, 16, 25, 19, 125, DateTimeKind.Unspecified).AddTicks(3075), "Carbonite web goalkeeper gloves are ergonomically designed to give easy fit", "Intelligent Concrete Chicken", 5 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name" },
                values: new object[] { 75, new DateTime(2020, 10, 22, 13, 17, 31, 736, DateTimeKind.Unspecified).AddTicks(7084), new DateTime(2021, 4, 15, 7, 12, 28, 740, DateTimeKind.Unspecified).AddTicks(4490), "New range of formal shirts are designed keeping you in mind. With fits and styling that will make you stand apart", "Ergonomic Cotton Shirt" });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 75, new DateTime(2019, 10, 3, 20, 20, 23, 128, DateTimeKind.Unspecified).AddTicks(7349), new DateTime(2020, 7, 5, 1, 51, 41, 561, DateTimeKind.Unspecified).AddTicks(748), "The slim & simple Maple Gaming Keyboard from Dev Byte comes with a sleek body and 7- Color RGB LED Back-lighting for smart functionality", "Ergonomic Fresh Gloves", 1 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 46, new DateTime(2018, 5, 19, 10, 18, 0, 616, DateTimeKind.Unspecified).AddTicks(7798), new DateTime(2021, 11, 21, 23, 55, 3, 29, DateTimeKind.Unspecified).AddTicks(3869), "Carbonite web goalkeeper gloves are ergonomically designed to give easy fit", "Incredible Fresh Computer", 6 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 46, new DateTime(2020, 8, 20, 0, 32, 3, 917, DateTimeKind.Unspecified).AddTicks(7080), new DateTime(2021, 4, 3, 17, 55, 30, 913, DateTimeKind.Unspecified).AddTicks(82), "New range of formal shirts are designed keeping you in mind. With fits and styling that will make you stand apart", "Generic Wooden Mouse", 4 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 36, new DateTime(2020, 3, 15, 12, 51, 22, 448, DateTimeKind.Unspecified).AddTicks(7220), new DateTime(2020, 5, 6, 9, 10, 26, 427, DateTimeKind.Unspecified).AddTicks(1252), "Ergonomic executive chair upholstered in bonded black leather and PVC padded seat and back for all-day comfort and support", "Awesome Cotton Chair", 9 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 3, new DateTime(2020, 9, 29, 8, 11, 45, 724, DateTimeKind.Unspecified).AddTicks(4928), new DateTime(2020, 11, 19, 6, 23, 55, 71, DateTimeKind.Unspecified).AddTicks(7662), "Boston's most advanced compression wear technology increases muscle oxygenation, stabilizes active muscles", "Licensed Fresh Keyboard", 7 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 26, new DateTime(2018, 2, 13, 7, 36, 28, 333, DateTimeKind.Unspecified).AddTicks(7308), new DateTime(2020, 5, 8, 23, 54, 6, 558, DateTimeKind.Unspecified).AddTicks(9608), "The Football Is Good For Training And Recreational Purposes", "Handmade Plastic Ball", 8 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 35, new DateTime(2021, 1, 3, 22, 34, 11, 513, DateTimeKind.Unspecified).AddTicks(2464), new DateTime(2021, 9, 3, 19, 7, 24, 452, DateTimeKind.Unspecified).AddTicks(3865), "New range of formal shirts are designed keeping you in mind. With fits and styling that will make you stand apart", "Gorgeous Cotton Mouse", 10 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 4, new DateTime(2021, 2, 22, 7, 29, 51, 0, DateTimeKind.Unspecified).AddTicks(1105), new DateTime(2021, 7, 24, 21, 2, 38, 731, DateTimeKind.Unspecified).AddTicks(3823), "The Football Is Good For Training And Recreational Purposes", "Rustic Soft Fish", 6 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 31, new DateTime(2021, 5, 15, 13, 7, 13, 370, DateTimeKind.Unspecified).AddTicks(6944), new DateTime(2021, 11, 19, 15, 26, 25, 11, DateTimeKind.Unspecified).AddTicks(3391), "Boston's most advanced compression wear technology increases muscle oxygenation, stabilizes active muscles", "Gorgeous Steel Salad", 5 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 5, new DateTime(2018, 7, 20, 20, 2, 46, 425, DateTimeKind.Unspecified).AddTicks(8031), new DateTime(2021, 6, 10, 7, 23, 48, 396, DateTimeKind.Unspecified).AddTicks(8669), "New ABC 13 9370, 13.3, 5th Gen CoreA5-8250U, 8GB RAM, 256GB SSD, power UHD Graphics, OS 10 Home, OS Office A & J 2016", "Fantastic Steel Car", 3 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Name", "TeamId" },
                values: new object[] { 52, new DateTime(2020, 4, 2, 16, 2, 10, 142, DateTimeKind.Unspecified).AddTicks(4200), new DateTime(2021, 7, 27, 8, 22, 59, 319, DateTimeKind.Unspecified).AddTicks(6034), "Fantastic Cotton Fish", 9 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 28, 2, 5, 4, 289, DateTimeKind.Unspecified).AddTicks(9966), "Magnam nihil facere veniam exercitationem dolorem.", new DateTime(2021, 9, 4, 6, 29, 30, 612, DateTimeKind.Unspecified).AddTicks(8418), "Velit provident numquam recusandae ut.", 20, 13, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 3, 27, 3, 41, 50, 454, DateTimeKind.Unspecified).AddTicks(9740), "Et et officiis eos mollitia omnis aut saepe in eum sunt.", new DateTime(2021, 4, 10, 13, 3, 47, 946, DateTimeKind.Unspecified).AddTicks(7013), "Ex ut et.", 80, 29, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2020, 3, 5, 4, 37, 41, 62, DateTimeKind.Unspecified).AddTicks(8898), "Nemo rerum eos doloremque inventore et ut natus in velit nostrum rerum autem.", new DateTime(2020, 3, 12, 8, 6, 3, 944, DateTimeKind.Unspecified).AddTicks(9676), "Consequatur dolorem.", 11, 30 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 9, 7, 16, 47, 39, 583, DateTimeKind.Unspecified).AddTicks(3476), "Repudiandae id est incidunt earum impedit dolore ut ut beatae quibusdam ex aut.", new DateTime(2020, 11, 20, 6, 32, 23, 994, DateTimeKind.Unspecified).AddTicks(5090), "Corrupti perspiciatis vero.", 69, 32, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2020, 10, 19, 12, 1, 55, 401, DateTimeKind.Unspecified).AddTicks(608), "Laboriosam incidunt quis consectetur itaque a non.", new DateTime(2020, 11, 8, 6, 45, 10, 971, DateTimeKind.Unspecified).AddTicks(9979), "Dolores maiores sunt.", 76, 14 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 1, 18, 8, 51, 53, 608, DateTimeKind.Unspecified).AddTicks(8781), "Voluptatem veritatis rerum consequatur culpa iure harum voluptas numquam quae consequuntur et modi.", new DateTime(2021, 4, 11, 11, 34, 13, 908, DateTimeKind.Unspecified).AddTicks(4526), "Ducimus perferendis sapiente.", 22, 15, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2020, 12, 23, 1, 52, 0, 510, DateTimeKind.Unspecified).AddTicks(4097), "Reprehenderit illum quod occaecati et voluptates nulla omnis.", new DateTime(2021, 1, 10, 8, 4, 55, 291, DateTimeKind.Unspecified).AddTicks(8610), "Et sint.", 28, 26 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2020, 8, 3, 19, 31, 18, 284, DateTimeKind.Unspecified).AddTicks(5738), "Est voluptates ea impedit illo sunt dolorem quia omnis.", new DateTime(2020, 12, 24, 6, 4, 5, 586, DateTimeKind.Unspecified).AddTicks(7604), "Ratione qui omnis voluptas expedita.", 68, 8 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 1, 8, 19, 23, 36, 827, DateTimeKind.Unspecified).AddTicks(7164), "Sint distinctio velit repellendus est possimus perferendis aut cum dolores molestias nesciunt.", new DateTime(2021, 4, 14, 23, 27, 30, 974, DateTimeKind.Unspecified).AddTicks(7278), "Consequuntur et inventore atque.", 33, 40, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 5, 27, 3, 54, 31, 890, DateTimeKind.Unspecified).AddTicks(1988), "Est temporibus qui voluptas mollitia magni quas magnam sit voluptas cumque ea.", new DateTime(2021, 8, 22, 18, 27, 9, 714, DateTimeKind.Unspecified).AddTicks(912), "Totam vel.", 50, 38, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2018, 12, 6, 15, 14, 44, 201, DateTimeKind.Unspecified).AddTicks(5181), "Vero nemo dolore aliquam consectetur porro aperiam deserunt libero reiciendis adipisci officiis.", new DateTime(2020, 7, 2, 23, 37, 54, 206, DateTimeKind.Unspecified).AddTicks(8482), "Exercitationem beatae adipisci dignissimos fuga.", 41, 39 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 12, 19, 17, 42, 59, 348, DateTimeKind.Unspecified).AddTicks(9896), "Culpa cupiditate maiores autem ab ipsum.", new DateTime(2020, 1, 21, 1, 28, 11, 99, DateTimeKind.Unspecified).AddTicks(2718), "Distinctio consequatur aut quibusdam.", 62, 30, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 5, 8, 9, 59, 25, 472, DateTimeKind.Unspecified).AddTicks(2477), "Ullam exercitationem aut est a ipsam.", new DateTime(2021, 7, 17, 12, 8, 57, 931, DateTimeKind.Unspecified).AddTicks(6849), "Quis rerum magnam.", 54, 8, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 10, 28, 21, 54, 35, 161, DateTimeKind.Unspecified).AddTicks(9562), "Et nam minus sed molestiae voluptatibus.", new DateTime(2020, 11, 1, 6, 27, 2, 976, DateTimeKind.Unspecified).AddTicks(8612), "Rem repellendus illum accusamus.", 57, 17, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 1, 23, 7, 50, 44, 260, DateTimeKind.Unspecified).AddTicks(8100), "Quaerat distinctio ea excepturi optio odit odit ut suscipit qui et sed.", new DateTime(2019, 10, 30, 6, 35, 9, 384, DateTimeKind.Unspecified).AddTicks(4971), "Maiores nulla.", 28, 21, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 10, 14, 9, 3, 15, 116, DateTimeKind.Unspecified).AddTicks(512), "Enim est facere sed ea deleniti nisi quia consequatur.", new DateTime(2021, 10, 29, 18, 39, 12, 428, DateTimeKind.Unspecified).AddTicks(9873), "Velit quae eius occaecati vero.", 25, 38 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 2, 11, 17, 11, 18, 186, DateTimeKind.Unspecified).AddTicks(3453), "Qui facere exercitationem dolor sint est nisi esse consequatur.", new DateTime(2021, 2, 17, 12, 53, 24, 524, DateTimeKind.Unspecified).AddTicks(8928), "Hic dolores.", 60, 12, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 3, 3, 18, 47, 10, 661, DateTimeKind.Unspecified).AddTicks(4713), "At odit qui adipisci in at enim quasi eaque qui praesentium voluptates qui.", new DateTime(2020, 6, 13, 14, 51, 2, 2, DateTimeKind.Unspecified).AddTicks(1266), "Voluptatibus sed consequatur aliquid sunt.", 62, 5, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 4, 6, 22, 15, 59, 79, DateTimeKind.Unspecified).AddTicks(118), "Voluptate sunt aut odio aut dolorem mollitia.", new DateTime(2021, 1, 30, 12, 36, 55, 508, DateTimeKind.Unspecified).AddTicks(3885), "Consectetur maiores fugit veritatis id.", 68, 23, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 6, 7, 5, 8, 27, 676, DateTimeKind.Unspecified).AddTicks(6847), "Omnis aut necessitatibus cumque dolorum optio et tempore.", new DateTime(2020, 5, 15, 2, 15, 17, 957, DateTimeKind.Unspecified).AddTicks(4977), "Nisi et architecto nisi.", 60, 10, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 1, 25, 15, 19, 57, 53, DateTimeKind.Unspecified).AddTicks(1936), "Veniam quaerat harum recusandae error et ut.", new DateTime(2021, 2, 1, 6, 52, 15, 928, DateTimeKind.Unspecified).AddTicks(7310), "Pariatur maiores.", 76, 12, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 6, 28, 8, 30, 34, 687, DateTimeKind.Unspecified).AddTicks(7208), "Molestias quos voluptatem nihil hic ipsam sed molestiae recusandae tempora sit expedita aut.", new DateTime(2021, 6, 30, 7, 8, 59, 655, DateTimeKind.Unspecified).AddTicks(3678), "Veritatis sed rerum.", 52, 25, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 5, 17, 8, 54, 41, 895, DateTimeKind.Unspecified).AddTicks(5444), "Ipsa ut libero minus aut enim quaerat labore hic.", new DateTime(2020, 11, 22, 9, 50, 3, 287, DateTimeKind.Unspecified).AddTicks(9081), "Quo reprehenderit doloribus dolore perspiciatis.", 27, 22, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 3, 13, 7, 20, 10, 660, DateTimeKind.Unspecified).AddTicks(2000), "Est eos labore nisi quaerat nihil nemo molestiae qui occaecati et.", new DateTime(2021, 3, 15, 0, 12, 48, 470, DateTimeKind.Unspecified).AddTicks(389), "Explicabo quas harum quidem neque.", 70, 32, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 1, 3, 0, 30, 51, 732, DateTimeKind.Unspecified).AddTicks(98), "Et quidem non impedit nemo et dolorum rerum ea laborum vel temporibus ut.", new DateTime(2020, 10, 23, 13, 44, 56, 902, DateTimeKind.Unspecified).AddTicks(2090), "Consequuntur sint et quam.", 71, 15, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 8, 17, 3, 34, 31, 647, DateTimeKind.Unspecified).AddTicks(4524), "Nihil et tempora consequatur quas quisquam et.", new DateTime(2019, 9, 5, 2, 39, 46, 514, DateTimeKind.Unspecified).AddTicks(4334), "Enim iste perferendis fugit saepe.", 33, 9, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 6, 25, 8, 17, 17, 945, DateTimeKind.Unspecified).AddTicks(3539), "Harum qui quas rerum nihil odio corporis.", new DateTime(2021, 6, 26, 15, 22, 15, 783, DateTimeKind.Unspecified).AddTicks(48), "Sed odio nemo molestias.", 24, 24, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 11, 18, 5, 24, 26, 135, DateTimeKind.Unspecified).AddTicks(8602), "Rerum alias qui beatae at necessitatibus eos culpa ut culpa.", new DateTime(2020, 11, 19, 0, 37, 18, 613, DateTimeKind.Unspecified).AddTicks(5591), "Cupiditate eaque.", 42, 34, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 6, 14, 23, 12, 34, 419, DateTimeKind.Unspecified).AddTicks(7929), "Dolores tempore dolor neque repellendus quas molestiae est enim aspernatur.", new DateTime(2021, 6, 24, 4, 54, 34, 446, DateTimeKind.Unspecified).AddTicks(2498), "Ab at.", 2, 25, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 6, 11, 20, 12, 42, 975, DateTimeKind.Unspecified).AddTicks(794), "Nemo quod quaerat et exercitationem consequuntur laborum quia culpa.", new DateTime(2020, 9, 24, 21, 13, 9, 571, DateTimeKind.Unspecified).AddTicks(4661), "Odio voluptates.", 16, 11, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 2, 15, 2, 38, 56, 654, DateTimeKind.Unspecified).AddTicks(6229), "Voluptas est sunt non velit et.", new DateTime(2020, 3, 2, 7, 33, 17, 824, DateTimeKind.Unspecified).AddTicks(7533), "Ut et.", 49, 20, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 11, 20, 9, 0, 20, 885, DateTimeKind.Unspecified).AddTicks(8308), "Labore placeat corrupti aperiam quia dolor nisi molestiae illum asperiores autem omnis.", new DateTime(2020, 1, 5, 9, 41, 57, 229, DateTimeKind.Unspecified).AddTicks(7680), "Sed architecto explicabo dolorum perspiciatis.", 48, 2, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 5, 8, 10, 45, 32, 352, DateTimeKind.Unspecified).AddTicks(896), "Iusto similique exercitationem et quisquam mollitia est vel consequatur.", new DateTime(2020, 7, 28, 1, 28, 58, 516, DateTimeKind.Unspecified).AddTicks(4213), "Vero vel perspiciatis.", 10, 26, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 8, 8, 11, 10, 0, 820, DateTimeKind.Unspecified).AddTicks(3561), "Quae quos aliquid minima porro assumenda quo voluptate voluptatum autem omnis dolorem.", new DateTime(2021, 8, 28, 23, 17, 45, 935, DateTimeKind.Unspecified).AddTicks(6361), "Aut iusto qui qui.", 60, 8, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 5, 9, 8, 25, 12, 211, DateTimeKind.Unspecified).AddTicks(5393), "Maiores maxime ut est sed eaque excepturi autem et.", new DateTime(2021, 6, 27, 23, 14, 51, 666, DateTimeKind.Unspecified).AddTicks(2586), "Praesentium placeat cumque.", 76, 19, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 3, 14, 16, 12, 45, 993, DateTimeKind.Unspecified).AddTicks(5282), "Possimus voluptatem et non accusamus atque aut blanditiis et optio veritatis consequuntur recusandae fugiat.", new DateTime(2019, 10, 20, 8, 43, 25, 166, DateTimeKind.Unspecified).AddTicks(3829), "Perferendis ratione.", 31, 3, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 12, 13, 23, 49, 8, 672, DateTimeKind.Unspecified).AddTicks(605), "Consequatur nihil dolores consequatur cumque qui nesciunt molestias.", new DateTime(2021, 6, 6, 4, 38, 44, 697, DateTimeKind.Unspecified).AddTicks(8073), "Ut et eveniet soluta non.", 71, 40, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 7, 14, 6, 54, 30, 712, DateTimeKind.Unspecified).AddTicks(3847), "Minus id non delectus cumque id dignissimos laborum quidem nesciunt porro a nihil.", new DateTime(2019, 7, 21, 17, 42, 18, 90, DateTimeKind.Unspecified).AddTicks(2817), "Laborum beatae ut.", 55, 9, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 6, 13, 14, 40, 21, 855, DateTimeKind.Unspecified).AddTicks(6545), "Nobis beatae minus qui tenetur vel nesciunt error vitae hic adipisci est ea.", new DateTime(2020, 6, 15, 10, 52, 59, 352, DateTimeKind.Unspecified).AddTicks(2598), "Et repellat suscipit delectus molestiae.", 8, 30, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 2, 18, 4, 15, 43, 46, DateTimeKind.Unspecified).AddTicks(6718), "Nemo quos nemo nemo molestiae fugit aut vel in assumenda aspernatur quia perferendis distinctio.", new DateTime(2021, 4, 2, 22, 9, 10, 644, DateTimeKind.Unspecified).AddTicks(8754), "Autem aperiam.", 45, 29, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 41,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2020, 1, 27, 11, 37, 40, 638, DateTimeKind.Unspecified).AddTicks(3360), "Impedit sapiente sed molestias repudiandae dolore delectus harum consequatur est assumenda labore quam.", new DateTime(2020, 11, 2, 15, 50, 59, 995, DateTimeKind.Unspecified).AddTicks(8089), "Laboriosam ad dicta.", 22, 26 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 42,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 6, 23, 8, 25, 10, 648, DateTimeKind.Unspecified).AddTicks(4090), "Numquam velit voluptate modi ut quidem.", new DateTime(2021, 7, 1, 16, 13, 21, 407, DateTimeKind.Unspecified).AddTicks(3918), "Molestiae tempore veniam quo.", 48, 25, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 43,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 11, 7, 3, 21, 49, 277, DateTimeKind.Unspecified).AddTicks(1343), "Ipsam quis saepe rem totam aperiam voluptas sint rerum quasi.", new DateTime(2020, 2, 18, 1, 9, 49, 530, DateTimeKind.Unspecified).AddTicks(7857), "Repudiandae ullam ut voluptatem aut.", 20, 30 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 44,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 7, 7, 17, 40, 22, 887, DateTimeKind.Unspecified).AddTicks(5480), "Quod qui ipsum aut id quo et ab atque magni praesentium consequatur in.", new DateTime(2021, 7, 8, 12, 53, 28, 159, DateTimeKind.Unspecified).AddTicks(1995), "Nostrum quo.", 17, 25 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 45,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2018, 8, 24, 23, 31, 23, 175, DateTimeKind.Unspecified).AddTicks(2835), "Iste nemo maxime iure ipsum quisquam aut consequatur amet enim veritatis quasi ratione amet.", new DateTime(2018, 9, 3, 8, 52, 40, 294, DateTimeKind.Unspecified).AddTicks(8557), "Minus quis eveniet quisquam.", 68, 7, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 46,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 3, 11, 20, 1, 3, 901, DateTimeKind.Unspecified).AddTicks(7770), "Et praesentium qui ut unde voluptatibus.", new DateTime(2021, 4, 15, 20, 25, 26, 866, DateTimeKind.Unspecified).AddTicks(561), "Dolor cumque voluptas.", 44, 19, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 47,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 10, 30, 5, 11, 3, 765, DateTimeKind.Unspecified).AddTicks(4583), "Voluptas blanditiis sit dicta natus dolores qui labore ab aut ipsum reprehenderit.", new DateTime(2020, 2, 21, 10, 41, 3, 940, DateTimeKind.Unspecified).AddTicks(7098), "Sunt occaecati aut.", 79, 3, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 48,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 1, 17, 0, 33, 36, 374, DateTimeKind.Unspecified).AddTicks(5037), "Quia nulla eius qui ut rerum quisquam perspiciatis eaque aut.", new DateTime(2019, 3, 13, 23, 0, 15, 944, DateTimeKind.Unspecified).AddTicks(7530), "Totam impedit eveniet ut.", 20, 4, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 49,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2020, 7, 26, 3, 37, 50, 326, DateTimeKind.Unspecified).AddTicks(8607), "Iusto consequatur architecto temporibus suscipit ex eligendi blanditiis eum illo ipsum et nihil.", new DateTime(2020, 11, 27, 11, 34, 53, 114, DateTimeKind.Unspecified).AddTicks(5177), "Repellendus rerum qui.", 60, 40 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 50,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 6, 19, 20, 9, 12, 65, DateTimeKind.Unspecified).AddTicks(8860), "Veniam explicabo suscipit id tempora soluta quasi ipsam ab voluptatem qui est molestiae veritatis.", new DateTime(2021, 6, 22, 23, 25, 20, 661, DateTimeKind.Unspecified).AddTicks(7177), "Mollitia voluptas similique magni nostrum.", 79, 25, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 51,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 3, 28, 19, 43, 13, 733, DateTimeKind.Unspecified).AddTicks(1462), "Quae temporibus maiores cupiditate et soluta repellendus.", new DateTime(2020, 4, 11, 13, 15, 51, 103, DateTimeKind.Unspecified).AddTicks(7294), "Non harum qui ut alias.", 58, 33, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 52,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 3, 15, 9, 28, 3, 133, DateTimeKind.Unspecified).AddTicks(9110), "Autem aut qui totam voluptates cum ut facilis veritatis velit.", new DateTime(2020, 4, 24, 19, 35, 54, 814, DateTimeKind.Unspecified).AddTicks(3918), "Quis aut.", 30, 6, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 53,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 10, 12, 3, 30, 43, 490, DateTimeKind.Unspecified).AddTicks(905), "Sequi porro maiores et aut ex deserunt unde alias laborum.", new DateTime(2020, 11, 9, 23, 1, 37, 398, DateTimeKind.Unspecified).AddTicks(696), "Minus sunt harum minima sit.", 77, 34, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 54,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2020, 11, 10, 19, 47, 0, 805, DateTimeKind.Unspecified).AddTicks(7775), "Blanditiis provident nihil ipsa ea saepe natus maiores et id.", new DateTime(2020, 11, 17, 17, 18, 59, 175, DateTimeKind.Unspecified).AddTicks(4858), "Tempora consequatur iste laboriosam.", 70, 34 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 55,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 6, 18, 0, 42, 32, 382, DateTimeKind.Unspecified).AddTicks(148), "Excepturi voluptate provident sed aut reprehenderit.", new DateTime(2021, 7, 6, 15, 10, 9, 679, DateTimeKind.Unspecified).AddTicks(3147), "Exercitationem libero aut.", 22, 25 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 56,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 4, 8, 15, 20, 2, 715, DateTimeKind.Unspecified).AddTicks(7469), "Sunt id praesentium aperiam error itaque officiis repellat odio ea itaque.", new DateTime(2019, 4, 27, 0, 55, 15, 209, DateTimeKind.Unspecified).AddTicks(7256), "Doloribus veritatis optio sit.", 54, 4 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 57,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 1, 3, 23, 45, 35, 44, DateTimeKind.Unspecified).AddTicks(2512), "Recusandae aut quis est temporibus sed rem aut incidunt temporibus sed nobis sint.", new DateTime(2021, 1, 6, 2, 10, 13, 481, DateTimeKind.Unspecified).AddTicks(7754), "Hic ab alias.", 76, 29, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 58,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 6, 6, 14, 34, 4, 887, DateTimeKind.Unspecified).AddTicks(981), "Tempore maxime qui molestiae similique quae nihil aut est sit tempore rerum mollitia quia.", new DateTime(2021, 7, 30, 16, 34, 8, 904, DateTimeKind.Unspecified).AddTicks(147), "Expedita sed tempore.", 72, 13, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 59,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 2, 7, 10, 50, 39, 44, DateTimeKind.Unspecified).AddTicks(6686), "Dolores numquam harum magnam ut consequatur qui nisi aliquid.", new DateTime(2021, 8, 5, 5, 8, 9, 137, DateTimeKind.Unspecified).AddTicks(1103), "Et qui.", 52, 11, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 60,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 11, 17, 10, 41, 55, 463, DateTimeKind.Unspecified).AddTicks(1321), "Rem quas dolore vitae totam magnam quo ipsum minus aliquam rerum laudantium.", new DateTime(2019, 11, 20, 8, 54, 14, 326, DateTimeKind.Unspecified).AddTicks(5536), "Et inventore non.", 58, 15 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 61,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 10, 13, 16, 51, 36, 424, DateTimeKind.Unspecified).AddTicks(9035), "Autem in natus voluptas magni praesentium dolores quo.", new DateTime(2020, 9, 29, 12, 27, 24, 525, DateTimeKind.Unspecified).AddTicks(3596), "Quibusdam iusto odit veritatis quo.", 72, 28, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 62,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2018, 10, 25, 0, 58, 55, 989, DateTimeKind.Unspecified).AddTicks(2089), "Fugiat pariatur dicta quo quibusdam est voluptas rerum rerum error.", new DateTime(2020, 4, 25, 15, 14, 48, 298, DateTimeKind.Unspecified).AddTicks(9098), "Dolorem alias.", 33, 39, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 63,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 4, 25, 19, 42, 41, 527, DateTimeKind.Unspecified).AddTicks(9314), "Eaque ad possimus et repellendus et quas iusto fugit.", new DateTime(2021, 6, 28, 17, 46, 44, 495, DateTimeKind.Unspecified).AddTicks(2249), "Molestiae nihil saepe eligendi.", 22, 24 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 64,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 5, 4, 0, 42, 28, 759, DateTimeKind.Unspecified).AddTicks(6187), "Molestiae autem est corrupti saepe cupiditate in officia consequatur itaque et.", new DateTime(2019, 12, 28, 15, 1, 45, 744, DateTimeKind.Unspecified).AddTicks(444), "Facere est.", 14, 39, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 65,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 3, 13, 3, 29, 43, 413, DateTimeKind.Unspecified).AddTicks(7374), "Quia non voluptatibus quia totam quia ullam adipisci.", new DateTime(2021, 7, 20, 19, 30, 37, 170, DateTimeKind.Unspecified).AddTicks(1902), "In voluptas voluptates perferendis eius.", 72, 37, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 66,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 2, 10, 15, 11, 27, 75, DateTimeKind.Unspecified).AddTicks(7103), "Et error adipisci voluptatem voluptatem dolores atque.", new DateTime(2021, 3, 9, 12, 37, 37, 952, DateTimeKind.Unspecified).AddTicks(6640), "Omnis vel ut modi.", 57, 29, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 67,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 6, 19, 1, 42, 19, 691, DateTimeKind.Unspecified).AddTicks(2531), "Voluptatem aut vel et maxime voluptas est corporis corporis esse rem et adipisci explicabo.", new DateTime(2020, 6, 5, 4, 57, 46, 609, DateTimeKind.Unspecified).AddTicks(2265), "Nihil sed deleniti sapiente.", 28, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 68,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 6, 25, 23, 26, 34, 363, DateTimeKind.Unspecified).AddTicks(2392), "Dolorem iste corrupti et iste voluptatem commodi numquam officia totam.", new DateTime(2021, 7, 24, 20, 41, 48, 200, DateTimeKind.Unspecified).AddTicks(2458), "Fugiat quo maiores.", 66, 37, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 69,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2018, 5, 29, 16, 30, 32, 259, DateTimeKind.Unspecified).AddTicks(3667), "Dignissimos voluptas perferendis placeat iste quo totam qui.", new DateTime(2019, 3, 27, 10, 54, 45, 55, DateTimeKind.Unspecified).AddTicks(6521), "Et nulla consectetur quasi et.", 20, 4, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 70,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 6, 16, 3, 33, 28, 895, DateTimeKind.Unspecified).AddTicks(7900), "Eaque ab aliquam expedita qui dolor similique alias voluptatem debitis voluptatem debitis.", new DateTime(2021, 6, 26, 12, 35, 51, 429, DateTimeKind.Unspecified).AddTicks(7714), "Voluptas aperiam.", 48, 40, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 71,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2018, 9, 2, 6, 1, 57, 633, DateTimeKind.Unspecified).AddTicks(9124), "Impedit cupiditate placeat consequatur iure nemo doloribus non possimus et voluptatem laudantium aspernatur.", new DateTime(2018, 9, 2, 23, 52, 20, 832, DateTimeKind.Unspecified).AddTicks(4464), "Neque animi.", 58, 7, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 72,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 9, 29, 10, 3, 47, 8, DateTimeKind.Unspecified).AddTicks(9718), "Sapiente ipsam nihil explicabo dicta quia quidem et vitae.", new DateTime(2020, 10, 11, 18, 8, 38, 694, DateTimeKind.Unspecified).AddTicks(703), "Dignissimos placeat.", 43, 28, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 73,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 1, 7, 17, 3, 6, 168, DateTimeKind.Unspecified).AddTicks(9124), "Qui magnam consequuntur esse rerum consequatur.", new DateTime(2021, 4, 11, 17, 37, 51, 704, DateTimeKind.Unspecified).AddTicks(8432), "Et laboriosam accusamus culpa.", 2, 36, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 74,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 4, 8, 19, 53, 59, 4, DateTimeKind.Unspecified).AddTicks(313), "Et ullam nostrum modi dolor exercitationem odit unde ut doloremque neque.", new DateTime(2020, 5, 4, 7, 45, 54, 60, DateTimeKind.Unspecified).AddTicks(9099), "Inventore nemo.", 31, 33, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 75,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 7, 25, 10, 16, 3, 389, DateTimeKind.Unspecified).AddTicks(3875), "Consectetur nostrum non porro nemo consequatur quidem est distinctio veniam aut accusantium.", new DateTime(2019, 8, 26, 0, 22, 7, 971, DateTimeKind.Unspecified).AddTicks(9093), "Et cum consequatur.", 52, 39 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 76,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2018, 10, 10, 23, 32, 20, 355, DateTimeKind.Unspecified).AddTicks(3184), "Unde velit temporibus sunt perferendis id quis vel ullam at et aut.", new DateTime(2019, 4, 19, 17, 3, 42, 308, DateTimeKind.Unspecified).AddTicks(2476), "Omnis nostrum nihil dolor eum.", 38, 4, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 77,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 2, 23, 3, 20, 35, 321, DateTimeKind.Unspecified).AddTicks(1518), "In sit et et ut porro consequatur velit et.", new DateTime(2020, 3, 2, 0, 29, 10, 629, DateTimeKind.Unspecified).AddTicks(1589), "Nobis placeat minima quibusdam cupiditate.", 44, 20, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 78,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2020, 3, 20, 8, 49, 26, 666, DateTimeKind.Unspecified).AddTicks(7500), "Eum laboriosam sapiente qui voluptas at.", new DateTime(2020, 4, 19, 8, 35, 43, 627, DateTimeKind.Unspecified).AddTicks(4906), "Molestiae asperiores et est.", 36, 33 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 79,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 5, 22, 19, 53, 1, 806, DateTimeKind.Unspecified).AddTicks(4054), "Deleniti culpa pariatur fugiat odit animi suscipit sunt.", new DateTime(2021, 5, 8, 14, 26, 54, 986, DateTimeKind.Unspecified).AddTicks(4664), "Necessitatibus eum sint nobis aut.", 61, 14, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 80,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 8, 25, 15, 12, 23, 138, DateTimeKind.Unspecified).AddTicks(6998), "Id quae eveniet nesciunt ipsam ut ipsa esse consequatur adipisci.", new DateTime(2020, 10, 25, 4, 56, 57, 954, DateTimeKind.Unspecified).AddTicks(7172), "Quasi velit sunt ut quis.", 2, 2, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 81,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 7, 18, 19, 38, 39, 813, DateTimeKind.Unspecified).AddTicks(3856), "Commodi aut facere quas saepe expedita suscipit aut.", new DateTime(2021, 9, 20, 19, 55, 6, 163, DateTimeKind.Unspecified).AddTicks(4158), "Quaerat repellat.", 23, 27 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 82,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 8, 31, 7, 41, 51, 285, DateTimeKind.Unspecified).AddTicks(9790), "Est occaecati voluptas iste et similique sequi eveniet velit rerum maiores voluptas.", new DateTime(2021, 8, 31, 9, 34, 56, 278, DateTimeKind.Unspecified).AddTicks(4885), "Dolorum corrupti et.", 1, 38, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 83,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 3, 25, 2, 42, 39, 237, DateTimeKind.Unspecified).AddTicks(6603), "Eum vel id non eum placeat cum adipisci veniam qui praesentium sunt deleniti qui.", new DateTime(2020, 4, 9, 0, 2, 12, 370, DateTimeKind.Unspecified).AddTicks(4627), "Voluptatibus perspiciatis consequatur quas.", 80, 33, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 84,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 5, 14, 9, 4, 46, 897, DateTimeKind.Unspecified).AddTicks(828), "Voluptas ut minima nemo itaque quas necessitatibus cum amet sed blanditiis dolor.", new DateTime(2021, 6, 4, 3, 40, 37, 355, DateTimeKind.Unspecified).AddTicks(3729), "Iure eveniet eveniet.", 42, 8, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 85,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 11, 30, 3, 49, 20, 804, DateTimeKind.Unspecified).AddTicks(9869), "Velit quibusdam autem consectetur aut quae deserunt atque deleniti voluptatibus velit.", new DateTime(2020, 1, 12, 8, 41, 21, 775, DateTimeKind.Unspecified).AddTicks(734), "Cumque sit totam culpa aspernatur.", 63, 6, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 86,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 5, 29, 0, 52, 6, 45, DateTimeKind.Unspecified).AddTicks(9142), "Cumque voluptatem culpa pariatur recusandae quidem adipisci porro quis placeat eos.", new DateTime(2021, 8, 29, 9, 35, 28, 721, DateTimeKind.Unspecified).AddTicks(6491), "Dolor eum earum itaque.", 56, 27 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 87,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 6, 21, 10, 1, 57, 507, DateTimeKind.Unspecified).AddTicks(4244), "Quo fuga cum nesciunt dolores molestiae corrupti.", new DateTime(2020, 5, 23, 5, 32, 17, 259, DateTimeKind.Unspecified).AddTicks(5486), "Qui rerum aut optio.", 46, 28, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 88,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 9, 15, 16, 19, 17, 340, DateTimeKind.Unspecified).AddTicks(6254), "Aut tenetur quis voluptas eligendi qui consequatur.", new DateTime(2020, 9, 28, 10, 59, 25, 347, DateTimeKind.Unspecified).AddTicks(9665), "Rerum est.", 80, 32, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 89,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 10, 26, 5, 49, 1, 876, DateTimeKind.Unspecified).AddTicks(4973), "Molestiae et ipsam a ipsum omnis qui enim tempora animi omnis voluptatibus alias provident.", new DateTime(2020, 5, 8, 16, 12, 14, 136, DateTimeKind.Unspecified).AddTicks(6047), "Labore asperiores.", 3, 2, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 90,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 2, 17, 0, 57, 8, 71, DateTimeKind.Unspecified).AddTicks(8303), "Eveniet autem eos dolores sunt a nesciunt nihil est animi voluptas et doloribus.", new DateTime(2020, 12, 12, 23, 37, 6, 932, DateTimeKind.Unspecified).AddTicks(1076), "Nobis recusandae molestias eum et.", 8, 14, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 91,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 11, 6, 11, 2, 56, 864, DateTimeKind.Unspecified).AddTicks(8199), "Animi quia consequatur veniam asperiores doloribus non laboriosam quis aut voluptas voluptate minus doloremque.", new DateTime(2021, 6, 9, 12, 31, 3, 193, DateTimeKind.Unspecified).AddTicks(2507), "Voluptatem autem qui beatae asperiores.", 36, 24, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 92,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2018, 8, 26, 18, 26, 24, 193, DateTimeKind.Unspecified).AddTicks(9680), "Qui esse atque deserunt sunt ipsum ullam accusamus deserunt ut placeat.", new DateTime(2018, 8, 31, 12, 19, 52, 635, DateTimeKind.Unspecified).AddTicks(8909), "Iusto occaecati optio dolor.", 32, 7, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 93,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2020, 10, 29, 7, 29, 26, 287, DateTimeKind.Unspecified).AddTicks(4822), "Aut et nemo velit quos voluptas quidem magnam totam vitae excepturi.", new DateTime(2020, 11, 10, 8, 31, 25, 469, DateTimeKind.Unspecified).AddTicks(9396), "Cumque blanditiis ab.", 36, 34 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 94,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2020, 4, 6, 18, 54, 5, 530, DateTimeKind.Unspecified).AddTicks(3498), "Inventore nulla libero debitis ut eum nisi.", new DateTime(2020, 4, 27, 5, 25, 50, 994, DateTimeKind.Unspecified).AddTicks(4735), "Velit impedit laudantium ut.", 5, 5 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 95,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 5, 14, 18, 57, 25, 278, DateTimeKind.Unspecified).AddTicks(1238), "Distinctio et consequuntur necessitatibus labore voluptas aut ab.", new DateTime(2020, 8, 13, 20, 16, 3, 54, DateTimeKind.Unspecified).AddTicks(5694), "Aliquid quia reprehenderit mollitia.", 60, 2, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 96,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 6, 11, 19, 27, 49, 227, DateTimeKind.Unspecified).AddTicks(9940), "Laborum est consequatur et nisi quaerat ut optio error perferendis.", new DateTime(2019, 8, 9, 14, 26, 32, 184, DateTimeKind.Unspecified).AddTicks(2835), "Quasi libero consequatur laudantium.", 31, 9, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 97,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 3, 22, 10, 12, 23, 118, DateTimeKind.Unspecified).AddTicks(1475), "Sit autem reprehenderit omnis voluptatem et.", new DateTime(2021, 4, 25, 5, 55, 57, 541, DateTimeKind.Unspecified).AddTicks(1520), "Et perferendis dolorem id laboriosam.", 57, 22, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 98,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 9, 13, 10, 36, 1, 292, DateTimeKind.Unspecified).AddTicks(9115), "Ut et et eaque vel soluta eius ut commodi omnis voluptatem.", new DateTime(2021, 9, 17, 17, 3, 17, 903, DateTimeKind.Unspecified).AddTicks(6378), "Voluptatibus quisquam at quo dolor.", 44, 27, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 99,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 11, 16, 9, 12, 37, 494, DateTimeKind.Unspecified).AddTicks(4227), "Assumenda sequi culpa sunt aut iste quos et distinctio tenetur non ut sed quidem.", new DateTime(2021, 4, 12, 13, 50, 43, 641, DateTimeKind.Unspecified).AddTicks(6647), "Perferendis sint autem.", 57, 29, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 100,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 6, 7, 16, 58, 57, 201, DateTimeKind.Unspecified).AddTicks(7897), "Ut quas at libero dignissimos exercitationem quo velit doloribus eligendi sed voluptatum distinctio.", new DateTime(2021, 10, 5, 20, 11, 49, 575, DateTimeKind.Unspecified).AddTicks(610), "Quam dolores voluptas.", 23, 38, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 101,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 11, 14, 10, 0, 43, 171, DateTimeKind.Unspecified).AddTicks(2368), "Deleniti impedit veniam aperiam aut praesentium occaecati harum.", new DateTime(2019, 11, 26, 18, 48, 50, 647, DateTimeKind.Unspecified).AddTicks(4747), "Dolorem earum quam corporis.", 57, 16, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 102,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 10, 19, 12, 26, 5, 666, DateTimeKind.Unspecified).AddTicks(3479), "Iusto ut quia adipisci ab officia id eos et earum necessitatibus pariatur velit.", new DateTime(2020, 10, 20, 23, 43, 12, 29, DateTimeKind.Unspecified).AddTicks(1552), "Quia commodi sed perferendis et.", 65, 26, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 103,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 7, 17, 6, 22, 52, 501, DateTimeKind.Unspecified).AddTicks(7433), "Et quia omnis quam et odit.", new DateTime(2021, 7, 24, 8, 23, 30, 733, DateTimeKind.Unspecified).AddTicks(8381), "Provident quia similique.", 37, 14 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 104,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 5, 30, 5, 18, 58, 786, DateTimeKind.Unspecified).AddTicks(3594), "Eum velit dignissimos et aut dolor quasi harum dignissimos ut ipsam et quo.", new DateTime(2021, 7, 3, 10, 45, 57, 38, DateTimeKind.Unspecified).AddTicks(805), "Et sed dicta voluptatibus numquam.", 6, 27 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 105,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 6, 12, 12, 45, 43, 102, DateTimeKind.Unspecified).AddTicks(4377), "Ut a cum id voluptas earum.", new DateTime(2021, 7, 2, 7, 42, 24, 630, DateTimeKind.Unspecified).AddTicks(8435), "Dolorem cumque doloremque.", 55, 8, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 106,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 12, 8, 14, 37, 9, 582, DateTimeKind.Unspecified).AddTicks(6997), "Enim nam doloribus reprehenderit enim praesentium autem et aspernatur.", new DateTime(2020, 12, 20, 19, 18, 37, 706, DateTimeKind.Unspecified).AddTicks(3504), "Consequatur facilis ipsa voluptatem autem.", 13, 11, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 107,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 5, 5, 3, 30, 4, 86, DateTimeKind.Unspecified).AddTicks(8875), "Odio incidunt eligendi et quam dolores odio aperiam.", new DateTime(2021, 6, 1, 9, 18, 36, 110, DateTimeKind.Unspecified).AddTicks(6550), "Odit non et.", 74, 36 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 108,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 11, 30, 1, 9, 23, 438, DateTimeKind.Unspecified).AddTicks(253), "Aut voluptas est numquam rerum et nulla non repudiandae.", new DateTime(2021, 3, 28, 19, 23, 7, 332, DateTimeKind.Unspecified).AddTicks(5168), "Illum molestiae.", 21, 14, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 109,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 10, 27, 3, 42, 52, 506, DateTimeKind.Unspecified).AddTicks(1715), "Culpa quam similique saepe accusamus molestiae repudiandae.", new DateTime(2020, 10, 27, 15, 48, 11, 164, DateTimeKind.Unspecified).AddTicks(5165), "Dolores at itaque.", 30, 17, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 110,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 8, 23, 13, 38, 11, 93, DateTimeKind.Unspecified).AddTicks(2480), "Consequatur quaerat vitae quos nemo nulla.", new DateTime(2021, 1, 2, 23, 34, 51, 165, DateTimeKind.Unspecified).AddTicks(8469), "Est maiores.", 41, 13, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 111,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 8, 27, 4, 26, 44, 118, DateTimeKind.Unspecified).AddTicks(558), "Amet optio nulla rerum sed hic.", new DateTime(2020, 12, 14, 7, 46, 46, 838, DateTimeKind.Unspecified).AddTicks(7799), "Labore et dicta.", 20, 32, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 112,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 6, 11, 14, 54, 8, 833, DateTimeKind.Unspecified).AddTicks(3153), "Et debitis est dignissimos consequatur minus odit veritatis.", new DateTime(2020, 6, 22, 19, 36, 37, 438, DateTimeKind.Unspecified).AddTicks(4067), "Qui ut saepe quibusdam qui.", 15, 30, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 113,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 25, 6, 0, 43, 654, DateTimeKind.Unspecified).AddTicks(4694), "Ipsa totam esse atque nostrum praesentium vel animi et est adipisci quam impedit.", new DateTime(2021, 7, 25, 16, 37, 6, 40, DateTimeKind.Unspecified).AddTicks(3790), "A accusantium dolor provident.", 24, 40, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 114,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 3, 15, 16, 39, 6, 970, DateTimeKind.Unspecified).AddTicks(9189), "Eius accusamus iste enim porro molestiae quibusdam.", new DateTime(2021, 3, 23, 13, 36, 57, 994, DateTimeKind.Unspecified).AddTicks(5314), "Necessitatibus fugit voluptas voluptatum.", 64, 32 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 115,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 8, 2, 17, 12, 2, 572, DateTimeKind.Unspecified).AddTicks(7698), "Tempore autem sed eos quia quos sed.", new DateTime(2020, 12, 24, 12, 40, 50, 935, DateTimeKind.Unspecified).AddTicks(120), "Qui deleniti voluptatibus repellat.", 52, 22, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 116,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 5, 20, 16, 20, 20, 464, DateTimeKind.Unspecified).AddTicks(6315), "Et rerum quas saepe ea inventore temporibus sunt adipisci ut illum vel omnis necessitatibus.", new DateTime(2021, 3, 13, 9, 21, 21, 588, DateTimeKind.Unspecified).AddTicks(493), "Velit asperiores velit voluptatem quam.", 7, 40, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 117,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 9, 13, 22, 11, 12, 444, DateTimeKind.Unspecified).AddTicks(9568), "Ex quo sequi numquam autem blanditiis.", new DateTime(2020, 1, 14, 16, 50, 8, 441, DateTimeKind.Unspecified).AddTicks(5066), "Veritatis aut sit dolor.", 20, 6 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 118,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 6, 28, 21, 52, 52, 321, DateTimeKind.Unspecified).AddTicks(1502), "Quam et aut amet non et.", new DateTime(2020, 7, 3, 6, 14, 10, 910, DateTimeKind.Unspecified).AddTicks(2418), "Atque magni.", 72, 28, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 119,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 3, 21, 6, 11, 31, 282, DateTimeKind.Unspecified).AddTicks(9807), "Quia eum quae nisi harum ex in saepe nobis placeat nobis iure.", new DateTime(2020, 5, 18, 19, 41, 20, 326, DateTimeKind.Unspecified).AddTicks(3623), "Dolor possimus nulla iure.", 1, 30, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 120,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId" },
                values: new object[] { new DateTime(2021, 8, 26, 0, 6, 47, 854, DateTimeKind.Unspecified).AddTicks(3431), "Magnam qui soluta a quis dolor ut earum tenetur corporis incidunt aut unde pariatur.", new DateTime(2021, 9, 26, 20, 59, 33, 615, DateTimeKind.Unspecified).AddTicks(9182), "Eos aut.", 46 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 121,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2018, 12, 2, 7, 12, 55, 642, DateTimeKind.Unspecified).AddTicks(3378), "Ab eaque ad a quia sint impedit veniam nihil.", new DateTime(2019, 5, 30, 20, 5, 42, 227, DateTimeKind.Unspecified).AddTicks(5282), "Exercitationem dolore ipsum doloribus.", 22, 3, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 122,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 1, 17, 22, 40, 38, 508, DateTimeKind.Unspecified).AddTicks(9989), "Ducimus blanditiis minima amet ratione delectus totam enim praesentium sunt dolores cumque.", new DateTime(2020, 3, 18, 2, 16, 17, 109, DateTimeKind.Unspecified).AddTicks(5833), "Et hic.", 26, 1, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 123,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 11, 7, 4, 34, 58, 593, DateTimeKind.Unspecified).AddTicks(5297), "Repellendus occaecati eum voluptas est delectus quisquam ut occaecati iure odit nisi excepturi.", new DateTime(2021, 3, 24, 20, 54, 22, 793, DateTimeKind.Unspecified).AddTicks(9343), "Fuga maiores.", 72, 29, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 124,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2020, 10, 27, 11, 42, 19, 178, DateTimeKind.Unspecified).AddTicks(2285), "Temporibus consequatur et quo tempore autem earum.", new DateTime(2020, 11, 11, 14, 1, 22, 893, DateTimeKind.Unspecified).AddTicks(4713), "Quos ut illo.", 53, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 125,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 11, 16, 11, 17, 59, 881, DateTimeKind.Unspecified).AddTicks(9737), "Itaque sit iure ex quos magnam sequi et blanditiis magnam enim dolores debitis qui.", new DateTime(2019, 12, 8, 4, 31, 53, 511, DateTimeKind.Unspecified).AddTicks(7744), "Ducimus ut quis repellat maxime.", 21, 16, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 126,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 11, 5, 4, 41, 12, 855, DateTimeKind.Unspecified).AddTicks(5367), "Incidunt vel natus inventore animi rerum occaecati sint ipsam.", new DateTime(2020, 6, 24, 4, 52, 45, 854, DateTimeKind.Unspecified).AddTicks(2971), "Iste culpa.", 33, 30, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 127,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 10, 20, 5, 16, 58, 957, DateTimeKind.Unspecified).AddTicks(1446), "Repellendus similique rem architecto recusandae omnis velit excepturi nam quod.", new DateTime(2020, 8, 31, 21, 18, 5, 737, DateTimeKind.Unspecified).AddTicks(4493), "In laudantium occaecati exercitationem.", 13, 28, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 128,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 4, 11, 22, 30, 51, 588, DateTimeKind.Unspecified).AddTicks(8027), "Asperiores placeat ut laborum molestiae quidem amet magni autem non quaerat sunt.", new DateTime(2021, 5, 27, 2, 6, 11, 672, DateTimeKind.Unspecified).AddTicks(4621), "Recusandae consectetur omnis eos recusandae.", 11, 23, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 129,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 3, 30, 10, 45, 24, 58, DateTimeKind.Unspecified).AddTicks(8719), "In ab laborum omnis consequuntur dolore tempore facilis enim delectus iusto omnis ab.", new DateTime(2019, 12, 22, 7, 18, 54, 370, DateTimeKind.Unspecified).AddTicks(141), "Incidunt aut.", 10, 23 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 130,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 6, 21, 20, 19, 44, 22, DateTimeKind.Unspecified).AddTicks(3435), "Quaerat quos et dignissimos et quas et molestiae.", new DateTime(2021, 7, 13, 5, 9, 27, 557, DateTimeKind.Unspecified).AddTicks(1458), "Quia sed hic ipsum odit.", 69, 36, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 131,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 3, 25, 20, 8, 29, 277, DateTimeKind.Unspecified).AddTicks(8542), "Natus dolorem ut occaecati sunt amet enim consequatur non blanditiis.", new DateTime(2020, 10, 9, 19, 45, 26, 551, DateTimeKind.Unspecified).AddTicks(5455), "Iure sint corrupti.", 41, 28, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 132,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 21, 11, 21, 11, 314, DateTimeKind.Unspecified).AddTicks(9143), "Sed laudantium aut fuga dolorem sequi accusantium.", new DateTime(2021, 8, 7, 13, 38, 6, 408, DateTimeKind.Unspecified).AddTicks(8180), "Accusamus possimus id.", 65, 36, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 133,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 2, 23, 9, 37, 6, 164, DateTimeKind.Unspecified).AddTicks(2111), "Est id in aut magni quaerat sunt ipsum.", new DateTime(2020, 6, 8, 5, 30, 33, 179, DateTimeKind.Unspecified).AddTicks(6562), "Molestiae est.", 63, 10, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 134,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 3, 17, 20, 7, 31, 472, DateTimeKind.Unspecified).AddTicks(4749), "Voluptatem explicabo voluptatem sint aliquid itaque neque molestiae.", new DateTime(2021, 6, 22, 9, 6, 42, 12, DateTimeKind.Unspecified).AddTicks(4149), "Et harum repudiandae.", 22, 14, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 135,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 7, 22, 11, 12, 25, 342, DateTimeKind.Unspecified).AddTicks(492), "Magni ea quisquam et rerum saepe harum eum id ea quia aspernatur.", new DateTime(2020, 9, 7, 2, 34, 14, 153, DateTimeKind.Unspecified).AddTicks(4525), "Enim modi.", 61, 5, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 136,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 9, 5, 13, 51, 6, 391, DateTimeKind.Unspecified).AddTicks(3641), "Iure praesentium porro nihil sint laudantium aut consequatur exercitationem animi perspiciatis error deserunt.", new DateTime(2020, 12, 5, 4, 52, 40, 831, DateTimeKind.Unspecified).AddTicks(3136), "Excepturi quaerat.", 30, 13, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 137,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 10, 4, 18, 0, 16, 850, DateTimeKind.Unspecified).AddTicks(2635), "Culpa a voluptas labore quisquam ipsam reiciendis excepturi dicta omnis sapiente a.", new DateTime(2020, 1, 12, 9, 28, 8, 479, DateTimeKind.Unspecified).AddTicks(6757), "Iure provident.", 3, 30, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 138,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 4, 4, 12, 50, 59, 21, DateTimeKind.Unspecified).AddTicks(1847), "Distinctio sed excepturi dicta atque nihil sed id nemo accusamus ad rerum.", new DateTime(2021, 7, 16, 19, 53, 8, 1, DateTimeKind.Unspecified).AddTicks(6679), "Provident sit nemo.", 60, 36, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 139,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 1, 9, 10, 14, 17, 698, DateTimeKind.Unspecified).AddTicks(6255), "Iste quis amet vero nisi omnis vero sint odio dolorem exercitationem.", new DateTime(2021, 3, 20, 14, 6, 58, 306, DateTimeKind.Unspecified).AddTicks(263), "Iure repellendus sint repellendus animi.", 22, 32, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 140,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 1, 26, 16, 15, 5, 334, DateTimeKind.Unspecified).AddTicks(5762), "Et doloribus aliquid soluta placeat et.", new DateTime(2021, 6, 18, 15, 30, 10, 951, DateTimeKind.Unspecified).AddTicks(9064), "Ut qui sunt soluta aut.", 70, 22, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 141,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 4, 1, 10, 3, 46, 358, DateTimeKind.Unspecified).AddTicks(8822), "Perferendis omnis sed illo totam culpa hic fugit tenetur fugit.", new DateTime(2020, 4, 19, 6, 5, 27, 573, DateTimeKind.Unspecified).AddTicks(2233), "Fugit occaecati cum qui.", 77, 33, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 142,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 3, 26, 17, 3, 27, 939, DateTimeKind.Unspecified).AddTicks(573), "Aspernatur odit necessitatibus repudiandae qui quam eos at aspernatur porro facere ullam hic et.", new DateTime(2019, 5, 3, 18, 25, 41, 853, DateTimeKind.Unspecified).AddTicks(5181), "Ut rerum accusamus.", 62, 4, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 143,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2020, 2, 10, 11, 6, 2, 796, DateTimeKind.Unspecified).AddTicks(9861), "Porro quasi perspiciatis nihil repudiandae beatae voluptas sunt consequatur voluptate.", new DateTime(2020, 3, 8, 0, 59, 2, 585, DateTimeKind.Unspecified).AddTicks(1084), "In error quibusdam.", 38, 18 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 144,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 2, 24, 10, 25, 9, 881, DateTimeKind.Unspecified).AddTicks(7114), "Ut commodi excepturi modi voluptate et.", new DateTime(2020, 4, 9, 10, 52, 41, 389, DateTimeKind.Unspecified).AddTicks(5069), "Porro aspernatur.", 37, 2, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 145,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 4, 15, 14, 34, 20, 989, DateTimeKind.Unspecified).AddTicks(78), "Officiis odio itaque voluptatem tenetur dolor corrupti perspiciatis corrupti ipsum.", new DateTime(2020, 9, 15, 15, 54, 59, 663, DateTimeKind.Unspecified).AddTicks(4478), "Provident enim autem adipisci.", 56, 2, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 146,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 6, 30, 23, 37, 18, 727, DateTimeKind.Unspecified).AddTicks(7421), "Pariatur dolores doloribus debitis cupiditate iure ea.", new DateTime(2021, 8, 3, 19, 53, 52, 196, DateTimeKind.Unspecified).AddTicks(5384), "Unde natus ratione.", 40, 14 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 147,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 5, 28, 21, 13, 34, 443, DateTimeKind.Unspecified).AddTicks(4163), "Nesciunt asperiores minima amet voluptatem cum et voluptatem.", new DateTime(2021, 6, 24, 9, 9, 23, 745, DateTimeKind.Unspecified).AddTicks(2774), "Tenetur quia.", 62, 40 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 148,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 3, 1, 15, 37, 41, 962, DateTimeKind.Unspecified).AddTicks(9724), "Nam quod ducimus veritatis et error omnis qui qui officia in omnis sint perspiciatis.", new DateTime(2020, 6, 26, 7, 14, 40, 692, DateTimeKind.Unspecified).AddTicks(7651), "Voluptatibus placeat tempora.", 23, 23, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 149,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 1, 13, 9, 55, 38, 738, DateTimeKind.Unspecified).AddTicks(235), "Molestias a sunt non sint voluptas ut odit quis et fugiat.", new DateTime(2019, 2, 28, 1, 38, 46, 59, DateTimeKind.Unspecified).AddTicks(4254), "Amet ipsum numquam qui necessitatibus.", 71, 4, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 150,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 4, 22, 13, 54, 10, 919, DateTimeKind.Unspecified).AddTicks(6136), "Commodi qui nihil totam voluptatem et dolores cupiditate quisquam ipsum repudiandae inventore qui similique.", new DateTime(2021, 6, 7, 18, 32, 19, 276, DateTimeKind.Unspecified).AddTicks(6931), "Sit nobis iusto nihil sit.", 33, 8, 1 });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2021, 5, 2, 10, 30, 55, 23, DateTimeKind.Unspecified).AddTicks(2906), "Hallie Dooley" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2019, 3, 13, 12, 2, 10, 513, DateTimeKind.Unspecified).AddTicks(1184), "Ola Zboncak" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2020, 11, 19, 8, 54, 0, 203, DateTimeKind.Unspecified).AddTicks(6052), "Durward Mueller" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2018, 6, 11, 11, 7, 3, 261, DateTimeKind.Unspecified).AddTicks(1548), "Xander Paucek" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2019, 12, 7, 19, 53, 3, 488, DateTimeKind.Unspecified).AddTicks(6073), "Joshua O'Reilly" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2020, 11, 19, 0, 44, 25, 992, DateTimeKind.Unspecified).AddTicks(7993), "Efren Schuppe" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2020, 1, 25, 18, 55, 26, 410, DateTimeKind.Unspecified).AddTicks(3395), "Zoe Wehner" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2018, 8, 25, 19, 27, 43, 599, DateTimeKind.Unspecified).AddTicks(4660), "Natalie Turcotte" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2020, 8, 28, 10, 9, 34, 475, DateTimeKind.Unspecified).AddTicks(7315), "Joanny Walsh" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2018, 11, 1, 11, 7, 25, 369, DateTimeKind.Unspecified).AddTicks(2301), "Jessy Mann" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1978, 6, 17, 15, 24, 24, 68, DateTimeKind.Unspecified).AddTicks(629), new DateTime(1992, 12, 6, 1, 24, 38, 456, DateTimeKind.Unspecified).AddTicks(6325), "Jovani52@hotmail.com", "Deangelo", "Wiegand", 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1995, 7, 19, 19, 43, 5, 605, DateTimeKind.Unspecified).AddTicks(949), new DateTime(2007, 2, 9, 23, 39, 23, 798, DateTimeKind.Unspecified).AddTicks(3267), "Libbie87@yahoo.com", "Jazlyn", "Windler", 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2004, 12, 1, 2, 59, 42, 828, DateTimeKind.Unspecified).AddTicks(338), new DateTime(2015, 3, 13, 21, 43, 8, 233, DateTimeKind.Unspecified).AddTicks(4358), "Freida.Leffler@gmail.com", "Raegan", "Altenwerth", 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2010, 6, 24, 7, 12, 25, 211, DateTimeKind.Unspecified).AddTicks(86), new DateTime(2020, 2, 12, 14, 36, 11, 546, DateTimeKind.Unspecified).AddTicks(6948), "Lavon.Rodriguez99@gmail.com", "Kiara", "Satterfield", 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2002, 6, 1, 13, 54, 6, 516, DateTimeKind.Unspecified).AddTicks(3640), new DateTime(2015, 12, 17, 3, 15, 1, 638, DateTimeKind.Unspecified).AddTicks(857), "Jacey28@gmail.com", "Logan", "Okuneva", 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1973, 12, 8, 16, 45, 1, 268, DateTimeKind.Unspecified).AddTicks(1084), new DateTime(2010, 9, 3, 20, 45, 16, 166, DateTimeKind.Unspecified).AddTicks(7148), "Hudson.Feest99@yahoo.com", "Colten", "Gutkowski", 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1999, 4, 30, 16, 0, 26, 111, DateTimeKind.Unspecified).AddTicks(2210), new DateTime(2019, 9, 30, 18, 57, 58, 612, DateTimeKind.Unspecified).AddTicks(2275), "Mckenzie_Ullrich72@gmail.com", "Thurman", "Carroll", 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1970, 8, 23, 8, 27, 22, 244, DateTimeKind.Unspecified).AddTicks(5734), new DateTime(1981, 1, 19, 5, 7, 29, 68, DateTimeKind.Unspecified).AddTicks(2106), "Felicita_Bashirian45@gmail.com", "Madonna", "Heller", 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2011, 6, 20, 15, 24, 48, 859, DateTimeKind.Unspecified).AddTicks(8318), new DateTime(2019, 6, 21, 14, 32, 14, 955, DateTimeKind.Unspecified).AddTicks(2751), "Lenny.Crooks@yahoo.com", "Adrien", "Hilpert", 9 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1988, 3, 19, 5, 53, 58, 63, DateTimeKind.Unspecified).AddTicks(2971), new DateTime(1996, 3, 26, 2, 48, 11, 748, DateTimeKind.Unspecified).AddTicks(3340), "Kameron_Zemlak27@hotmail.com", "Rasheed", "Barrows", 9 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1974, 12, 25, 10, 8, 28, 512, DateTimeKind.Unspecified).AddTicks(1796), new DateTime(2007, 4, 12, 17, 28, 58, 296, DateTimeKind.Unspecified).AddTicks(6237), "Ayla_Gerlach49@yahoo.com", "Chanelle", "Lind", 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1986, 2, 7, 14, 3, 33, 138, DateTimeKind.Unspecified).AddTicks(8076), new DateTime(2013, 4, 7, 14, 19, 7, 154, DateTimeKind.Unspecified).AddTicks(9751), "Roscoe77@gmail.com", "Marjolaine", "Cronin", 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1976, 7, 28, 9, 19, 46, 480, DateTimeKind.Unspecified).AddTicks(8136), new DateTime(1991, 5, 26, 23, 19, 31, 454, DateTimeKind.Unspecified).AddTicks(9110), "Enoch.Kozey16@hotmail.com", "Seamus", "Spinka", 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1998, 1, 2, 20, 34, 16, 20, DateTimeKind.Unspecified).AddTicks(406), new DateTime(2011, 7, 30, 14, 1, 58, 198, DateTimeKind.Unspecified).AddTicks(1991), "Riley_Smith@hotmail.com", "Amya", "Walsh", 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1993, 4, 21, 18, 52, 34, 113, DateTimeKind.Unspecified).AddTicks(2930), new DateTime(2010, 2, 26, 17, 35, 30, 628, DateTimeKind.Unspecified).AddTicks(3624), "Ewald.Lockman@gmail.com", "Sidney", "Dicki", 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1973, 3, 5, 19, 2, 30, 856, DateTimeKind.Unspecified).AddTicks(606), new DateTime(2006, 5, 8, 13, 16, 1, 140, DateTimeKind.Unspecified).AddTicks(1582), "Elisabeth_Powlowski78@gmail.com", "Alysha", "Gutkowski", 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1987, 5, 17, 13, 32, 51, 545, DateTimeKind.Unspecified).AddTicks(6308), new DateTime(2015, 12, 31, 11, 39, 9, 785, DateTimeKind.Unspecified).AddTicks(7851), "Liza1@gmail.com", "Viola", "Kilback", 9 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1986, 8, 31, 11, 54, 23, 795, DateTimeKind.Unspecified).AddTicks(8630), new DateTime(2018, 10, 20, 20, 58, 9, 116, DateTimeKind.Unspecified).AddTicks(4920), "Winnifred_OHara92@gmail.com", "Moshe", "Hayes", 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1971, 7, 3, 6, 57, 10, 683, DateTimeKind.Unspecified).AddTicks(9001), new DateTime(2010, 4, 11, 8, 37, 30, 621, DateTimeKind.Unspecified).AddTicks(3079), "Nicklaus.Schroeder@yahoo.com", "Melvina", "Pfannerstill", 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1977, 9, 23, 8, 30, 57, 951, DateTimeKind.Unspecified).AddTicks(1976), new DateTime(1990, 6, 9, 14, 25, 56, 447, DateTimeKind.Unspecified).AddTicks(8705), "Tia_Rodriguez31@gmail.com", "Keanu", "Rice", 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1972, 10, 7, 22, 51, 53, 394, DateTimeKind.Unspecified).AddTicks(7801), new DateTime(2013, 4, 13, 12, 9, 51, 662, DateTimeKind.Unspecified).AddTicks(4587), "Vida4@gmail.com", "Lynn", "Murray", 9 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1988, 10, 29, 19, 31, 55, 581, DateTimeKind.Unspecified).AddTicks(7561), new DateTime(2014, 9, 9, 0, 44, 4, 191, DateTimeKind.Unspecified).AddTicks(3299), "Derek_Rowe@hotmail.com", "Maverick", "Reilly", 9 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2010, 6, 7, 19, 25, 10, 419, DateTimeKind.Unspecified).AddTicks(1466), new DateTime(2020, 9, 18, 9, 28, 11, 768, DateTimeKind.Unspecified).AddTicks(3998), "Chandler6@hotmail.com", "Fletcher", "Bednar", 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2005, 4, 9, 1, 37, 22, 483, DateTimeKind.Unspecified).AddTicks(9234), new DateTime(2021, 6, 10, 19, 37, 17, 252, DateTimeKind.Unspecified).AddTicks(9336), "Johann24@hotmail.com", "Lewis", "Dooley", 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1999, 11, 1, 5, 48, 49, 356, DateTimeKind.Unspecified).AddTicks(8514), new DateTime(2021, 4, 14, 15, 47, 40, 696, DateTimeKind.Unspecified).AddTicks(2682), "Dewayne40@hotmail.com", "Ava", "Witting", 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1976, 3, 29, 9, 3, 54, 581, DateTimeKind.Unspecified).AddTicks(6362), new DateTime(1997, 7, 16, 6, 8, 9, 577, DateTimeKind.Unspecified).AddTicks(2328), "Derrick_Sanford@yahoo.com", "Rosalia", "Towne", 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1978, 2, 17, 12, 11, 57, 548, DateTimeKind.Unspecified).AddTicks(4878), new DateTime(1992, 12, 31, 16, 20, 38, 956, DateTimeKind.Unspecified).AddTicks(1876), "Kaitlyn_Fisher@gmail.com", "Derek", "Cole", 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1991, 9, 13, 15, 38, 9, 318, DateTimeKind.Unspecified).AddTicks(2479), new DateTime(2006, 7, 16, 7, 20, 59, 92, DateTimeKind.Unspecified).AddTicks(9819), "Jessyca.Satterfield@yahoo.com", "Elmore", "Bode", 6 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2002, 8, 3, 6, 47, 40, 680, DateTimeKind.Unspecified).AddTicks(2952), new DateTime(2015, 9, 13, 10, 59, 38, 381, DateTimeKind.Unspecified).AddTicks(1058), "Electa_Johnson51@hotmail.com", "Pat", "Hudson", 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1970, 12, 17, 14, 53, 33, 599, DateTimeKind.Unspecified).AddTicks(2008), new DateTime(1994, 6, 27, 4, 45, 53, 276, DateTimeKind.Unspecified).AddTicks(2534), "Vladimir.Windler@yahoo.com", "Barton", "Kertzmann", 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1991, 3, 28, 16, 0, 54, 191, DateTimeKind.Unspecified).AddTicks(5754), new DateTime(2007, 12, 15, 21, 4, 29, 166, DateTimeKind.Unspecified).AddTicks(5467), "Bruce_Ruecker@gmail.com", "Dandre", "Kreiger", 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1975, 5, 12, 5, 43, 1, 964, DateTimeKind.Unspecified).AddTicks(9987), new DateTime(2002, 11, 28, 8, 13, 21, 530, DateTimeKind.Unspecified).AddTicks(1636), "Robb2@hotmail.com", "Glen", "Shanahan", 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1993, 8, 12, 5, 30, 21, 2, DateTimeKind.Unspecified).AddTicks(9863), new DateTime(2017, 10, 29, 4, 21, 37, 737, DateTimeKind.Unspecified).AddTicks(7173), "Justyn.Doyle@hotmail.com", "Isaac", "Klein", 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1999, 2, 11, 3, 14, 2, 58, DateTimeKind.Unspecified).AddTicks(1212), new DateTime(2011, 1, 3, 4, 58, 37, 948, DateTimeKind.Unspecified).AddTicks(6482), "Kane_Maggio15@hotmail.com", "Mohamed", "Konopelski", 9 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2003, 8, 16, 12, 5, 44, 745, DateTimeKind.Unspecified).AddTicks(7316), new DateTime(2017, 10, 31, 13, 37, 57, 836, DateTimeKind.Unspecified).AddTicks(2960), "Osborne_Gorczany10@hotmail.com", "Alexzander", "Heaney", 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1987, 8, 26, 2, 22, 12, 778, DateTimeKind.Unspecified).AddTicks(7131), new DateTime(2006, 7, 2, 6, 57, 41, 129, DateTimeKind.Unspecified).AddTicks(7558), "Omer_Rohan11@yahoo.com", "Johann", "Mraz", 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1995, 1, 1, 13, 21, 27, 529, DateTimeKind.Unspecified).AddTicks(5144), new DateTime(2019, 2, 5, 7, 0, 24, 140, DateTimeKind.Unspecified).AddTicks(7703), "Seth95@hotmail.com", "Maeve", "Rodriguez", 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1993, 5, 14, 22, 1, 53, 66, DateTimeKind.Unspecified).AddTicks(6298), new DateTime(2009, 12, 16, 6, 49, 30, 959, DateTimeKind.Unspecified).AddTicks(8334), "Bernhard_Rempel97@yahoo.com", "Deangelo", "Welch", 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName" },
                values: new object[] { new DateTime(1979, 8, 11, 12, 18, 38, 993, DateTimeKind.Unspecified).AddTicks(9187), new DateTime(2005, 10, 18, 19, 16, 42, 816, DateTimeKind.Unspecified).AddTicks(6111), "Ernestina.Watsica90@hotmail.com", "Bella", "Kirlin" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1981, 11, 14, 14, 46, 49, 105, DateTimeKind.Unspecified).AddTicks(3487), new DateTime(1994, 9, 13, 14, 1, 1, 154, DateTimeKind.Unspecified).AddTicks(8589), "Dennis.Stehr@hotmail.com", "Geoffrey", "Hintz", 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 41,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2014, 4, 9, 17, 43, 53, 711, DateTimeKind.Unspecified).AddTicks(2114), new DateTime(2021, 7, 6, 22, 2, 46, 722, DateTimeKind.Unspecified).AddTicks(259), "Maria.Dietrich@gmail.com", "Nils", "Schroeder", 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 42,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1991, 4, 24, 20, 47, 33, 635, DateTimeKind.Unspecified).AddTicks(9287), new DateTime(2016, 10, 17, 7, 1, 44, 55, DateTimeKind.Unspecified).AddTicks(1793), "Kaycee_Gulgowski28@yahoo.com", "Saul", "Zemlak", 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 43,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2009, 12, 15, 7, 52, 56, 793, DateTimeKind.Unspecified).AddTicks(6440), new DateTime(2019, 11, 1, 9, 29, 3, 483, DateTimeKind.Unspecified).AddTicks(8781), "Kelvin.Batz@yahoo.com", "Maddison", "Ward", 6 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 44,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1975, 8, 12, 8, 50, 52, 277, DateTimeKind.Unspecified).AddTicks(161), new DateTime(1988, 12, 19, 18, 57, 8, 42, DateTimeKind.Unspecified).AddTicks(7549), "Ezra_Nienow73@hotmail.com", "Destin", "Goodwin", 10 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 45,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2009, 4, 22, 21, 13, 37, 528, DateTimeKind.Unspecified).AddTicks(9020), new DateTime(2020, 11, 14, 14, 59, 2, 602, DateTimeKind.Unspecified).AddTicks(460), "Gideon95@gmail.com", "Conrad", "Huel", 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 46,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1980, 7, 14, 0, 53, 51, 57, DateTimeKind.Unspecified).AddTicks(2613), new DateTime(2006, 7, 28, 8, 22, 23, 315, DateTimeKind.Unspecified).AddTicks(3972), "Bella.Thompson15@yahoo.com", "Parker", "Hickle", 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 47,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1982, 6, 26, 12, 52, 52, 854, DateTimeKind.Unspecified).AddTicks(9456), new DateTime(2018, 4, 4, 22, 53, 34, 161, DateTimeKind.Unspecified).AddTicks(2015), "Miller65@yahoo.com", "Jaydon", "Muller", 6 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 48,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1981, 12, 24, 2, 28, 30, 906, DateTimeKind.Unspecified).AddTicks(1401), new DateTime(1999, 6, 6, 0, 29, 22, 817, DateTimeKind.Unspecified).AddTicks(6979), "Isidro.Lind@hotmail.com", "Fermin", "Oberbrunner", 6 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 49,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2003, 9, 10, 14, 21, 31, 197, DateTimeKind.Unspecified).AddTicks(8600), new DateTime(2019, 6, 14, 9, 17, 59, 175, DateTimeKind.Unspecified).AddTicks(2151), "Janice.Upton@yahoo.com", "Adrianna", "Parisian", 6 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 50,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1971, 6, 21, 12, 39, 40, 294, DateTimeKind.Unspecified).AddTicks(2601), new DateTime(2018, 2, 18, 5, 31, 5, 583, DateTimeKind.Unspecified).AddTicks(5337), "Rahsaan.Reilly66@yahoo.com", "Kory", "Towne", 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 51,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1993, 10, 1, 2, 11, 23, 330, DateTimeKind.Unspecified).AddTicks(7628), new DateTime(2020, 3, 31, 21, 40, 41, 225, DateTimeKind.Unspecified).AddTicks(7330), "Howard_Raynor34@yahoo.com", "Leatha", "Wilkinson", 9 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 52,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1987, 12, 13, 23, 45, 14, 463, DateTimeKind.Unspecified).AddTicks(8990), new DateTime(2020, 8, 27, 22, 15, 36, 408, DateTimeKind.Unspecified).AddTicks(1925), "Price81@gmail.com", "Trenton", "Barrows", 10 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 53,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2013, 6, 12, 7, 56, 40, 598, DateTimeKind.Unspecified).AddTicks(6124), new DateTime(2021, 6, 23, 6, 6, 57, 905, DateTimeKind.Unspecified).AddTicks(4625), "Jalon_Bechtelar77@hotmail.com", "Jenifer", "Hermann", 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 54,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1994, 10, 7, 23, 4, 33, 868, DateTimeKind.Unspecified).AddTicks(4870), new DateTime(2013, 9, 28, 4, 38, 20, 311, DateTimeKind.Unspecified).AddTicks(6703), "Moises19@yahoo.com", "Gennaro", "Fadel", 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 55,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1980, 12, 22, 22, 44, 44, 376, DateTimeKind.Unspecified).AddTicks(7417), new DateTime(2016, 11, 25, 21, 10, 52, 837, DateTimeKind.Unspecified).AddTicks(4248), "Vanessa_Mann49@hotmail.com", "Desiree", "Moen", 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 56,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2011, 5, 27, 1, 4, 11, 685, DateTimeKind.Unspecified).AddTicks(2922), new DateTime(2020, 8, 6, 23, 32, 4, 695, DateTimeKind.Unspecified).AddTicks(6671), "Layla_Nolan83@gmail.com", "Veda", "Huels", 6 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 57,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1974, 10, 10, 19, 15, 35, 427, DateTimeKind.Unspecified).AddTicks(1138), new DateTime(2006, 10, 12, 17, 36, 57, 78, DateTimeKind.Unspecified).AddTicks(1914), "Quinn52@yahoo.com", "Hillary", "O'Hara", 9 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 58,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName" },
                values: new object[] { new DateTime(1979, 10, 24, 23, 7, 24, 836, DateTimeKind.Unspecified).AddTicks(5232), new DateTime(2014, 11, 22, 23, 13, 36, 744, DateTimeKind.Unspecified).AddTicks(2643), "Neil.Gottlieb@gmail.com", "George", "Kuhlman" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 59,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1974, 4, 14, 0, 54, 25, 896, DateTimeKind.Unspecified).AddTicks(8092), new DateTime(1984, 10, 12, 14, 18, 18, 940, DateTimeKind.Unspecified).AddTicks(5966), "Herta12@yahoo.com", "Monte", "O'Connell", 6 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 60,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2003, 11, 23, 3, 57, 58, 663, DateTimeKind.Unspecified).AddTicks(7698), new DateTime(2012, 6, 5, 16, 36, 27, 806, DateTimeKind.Unspecified).AddTicks(784), "Maximo39@hotmail.com", "Mollie", "O'Kon", 9 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 61,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1986, 4, 27, 3, 18, 43, 717, DateTimeKind.Unspecified).AddTicks(7132), new DateTime(2006, 11, 5, 7, 13, 41, 824, DateTimeKind.Unspecified).AddTicks(3936), "Otilia_Marquardt85@gmail.com", "Viva", "Conn", 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 62,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2013, 6, 22, 15, 26, 58, 67, DateTimeKind.Unspecified).AddTicks(670), new DateTime(2021, 6, 26, 19, 45, 0, 517, DateTimeKind.Unspecified).AddTicks(6088), "Rhett13@gmail.com", "Leonardo", "Cassin", 10 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 63,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1995, 6, 23, 7, 43, 17, 386, DateTimeKind.Unspecified).AddTicks(8168), new DateTime(2007, 10, 10, 5, 59, 7, 396, DateTimeKind.Unspecified).AddTicks(9916), "Blaze_Reinger@gmail.com", "Hollis", "Baumbach", 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 64,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2008, 1, 21, 1, 35, 50, 702, DateTimeKind.Unspecified).AddTicks(7272), new DateTime(2021, 1, 27, 12, 0, 21, 513, DateTimeKind.Unspecified).AddTicks(7004), "Elsa_McLaughlin@hotmail.com", "Mitchell", "Cruickshank", 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 65,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2008, 10, 18, 5, 18, 29, 217, DateTimeKind.Unspecified).AddTicks(4006), new DateTime(2018, 8, 27, 6, 8, 53, 276, DateTimeKind.Unspecified).AddTicks(8827), "Everardo_Carroll@yahoo.com", "Doris", "Renner", 10 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 66,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1999, 6, 26, 6, 51, 10, 234, DateTimeKind.Unspecified).AddTicks(4430), new DateTime(2019, 5, 7, 21, 43, 56, 292, DateTimeKind.Unspecified).AddTicks(4836), "Sophie14@hotmail.com", "Dimitri", "Schaden", 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 67,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2013, 10, 13, 5, 45, 11, 803, DateTimeKind.Unspecified).AddTicks(7308), new DateTime(2021, 7, 27, 23, 14, 8, 289, DateTimeKind.Unspecified).AddTicks(3072), "Ewell.Koch@gmail.com", "Fabiola", "Goyette", 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 68,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1970, 2, 28, 9, 22, 55, 392, DateTimeKind.Unspecified).AddTicks(8883), new DateTime(1986, 9, 9, 18, 7, 10, 182, DateTimeKind.Unspecified).AddTicks(4121), "Vaughn.Padberg75@gmail.com", "Damaris", "Gorczany", 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 69,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName" },
                values: new object[] { new DateTime(2004, 6, 20, 1, 36, 44, 394, DateTimeKind.Unspecified).AddTicks(516), new DateTime(2016, 12, 8, 4, 54, 33, 59, DateTimeKind.Unspecified).AddTicks(3558), "Rossie.Weimann@gmail.com", "Maida", "Daugherty" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 70,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2009, 6, 8, 16, 44, 15, 298, DateTimeKind.Unspecified).AddTicks(7646), new DateTime(2020, 3, 3, 9, 49, 12, 813, DateTimeKind.Unspecified).AddTicks(2912), "Antonina.Ziemann@yahoo.com", "Caroline", "Altenwerth", 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 71,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2007, 1, 7, 6, 46, 6, 326, DateTimeKind.Unspecified).AddTicks(4466), new DateTime(2019, 4, 8, 17, 2, 12, 596, DateTimeKind.Unspecified).AddTicks(1974), "Olga_Zboncak22@gmail.com", "Keon", "Nader", 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 72,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2012, 11, 26, 19, 18, 31, 787, DateTimeKind.Unspecified).AddTicks(9574), new DateTime(2020, 12, 20, 11, 24, 41, 283, DateTimeKind.Unspecified).AddTicks(3378), "Marlen.Brakus51@gmail.com", "Mafalda", "O'Conner", 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 73,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1978, 2, 20, 10, 22, 49, 663, DateTimeKind.Unspecified).AddTicks(5986), new DateTime(2016, 10, 15, 10, 29, 51, 771, DateTimeKind.Unspecified).AddTicks(6142), "Dion_Jacobs10@gmail.com", "Nasir", "Braun", 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 74,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2007, 5, 22, 2, 19, 12, 330, DateTimeKind.Unspecified).AddTicks(6348), new DateTime(2021, 2, 25, 20, 40, 12, 129, DateTimeKind.Unspecified).AddTicks(4188), "Carley.Kozey74@gmail.com", "Roderick", "Lebsack", 9 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 75,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1986, 5, 25, 14, 12, 14, 277, DateTimeKind.Unspecified).AddTicks(7723), new DateTime(2013, 8, 2, 23, 40, 39, 210, DateTimeKind.Unspecified).AddTicks(5927), "Jayde_Howell76@yahoo.com", "Selina", "Kuvalis", 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 76,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName" },
                values: new object[] { new DateTime(1993, 12, 28, 17, 19, 3, 723, DateTimeKind.Unspecified).AddTicks(331), new DateTime(2016, 3, 27, 19, 47, 38, 682, DateTimeKind.Unspecified).AddTicks(8294), "Merl85@hotmail.com", "Flossie", "Toy" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 77,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1983, 9, 4, 6, 11, 36, 256, DateTimeKind.Unspecified).AddTicks(1298), new DateTime(1998, 1, 30, 0, 17, 2, 377, DateTimeKind.Unspecified).AddTicks(3444), "Madaline.Okuneva63@hotmail.com", "Juanita", "Hansen", 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 78,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1987, 2, 9, 2, 11, 53, 950, DateTimeKind.Unspecified).AddTicks(452), new DateTime(2006, 3, 12, 2, 14, 18, 496, DateTimeKind.Unspecified).AddTicks(5186), "Keaton.Ullrich78@gmail.com", "Dixie", "Turner", 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 79,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2009, 12, 20, 11, 33, 22, 679, DateTimeKind.Unspecified).AddTicks(7422), new DateTime(2019, 1, 13, 4, 1, 16, 125, DateTimeKind.Unspecified).AddTicks(5521), "Bridie.Ferry88@gmail.com", "Linda", "McDermott", 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 80,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1975, 9, 16, 8, 34, 42, 210, DateTimeKind.Unspecified).AddTicks(7284), new DateTime(1986, 1, 7, 20, 39, 21, 197, DateTimeKind.Unspecified).AddTicks(798), "Kasandra_Shields30@gmail.com", "Frederique", "Hermann", 5 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Name",
                table: "Teams",
                newName: "TeamName");

            migrationBuilder.RenameColumn(
                name: "Name",
                table: "Tasks",
                newName: "TaskName");

            migrationBuilder.RenameColumn(
                name: "Name",
                table: "Projects",
                newName: "ProjectName");

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 6, new DateTime(2021, 2, 23, 18, 30, 25, 885, DateTimeKind.Unspecified).AddTicks(7221), new DateTime(2021, 10, 24, 11, 0, 26, 71, DateTimeKind.Unspecified).AddTicks(8336), "The beautiful range of Apple Naturalé that has an exciting mix of natural ingredients. With the Goodness of 100% Natural Ingredients", "Intelligent Frozen Table", 6 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 61, new DateTime(2019, 6, 9, 5, 7, 56, 664, DateTimeKind.Unspecified).AddTicks(4873), new DateTime(2020, 1, 17, 4, 37, 44, 347, DateTimeKind.Unspecified).AddTicks(3434), "The Football Is Good For Training And Recreational Purposes", "Sleek Plastic Chair", 5 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 64, new DateTime(2019, 9, 11, 1, 37, 14, 266, DateTimeKind.Unspecified).AddTicks(3726), new DateTime(2019, 9, 27, 15, 57, 11, 759, DateTimeKind.Unspecified).AddTicks(1204), "The Apollotech B340 is an affordable wireless mouse with reliable connectivity, 12 months battery life and modern design", "Practical Granite Cheese", 6 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 46, new DateTime(2019, 12, 20, 3, 39, 32, 117, DateTimeKind.Unspecified).AddTicks(4579), new DateTime(2020, 3, 10, 9, 18, 9, 656, DateTimeKind.Unspecified).AddTicks(8655), "New range of formal shirts are designed keeping you in mind. With fits and styling that will make you stand apart", "Intelligent Granite Car", 7 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 31, new DateTime(2018, 9, 27, 22, 55, 26, 802, DateTimeKind.Unspecified).AddTicks(8148), new DateTime(2021, 5, 5, 20, 33, 54, 669, DateTimeKind.Unspecified).AddTicks(9692), "New ABC 13 9370, 13.3, 5th Gen CoreA5-8250U, 8GB RAM, 256GB SSD, power UHD Graphics, OS 10 Home, OS Office A & J 2016", "Handmade Fresh Bacon", 2 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 48, new DateTime(2018, 12, 11, 15, 48, 9, 662, DateTimeKind.Unspecified).AddTicks(2650), new DateTime(2020, 7, 1, 5, 36, 13, 159, DateTimeKind.Unspecified).AddTicks(8289), "The slim & simple Maple Gaming Keyboard from Dev Byte comes with a sleek body and 7- Color RGB LED Back-lighting for smart functionality", "Rustic Concrete Pants", 2 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 8, new DateTime(2018, 2, 21, 0, 34, 27, 313, DateTimeKind.Unspecified).AddTicks(8228), new DateTime(2020, 6, 20, 6, 8, 53, 809, DateTimeKind.Unspecified).AddTicks(767), "New ABC 13 9370, 13.3, 5th Gen CoreA5-8250U, 8GB RAM, 256GB SSD, power UHD Graphics, OS 10 Home, OS Office A & J 2016", "Handcrafted Rubber Keyboard", 9 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 39, new DateTime(2019, 1, 9, 17, 59, 39, 86, DateTimeKind.Unspecified).AddTicks(9470), new DateTime(2021, 8, 21, 13, 22, 25, 307, DateTimeKind.Unspecified).AddTicks(77), "The Football Is Good For Training And Recreational Purposes", "Sleek Cotton Chicken", 2 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 48, new DateTime(2021, 1, 5, 17, 33, 18, 58, DateTimeKind.Unspecified).AddTicks(9832), new DateTime(2021, 4, 11, 17, 1, 23, 941, DateTimeKind.Unspecified).AddTicks(3810), "The beautiful range of Apple Naturalé that has an exciting mix of natural ingredients. With the Goodness of 100% Natural Ingredients", "Handmade Frozen Pizza", 3 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName" },
                values: new object[] { 17, new DateTime(2019, 7, 1, 16, 3, 48, 12, DateTimeKind.Unspecified).AddTicks(6090), new DateTime(2019, 9, 2, 4, 0, 28, 928, DateTimeKind.Unspecified).AddTicks(5614), "Carbonite web goalkeeper gloves are ergonomically designed to give easy fit", "Incredible Frozen Bike" });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 37, new DateTime(2018, 1, 26, 8, 5, 7, 822, DateTimeKind.Unspecified).AddTicks(2460), new DateTime(2021, 6, 10, 10, 27, 9, 685, DateTimeKind.Unspecified).AddTicks(9529), "Andy shoes are designed to keeping in mind durability as well as trends, the most stylish range of shoes & sandals", "Gorgeous Rubber Car", 4 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 36, new DateTime(2019, 3, 6, 23, 3, 54, 470, DateTimeKind.Unspecified).AddTicks(1787), new DateTime(2019, 3, 9, 18, 55, 12, 369, DateTimeKind.Unspecified).AddTicks(4514), "Andy shoes are designed to keeping in mind durability as well as trends, the most stylish range of shoes & sandals", "Refined Concrete Tuna", 7 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 21, new DateTime(2021, 3, 31, 22, 34, 30, 708, DateTimeKind.Unspecified).AddTicks(2037), new DateTime(2021, 7, 21, 15, 53, 44, 25, DateTimeKind.Unspecified).AddTicks(141), "Boston's most advanced compression wear technology increases muscle oxygenation, stabilizes active muscles", "Unbranded Granite Towels", 4 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 49, new DateTime(2019, 6, 14, 13, 7, 17, 57, DateTimeKind.Unspecified).AddTicks(9538), new DateTime(2020, 4, 11, 15, 29, 37, 783, DateTimeKind.Unspecified).AddTicks(6006), "The automobile layout consists of a front-engine design, with transaxle-type transmissions mounted at the rear of the engine and four wheel drive", "Handmade Fresh Salad", 5 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 8, new DateTime(2019, 7, 17, 12, 12, 0, 542, DateTimeKind.Unspecified).AddTicks(8266), new DateTime(2020, 6, 5, 9, 11, 45, 106, DateTimeKind.Unspecified).AddTicks(2658), "Andy shoes are designed to keeping in mind durability as well as trends, the most stylish range of shoes & sandals", "Tasty Rubber Gloves", 4 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 46, new DateTime(2020, 12, 15, 0, 42, 59, 530, DateTimeKind.Unspecified).AddTicks(6286), new DateTime(2021, 8, 3, 16, 37, 14, 148, DateTimeKind.Unspecified).AddTicks(2442), "The Apollotech B340 is an affordable wireless mouse with reliable connectivity, 12 months battery life and modern design", "Unbranded Frozen Car", 9 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 48, new DateTime(2020, 11, 9, 20, 6, 14, 467, DateTimeKind.Unspecified).AddTicks(5893), new DateTime(2021, 10, 11, 12, 20, 6, 437, DateTimeKind.Unspecified).AddTicks(6815), "Boston's most advanced compression wear technology increases muscle oxygenation, stabilizes active muscles", "Generic Concrete Chicken", 8 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 38, new DateTime(2018, 12, 15, 13, 40, 53, 204, DateTimeKind.Unspecified).AddTicks(4158), new DateTime(2020, 1, 22, 6, 12, 36, 661, DateTimeKind.Unspecified).AddTicks(2124), "Carbonite web goalkeeper gloves are ergonomically designed to give easy fit", "Generic Rubber Ball", 9 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName" },
                values: new object[] { 29, new DateTime(2021, 3, 11, 12, 15, 23, 459, DateTimeKind.Unspecified).AddTicks(5195), new DateTime(2021, 8, 20, 8, 18, 3, 442, DateTimeKind.Unspecified).AddTicks(8290), "New range of formal shirts are designed keeping you in mind. With fits and styling that will make you stand apart", "Licensed Plastic Chair" });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 27, new DateTime(2021, 1, 26, 6, 5, 8, 703, DateTimeKind.Unspecified).AddTicks(9776), new DateTime(2021, 10, 11, 0, 34, 25, 94, DateTimeKind.Unspecified).AddTicks(8216), "The slim & simple Maple Gaming Keyboard from Dev Byte comes with a sleek body and 7- Color RGB LED Back-lighting for smart functionality", "Small Concrete Bacon", 6 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 63, new DateTime(2020, 7, 30, 15, 25, 3, 962, DateTimeKind.Unspecified).AddTicks(1050), new DateTime(2020, 11, 17, 7, 19, 38, 801, DateTimeKind.Unspecified).AddTicks(9255), "The Apollotech B340 is an affordable wireless mouse with reliable connectivity, 12 months battery life and modern design", "Small Concrete Hat", 7 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 3, new DateTime(2021, 3, 3, 0, 18, 5, 919, DateTimeKind.Unspecified).AddTicks(324), new DateTime(2021, 3, 6, 6, 44, 56, 986, DateTimeKind.Unspecified).AddTicks(9431), "The Nagasaki Lander is the trademarked name of several series of Nagasaki sport bikes, that started with the 1984 ABC800J", "Licensed Cotton Gloves", 9 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "ProjectName", "TeamId" },
                values: new object[] { 70, new DateTime(2019, 1, 14, 5, 46, 22, 827, DateTimeKind.Unspecified).AddTicks(1580), new DateTime(2019, 8, 18, 9, 32, 25, 161, DateTimeKind.Unspecified).AddTicks(6523), "Handmade Metal Soap", 1 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 77, new DateTime(2019, 11, 11, 6, 45, 18, 174, DateTimeKind.Unspecified).AddTicks(9945), new DateTime(2020, 1, 4, 10, 40, 3, 469, DateTimeKind.Unspecified).AddTicks(7993), "The Apollotech B340 is an affordable wireless mouse with reliable connectivity, 12 months battery life and modern design", "Incredible Steel Fish", 6 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 28, new DateTime(2020, 1, 18, 21, 21, 37, 522, DateTimeKind.Unspecified).AddTicks(6934), new DateTime(2020, 4, 5, 15, 24, 48, 291, DateTimeKind.Unspecified).AddTicks(9641), "New ABC 13 9370, 13.3, 5th Gen CoreA5-8250U, 8GB RAM, 256GB SSD, power UHD Graphics, OS 10 Home, OS Office A & J 2016", "Handmade Cotton Towels", 10 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 9, new DateTime(2020, 1, 9, 7, 32, 32, 125, DateTimeKind.Unspecified).AddTicks(7975), new DateTime(2020, 5, 12, 12, 19, 18, 806, DateTimeKind.Unspecified).AddTicks(7530), "Andy shoes are designed to keeping in mind durability as well as trends, the most stylish range of shoes & sandals", "Generic Soft Chair", 5 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 77, new DateTime(2021, 5, 2, 8, 13, 43, 139, DateTimeKind.Unspecified).AddTicks(2803), new DateTime(2021, 12, 2, 9, 36, 6, 551, DateTimeKind.Unspecified).AddTicks(9746), "Carbonite web goalkeeper gloves are ergonomically designed to give easy fit", "Incredible Soft Towels", 7 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 78, new DateTime(2018, 12, 9, 5, 54, 48, 698, DateTimeKind.Unspecified).AddTicks(3975), new DateTime(2019, 9, 1, 5, 12, 15, 287, DateTimeKind.Unspecified).AddTicks(6110), "Boston's most advanced compression wear technology increases muscle oxygenation, stabilizes active muscles", "Unbranded Plastic Car", 4 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName" },
                values: new object[] { 22, new DateTime(2020, 4, 23, 15, 51, 27, 450, DateTimeKind.Unspecified).AddTicks(3117), new DateTime(2021, 8, 8, 9, 28, 27, 867, DateTimeKind.Unspecified).AddTicks(9761), "The Football Is Good For Training And Recreational Purposes", "Unbranded Concrete Cheese" });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 79, new DateTime(2018, 5, 12, 7, 13, 53, 251, DateTimeKind.Unspecified).AddTicks(4554), new DateTime(2021, 7, 7, 22, 37, 15, 474, DateTimeKind.Unspecified).AddTicks(7966), "The automobile layout consists of a front-engine design, with transaxle-type transmissions mounted at the rear of the engine and four wheel drive", "Ergonomic Granite Tuna", 4 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 80, new DateTime(2018, 5, 20, 23, 23, 54, 93, DateTimeKind.Unspecified).AddTicks(8453), new DateTime(2018, 9, 17, 16, 39, 31, 465, DateTimeKind.Unspecified).AddTicks(5573), "New ABC 13 9370, 13.3, 5th Gen CoreA5-8250U, 8GB RAM, 256GB SSD, power UHD Graphics, OS 10 Home, OS Office A & J 2016", "Sleek Granite Computer", 9 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 69, new DateTime(2018, 7, 5, 9, 40, 55, 619, DateTimeKind.Unspecified).AddTicks(4604), new DateTime(2019, 4, 27, 18, 20, 3, 96, DateTimeKind.Unspecified).AddTicks(1491), "Ergonomic executive chair upholstered in bonded black leather and PVC padded seat and back for all-day comfort and support", "Sleek Rubber Ball", 9 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 55, new DateTime(2021, 3, 13, 3, 34, 12, 428, DateTimeKind.Unspecified).AddTicks(4074), new DateTime(2021, 8, 24, 12, 26, 47, 224, DateTimeKind.Unspecified).AddTicks(2412), "The beautiful range of Apple Naturalé that has an exciting mix of natural ingredients. With the Goodness of 100% Natural Ingredients", "Sleek Soft Chair", 10 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 79, new DateTime(2018, 1, 15, 19, 42, 47, 757, DateTimeKind.Unspecified).AddTicks(1434), new DateTime(2020, 3, 27, 6, 16, 27, 978, DateTimeKind.Unspecified).AddTicks(8297), "The Nagasaki Lander is the trademarked name of several series of Nagasaki sport bikes, that started with the 1984 ABC800J", "Rustic Soft Towels", 9 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 22, new DateTime(2018, 11, 17, 19, 7, 17, 28, DateTimeKind.Unspecified).AddTicks(7913), new DateTime(2018, 12, 6, 18, 18, 51, 445, DateTimeKind.Unspecified).AddTicks(687), "The Nagasaki Lander is the trademarked name of several series of Nagasaki sport bikes, that started with the 1984 ABC800J", "Intelligent Wooden Bike", 1 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 32, new DateTime(2021, 4, 12, 10, 12, 18, 40, DateTimeKind.Unspecified).AddTicks(4390), new DateTime(2021, 6, 5, 5, 42, 45, 696, DateTimeKind.Unspecified).AddTicks(1629), "Boston's most advanced compression wear technology increases muscle oxygenation, stabilizes active muscles", "Rustic Metal Computer", 7 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 29, new DateTime(2018, 9, 19, 4, 38, 28, 943, DateTimeKind.Unspecified).AddTicks(7946), new DateTime(2020, 4, 11, 5, 36, 30, 784, DateTimeKind.Unspecified).AddTicks(4679), "New ABC 13 9370, 13.3, 5th Gen CoreA5-8250U, 8GB RAM, 256GB SSD, power UHD Graphics, OS 10 Home, OS Office A & J 2016", "Incredible Cotton Towels", 3 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 27, new DateTime(2018, 5, 26, 11, 59, 19, 247, DateTimeKind.Unspecified).AddTicks(2167), new DateTime(2020, 8, 5, 16, 59, 0, 660, DateTimeKind.Unspecified).AddTicks(7071), "New ABC 13 9370, 13.3, 5th Gen CoreA5-8250U, 8GB RAM, 256GB SSD, power UHD Graphics, OS 10 Home, OS Office A & J 2016", "Generic Granite Car", 7 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "Description", "ProjectName", "TeamId" },
                values: new object[] { 51, new DateTime(2021, 5, 25, 16, 15, 20, 363, DateTimeKind.Unspecified).AddTicks(7711), new DateTime(2021, 8, 14, 14, 41, 56, 859, DateTimeKind.Unspecified).AddTicks(6040), "Boston's most advanced compression wear technology increases muscle oxygenation, stabilizes active muscles", "Awesome Metal Bacon", 8 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "AuthorId", "CreatedAt", "Deadline", "ProjectName", "TeamId" },
                values: new object[] { 73, new DateTime(2018, 10, 24, 4, 39, 12, 424, DateTimeKind.Unspecified).AddTicks(6845), new DateTime(2020, 4, 24, 8, 56, 41, 876, DateTimeKind.Unspecified).AddTicks(2046), "Awesome Steel Fish", 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 5, 3, 0, 10, 11, 666, DateTimeKind.Unspecified).AddTicks(8547), "Voluptatibus tenetur placeat illum quibusdam eaque sit.", new DateTime(2019, 6, 28, 14, 31, 6, 641, DateTimeKind.Unspecified).AddTicks(4484), "Quia nesciunt natus exercitationem vel.", 9, 28, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 7, 17, 5, 42, 602, DateTimeKind.Unspecified).AddTicks(4124), "Ratione distinctio ut dolores sit culpa vel ut sunt quam.", new DateTime(2021, 8, 12, 7, 32, 30, 190, DateTimeKind.Unspecified).AddTicks(4992), "Eum labore mollitia et.", 61, 1, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 5, 18, 23, 24, 8, 270, DateTimeKind.Unspecified).AddTicks(5518), "Iure occaecati quia labore voluptatum odio magni.", new DateTime(2019, 7, 24, 5, 37, 34, 568, DateTimeKind.Unspecified).AddTicks(9571), "Qui placeat rerum saepe.", 44, 23 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 8, 15, 5, 10, 18, 584, DateTimeKind.Unspecified).AddTicks(4250), "Corrupti assumenda ipsum odio error laudantium mollitia tempore ipsa asperiores eos aliquid voluptatem.", new DateTime(2021, 10, 5, 3, 35, 24, 411, DateTimeKind.Unspecified).AddTicks(3588), "Ratione sequi.", 22, 1, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2020, 5, 8, 20, 28, 39, 468, DateTimeKind.Unspecified).AddTicks(6190), "Nam et quo quis sint odio dolor id.", new DateTime(2020, 5, 11, 11, 17, 49, 966, DateTimeKind.Unspecified).AddTicks(3991), "Eos accusantium aliquam impedit eos.", 37, 26 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2018, 3, 4, 17, 20, 44, 205, DateTimeKind.Unspecified).AddTicks(2337), "Ullam dolor esse est sunt aut illum pariatur iure perferendis.", new DateTime(2018, 3, 27, 11, 18, 14, 375, DateTimeKind.Unspecified).AddTicks(5777), "Et et est adipisci.", 16, 34, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 8, 16, 15, 41, 14, 77, DateTimeKind.Unspecified).AddTicks(3462), "Placeat excepturi tempore excepturi repellat enim et voluptas eius autem inventore tempora quo totam.", new DateTime(2019, 8, 21, 4, 45, 19, 833, DateTimeKind.Unspecified).AddTicks(6068), "Perspiciatis omnis iure quia omnis.", 9, 10 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 3, 4, 6, 0, 19, 575, DateTimeKind.Unspecified).AddTicks(9853), "Nobis ut porro vel neque voluptatem architecto non laboriosam aperiam et reiciendis culpa perferendis.", new DateTime(2021, 3, 5, 20, 55, 31, 341, DateTimeKind.Unspecified).AddTicks(3821), "Autem officia quibusdam aspernatur.", 38, 22 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 6, 15, 12, 59, 8, 118, DateTimeKind.Unspecified).AddTicks(1685), "Est enim perferendis aut voluptatem sapiente dolorem nesciunt quo sunt deserunt ipsam.", new DateTime(2021, 6, 22, 16, 48, 20, 442, DateTimeKind.Unspecified).AddTicks(7676), "Fugiat consectetur.", 10, 1, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 2, 2, 3, 36, 0, 455, DateTimeKind.Unspecified).AddTicks(3001), "Voluptatem iste qui dolore temporibus voluptas molestiae.", new DateTime(2020, 2, 4, 15, 55, 2, 461, DateTimeKind.Unspecified).AddTicks(2108), "Ullam dolores iusto nisi exercitationem.", 9, 4, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 3, 8, 17, 59, 26, 969, DateTimeKind.Unspecified).AddTicks(3694), "Quia officiis consectetur ea doloribus cumque perspiciatis quaerat vitae maxime ex amet provident optio.", new DateTime(2019, 3, 9, 15, 2, 37, 786, DateTimeKind.Unspecified).AddTicks(340), "Voluptatem ab.", 68, 12 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 12, 1, 6, 56, 39, 76, DateTimeKind.Unspecified).AddTicks(7308), "Est quas qui blanditiis accusamus ea et sapiente recusandae.", new DateTime(2019, 12, 21, 4, 35, 12, 927, DateTimeKind.Unspecified).AddTicks(8307), "Similique sint quis.", 72, 14, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2018, 6, 26, 6, 40, 54, 943, DateTimeKind.Unspecified).AddTicks(7000), "Neque quae ut id omnis quibusdam ut.", new DateTime(2018, 7, 21, 14, 38, 55, 481, DateTimeKind.Unspecified).AddTicks(1667), "Error perferendis.", 47, 31, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 1, 12, 1, 34, 8, 782, DateTimeKind.Unspecified).AddTicks(9635), "Atque debitis dicta reprehenderit est voluptas qui magnam velit quis porro nesciunt.", new DateTime(2020, 2, 7, 13, 42, 48, 312, DateTimeKind.Unspecified).AddTicks(366), "Aliquam et aut.", 35, 4, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 13, 3, 14, 18, 264, DateTimeKind.Unspecified).AddTicks(2047), "Aut incidunt animi expedita aliquid recusandae quasi.", new DateTime(2019, 9, 23, 6, 11, 47, 192, DateTimeKind.Unspecified).AddTicks(7333), "Voluptas esse voluptates hic accusantium.", 55, 3, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2020, 2, 11, 3, 28, 38, 461, DateTimeKind.Unspecified).AddTicks(2611), "Nihil unde animi eaque rerum sit magni ut et.", new DateTime(2020, 4, 20, 5, 56, 54, 397, DateTimeKind.Unspecified).AddTicks(9916), "Maxime odio quia in atque.", 69, 7 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 3, 1, 43, 7, 404, DateTimeKind.Unspecified).AddTicks(4748), "Corporis expedita vel a ut animi non nobis.", new DateTime(2021, 7, 10, 22, 45, 38, 771, DateTimeKind.Unspecified).AddTicks(1346), "Veniam dolores labore quod ut.", 48, 13, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 11, 8, 43, 1, 175, DateTimeKind.Unspecified).AddTicks(3979), "Eius delectus sunt necessitatibus a veniam dolorem eum accusantium laudantium rerum.", new DateTime(2021, 7, 14, 9, 4, 56, 537, DateTimeKind.Unspecified).AddTicks(1321), "Rerum molestias adipisci a.", 10, 39, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 3, 7, 3, 28, 23, 45, DateTimeKind.Unspecified).AddTicks(3388), "Sit illo unde nihil recusandae magnam quaerat omnis.", new DateTime(2019, 3, 8, 22, 41, 37, 158, DateTimeKind.Unspecified).AddTicks(8874), "Temporibus vitae tempore esse animi.", 25, 12, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 3, 11, 10, 34, 38, 733, DateTimeKind.Unspecified).AddTicks(4548), "Quas modi quis nesciunt consequatur voluptas molestias deleniti maxime porro fugit quis.", new DateTime(2021, 3, 23, 13, 49, 45, 765, DateTimeKind.Unspecified).AddTicks(1170), "Assumenda at tempore et.", 67, 9, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 2, 23, 6, 10, 32, 237, DateTimeKind.Unspecified).AddTicks(1891), "Est facilis soluta id ut temporibus iure consequuntur itaque voluptate accusamus est.", new DateTime(2020, 3, 8, 17, 7, 14, 604, DateTimeKind.Unspecified).AddTicks(1345), "Eos incidunt culpa.", 44, 4, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 4, 13, 8, 57, 2, 634, DateTimeKind.Unspecified).AddTicks(7447), "Repellat debitis veritatis dolorum nisi recusandae dignissimos.", new DateTime(2021, 7, 20, 2, 6, 6, 323, DateTimeKind.Unspecified).AddTicks(2192), "Nihil est excepturi veritatis.", 43, 33, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 3, 30, 22, 13, 7, 445, DateTimeKind.Unspecified).AddTicks(8574), "Dolorum omnis nihil esse sunt occaecati.", new DateTime(2020, 4, 4, 21, 20, 8, 205, DateTimeKind.Unspecified).AddTicks(6599), "Voluptatum enim itaque quisquam.", 23, 25, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 4, 2, 6, 28, 58, 355, DateTimeKind.Unspecified).AddTicks(3001), "Voluptas dolorem quae adipisci quos mollitia ullam blanditiis commodi non dicta quam enim.", new DateTime(2020, 5, 8, 5, 1, 54, 562, DateTimeKind.Unspecified).AddTicks(9661), "Aut aliquam et praesentium nobis.", 23, 26, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 4, 4, 23, 8, 52, 941, DateTimeKind.Unspecified).AddTicks(1839), "Assumenda asperiores in sed omnis eum.", new DateTime(2021, 9, 1, 12, 46, 28, 924, DateTimeKind.Unspecified).AddTicks(3910), "Culpa corporis ut.", 34, 20, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 1, 24, 15, 52, 55, 537, DateTimeKind.Unspecified).AddTicks(4778), "Quis odit sed sed sequi explicabo quos sint mollitia.", new DateTime(2020, 3, 4, 3, 28, 2, 928, DateTimeKind.Unspecified).AddTicks(7032), "Iste quae.", 28, 26, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 3, 17, 7, 51, 30, 954, DateTimeKind.Unspecified).AddTicks(9521), "Sint veritatis sed quasi rerum dolorum illum.", new DateTime(2020, 3, 25, 13, 36, 29, 230, DateTimeKind.Unspecified).AddTicks(3127), "Aut omnis ullam dolorem minima.", 51, 26, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 12, 20, 5, 12, 23, 112, DateTimeKind.Unspecified).AddTicks(9955), "Eum et ad voluptatum reiciendis voluptates expedita.", new DateTime(2020, 5, 24, 12, 51, 40, 835, DateTimeKind.Unspecified).AddTicks(708), "Asperiores autem laudantium.", 56, 11, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 5, 21, 23, 21, 26, 705, DateTimeKind.Unspecified).AddTicks(7312), "Culpa quae enim voluptatum tempore necessitatibus.", new DateTime(2021, 6, 13, 23, 8, 4, 837, DateTimeKind.Unspecified).AddTicks(2552), "Quibusdam iure assumenda tempora quis.", 72, 30, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2018, 11, 29, 0, 42, 28, 709, DateTimeKind.Unspecified).AddTicks(3971), "Quaerat alias quisquam itaque sequi aut consequatur est illo non earum asperiores possimus omnis.", new DateTime(2018, 12, 1, 13, 36, 0, 584, DateTimeKind.Unspecified).AddTicks(893), "Id quidem commodi.", 71, 35, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 23, 6, 21, 57, 156, DateTimeKind.Unspecified).AddTicks(7147), "Officia labore fugiat optio assumenda eius numquam est ab et aut.", new DateTime(2020, 1, 23, 2, 16, 21, 908, DateTimeKind.Unspecified).AddTicks(6989), "Fugit et.", 72, 6, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 4, 5, 7, 56, 18, 445, DateTimeKind.Unspecified).AddTicks(4003), "Maxime voluptatem sed fuga expedita est ad facere modi et dolorem in dolores optio.", new DateTime(2021, 6, 3, 12, 46, 13, 8, DateTimeKind.Unspecified).AddTicks(9453), "Ut et quam ratione.", 68, 13, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 4, 12, 23, 5, 30, 225, DateTimeKind.Unspecified).AddTicks(4818), "Adipisci iusto amet et et perferendis blanditiis rerum dolores excepturi dolores ratione.", new DateTime(2019, 5, 21, 0, 51, 35, 975, DateTimeKind.Unspecified).AddTicks(1433), "Necessitatibus dolores debitis harum aspernatur.", 22, 23, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 7, 14, 7, 53, 23, 906, DateTimeKind.Unspecified).AddTicks(8928), "Minus sunt excepturi adipisci rerum in nemo inventore dolore.", new DateTime(2020, 1, 6, 19, 19, 46, 692, DateTimeKind.Unspecified).AddTicks(6823), "Ullam consequatur.", 43, 18, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 10, 2, 16, 51, 47, 708, DateTimeKind.Unspecified).AddTicks(2915), "Sed consequatur est saepe maiores ex molestiae deserunt modi autem.", new DateTime(2019, 12, 29, 16, 35, 46, 945, DateTimeKind.Unspecified).AddTicks(4143), "Similique voluptatem sit vel rerum.", 70, 14, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 5, 7, 11, 30, 37, 663, DateTimeKind.Unspecified).AddTicks(9306), "Magnam quasi molestiae fuga facilis a sunt minus tenetur eveniet corrupti.", new DateTime(2021, 9, 4, 6, 34, 49, 571, DateTimeKind.Unspecified).AddTicks(4677), "Impedit libero odio ratione.", 66, 17, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 5, 5, 1, 8, 16, 502, DateTimeKind.Unspecified).AddTicks(1501), "Accusantium est labore autem omnis architecto aut saepe laborum excepturi.", new DateTime(2020, 6, 3, 20, 51, 23, 54, DateTimeKind.Unspecified).AddTicks(2121), "A modi aut.", 60, 15, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 8, 27, 19, 48, 18, 47, DateTimeKind.Unspecified).AddTicks(7423), "Eveniet voluptas nemo corrupti molestiae aut pariatur ea.", new DateTime(2020, 10, 8, 7, 45, 46, 848, DateTimeKind.Unspecified).AddTicks(3952), "Ut nisi in at vero.", 18, 29, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 11, 23, 7, 31, 9, 315, DateTimeKind.Unspecified).AddTicks(3861), "Mollitia quo nisi iure fugit fuga aut culpa voluptatem inventore aperiam et dolorem.", new DateTime(2020, 1, 3, 15, 22, 42, 931, DateTimeKind.Unspecified).AddTicks(1556), "Est eum sunt.", 32, 2, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 5, 19, 15, 3, 31, 281, DateTimeKind.Unspecified).AddTicks(9636), "Quia et nesciunt non repellendus est.", new DateTime(2021, 6, 4, 12, 14, 54, 844, DateTimeKind.Unspecified).AddTicks(5300), "Et voluptatem officiis.", 42, 13, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 41,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 3, 7, 0, 8, 27, 625, DateTimeKind.Unspecified).AddTicks(1394), "Soluta sint nisi beatae autem consequatur explicabo reprehenderit et excepturi totam libero.", new DateTime(2019, 3, 9, 1, 8, 22, 252, DateTimeKind.Unspecified).AddTicks(1426), "Sit molestiae molestias.", 58, 12 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 42,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 3, 10, 10, 55, 52, 460, DateTimeKind.Unspecified).AddTicks(8575), "Nihil voluptatibus mollitia commodi nemo assumenda officia eum et tempore aut omnis aut.", new DateTime(2019, 4, 13, 12, 27, 29, 15, DateTimeKind.Unspecified).AddTicks(785), "Dolores officia a aspernatur amet.", 62, 32, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 43,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2018, 5, 30, 18, 49, 25, 387, DateTimeKind.Unspecified).AddTicks(9014), "Explicabo tempora dolor provident tempora autem et assumenda qui voluptas corrupti autem vel.", new DateTime(2018, 7, 21, 22, 47, 41, 183, DateTimeKind.Unspecified).AddTicks(5031), "Enim quaerat et.", 75, 31 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 44,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 10, 29, 6, 53, 43, 223, DateTimeKind.Unspecified).AddTicks(5304), "Molestiae perspiciatis exercitationem aut consectetur consequatur repudiandae mollitia ab adipisci et eos non ut.", new DateTime(2020, 5, 15, 1, 37, 2, 908, DateTimeKind.Unspecified).AddTicks(5783), "Nihil dolorum ut rem.", 18, 15 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 45,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2018, 7, 31, 23, 10, 13, 446, DateTimeKind.Unspecified).AddTicks(93), "Et aut nobis vel ut dolorum incidunt dolorem iste.", new DateTime(2018, 9, 2, 11, 36, 36, 572, DateTimeKind.Unspecified).AddTicks(4473), "Voluptatem nihil.", 40, 31, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 46,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 7, 31, 10, 6, 44, 529, DateTimeKind.Unspecified).AddTicks(9510), "Necessitatibus quia dolorum enim deserunt iste labore error animi.", new DateTime(2020, 1, 7, 21, 29, 3, 408, DateTimeKind.Unspecified).AddTicks(6419), "In est.", 49, 5, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 47,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 11, 9, 22, 6, 52, 893, DateTimeKind.Unspecified).AddTicks(1006), "Similique reiciendis qui accusantium rerum eum tempore et soluta perspiciatis minima voluptas dignissimos consequatur.", new DateTime(2020, 1, 7, 3, 39, 59, 710, DateTimeKind.Unspecified).AddTicks(1310), "Odit tempora et molestias.", 31, 18, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 48,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 11, 18, 9, 29, 21, 138, DateTimeKind.Unspecified).AddTicks(6490), "Consequuntur qui incidunt quas dolore aut.", new DateTime(2019, 11, 21, 16, 58, 11, 362, DateTimeKind.Unspecified).AddTicks(9662), "Sunt dolor.", 63, 24, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 49,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2020, 1, 3, 12, 11, 26, 858, DateTimeKind.Unspecified).AddTicks(3507), "Laborum rerum inventore qui reprehenderit aut odio dolor voluptate veniam et non vel.", new DateTime(2020, 1, 16, 11, 54, 29, 909, DateTimeKind.Unspecified).AddTicks(9187), "Repudiandae quas deserunt voluptas non.", 46, 18 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 50,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 5, 15, 20, 14, 7, 395, DateTimeKind.Unspecified).AddTicks(5768), "Ratione aliquam libero delectus sint mollitia tempora laudantium.", new DateTime(2020, 6, 16, 13, 34, 26, 890, DateTimeKind.Unspecified).AddTicks(1219), "Debitis molestiae non aut.", 78, 7, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 51,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 23, 16, 39, 2, 867, DateTimeKind.Unspecified).AddTicks(8264), "Eligendi nostrum quo est eligendi ut.", new DateTime(2019, 9, 23, 18, 29, 56, 760, DateTimeKind.Unspecified).AddTicks(8081), "Veritatis aliquid et.", 50, 3, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 52,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 3, 7, 14, 40, 49, 623, DateTimeKind.Unspecified).AddTicks(4431), "Tempore dolores non voluptatum nam consequatur inventore ut ea dolores totam saepe doloribus.", new DateTime(2021, 7, 6, 9, 6, 53, 654, DateTimeKind.Unspecified).AddTicks(5724), "Blanditiis dicta dolor.", 29, 16, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 53,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 5, 28, 5, 26, 59, 83, DateTimeKind.Unspecified).AddTicks(5471), "Iste repellendus quia pariatur fugiat temporibus officiis fugit sit harum molestiae qui deserunt ea.", new DateTime(2021, 5, 30, 5, 4, 0, 116, DateTimeKind.Unspecified).AddTicks(3391), "Vel odio ut error voluptatem.", 45, 36, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 54,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2018, 11, 27, 6, 2, 59, 860, DateTimeKind.Unspecified).AddTicks(703), "Explicabo magnam ut nisi dolores sunt saepe similique odit voluptatem.", new DateTime(2018, 11, 30, 12, 44, 5, 617, DateTimeKind.Unspecified).AddTicks(4434), "Dolorem delectus nihil molestias quaerat.", 48, 35 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 55,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 9, 19, 19, 20, 35, 91, DateTimeKind.Unspecified).AddTicks(2972), "Hic culpa sunt nostrum consequatur odio quod deserunt commodi cumque officia nemo eligendi dolorum.", new DateTime(2019, 12, 30, 6, 38, 37, 631, DateTimeKind.Unspecified).AddTicks(2436), "Consequatur facere officia numquam eius.", 20, 6 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 56,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 5, 4, 12, 41, 24, 70, DateTimeKind.Unspecified).AddTicks(1733), "Maxime quisquam numquam quis iure nesciunt tempore et ipsa.", new DateTime(2021, 6, 4, 1, 44, 35, 200, DateTimeKind.Unspecified).AddTicks(378), "Exercitationem eos.", 62, 13 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 57,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 7, 13, 19, 22, 30, 295, DateTimeKind.Unspecified).AddTicks(2433), "Provident eius et ut harum ut aliquam.", new DateTime(2019, 8, 16, 4, 20, 2, 795, DateTimeKind.Unspecified).AddTicks(5399), "Error qui eum sint distinctio.", 8, 10, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 58,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 2, 20, 23, 3, 59, 683, DateTimeKind.Unspecified).AddTicks(527), "Sequi aliquid et eligendi dolor perspiciatis alias omnis voluptatem facere veritatis expedita temporibus.", new DateTime(2019, 3, 6, 2, 58, 16, 63, DateTimeKind.Unspecified).AddTicks(4792), "Dolorem sunt.", 19, 32, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 59,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 25, 20, 45, 22, 97, DateTimeKind.Unspecified).AddTicks(5927), "Possimus amet esse est architecto aut voluptatum numquam.", new DateTime(2021, 3, 18, 4, 50, 27, 293, DateTimeKind.Unspecified).AddTicks(6181), "Quas natus.", 25, 8, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 60,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 8, 7, 2, 37, 4, 41, DateTimeKind.Unspecified).AddTicks(5422), "Eius aspernatur et debitis voluptates animi odio quia consequatur aut necessitatibus delectus laborum quasi.", new DateTime(2019, 8, 13, 13, 28, 6, 540, DateTimeKind.Unspecified).AddTicks(7539), "Vitae ut quis.", 22, 10 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 61,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 8, 10, 7, 29, 42, 643, DateTimeKind.Unspecified).AddTicks(4255), "Aut natus fugit eum perferendis quos.", new DateTime(2021, 6, 18, 19, 51, 24, 485, DateTimeKind.Unspecified).AddTicks(1802), "Rem saepe.", 19, 29, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 62,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 2, 18, 17, 49, 26, 83, DateTimeKind.Unspecified).AddTicks(3690), "Illum dolore ut aut deserunt voluptas incidunt laboriosam.", new DateTime(2021, 4, 5, 21, 17, 26, 1, DateTimeKind.Unspecified).AddTicks(446), "Laudantium nam aut.", 67, 9, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 63,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2018, 8, 25, 19, 49, 5, 125, DateTimeKind.Unspecified).AddTicks(5891), "Voluptatem quibusdam numquam molestiae soluta exercitationem sit expedita odit eum.", new DateTime(2019, 2, 16, 11, 4, 41, 685, DateTimeKind.Unspecified).AddTicks(552), "Minima sint voluptas sit.", 78, 32 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 64,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 11, 18, 12, 31, 27, 429, DateTimeKind.Unspecified).AddTicks(6741), "Omnis sit est dolor totam doloremque qui et sed culpa.", new DateTime(2020, 12, 18, 18, 54, 57, 546, DateTimeKind.Unspecified).AddTicks(7518), "Nulla sunt temporibus.", 45, 29, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 65,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 3, 5, 6, 2, 22, 410, DateTimeKind.Unspecified).AddTicks(3261), "Velit libero quia tenetur quam dolor nostrum quia eos labore iusto rerum sed.", new DateTime(2021, 3, 5, 6, 35, 47, 522, DateTimeKind.Unspecified).AddTicks(7255), "Animi qui eius.", 3, 22, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 66,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 3, 15, 27, 17, 357, DateTimeKind.Unspecified).AddTicks(6605), "Dolorem quia inventore voluptatem corrupti at voluptatem aliquid voluptatibus architecto odit quia.", new DateTime(2021, 8, 11, 2, 22, 22, 307, DateTimeKind.Unspecified).AddTicks(8918), "Est rem voluptatem quia cupiditate.", 52, 33, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 67,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 3, 7, 19, 6, 45, 990, DateTimeKind.Unspecified).AddTicks(4285), "Quam voluptates nisi in praesentium similique eos sunt suscipit explicabo unde animi.", new DateTime(2019, 3, 8, 6, 7, 25, 553, DateTimeKind.Unspecified).AddTicks(7985), "Ullam ratione.", 12, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 68,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 3, 18, 6, 50, 11, 728, DateTimeKind.Unspecified).AddTicks(4628), "Praesentium nam dolor sed ad enim voluptates dolorem ipsam qui.", new DateTime(2021, 5, 14, 9, 38, 57, 796, DateTimeKind.Unspecified).AddTicks(5544), "Eum velit mollitia.", 58, 16, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 69,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 3, 11, 6, 44, 42, 430, DateTimeKind.Unspecified).AddTicks(5347), "Voluptatibus natus id ut dolorem incidunt excepturi fuga et sint id.", new DateTime(2020, 1, 22, 12, 49, 38, 361, DateTimeKind.Unspecified).AddTicks(6285), "Doloremque quis neque omnis.", 64, 7, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 70,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 8, 26, 13, 14, 12, 643, DateTimeKind.Unspecified).AddTicks(3680), "Omnis est tempora consequatur consectetur quae sint ducimus nisi explicabo.", new DateTime(2019, 8, 29, 16, 50, 1, 177, DateTimeKind.Unspecified).AddTicks(9673), "Exercitationem officia nam.", 9, 10, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 71,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 3, 3, 2, 15, 10, 288, DateTimeKind.Unspecified).AddTicks(3136), "Qui sed est et rerum maxime.", new DateTime(2021, 3, 5, 14, 5, 3, 899, DateTimeKind.Unspecified).AddTicks(3024), "Nam odit.", 63, 22, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 72,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 10, 12, 7, 29, 910, DateTimeKind.Unspecified).AddTicks(8589), "Quia reiciendis amet asperiores dolor veniam qui corrupti ut.", new DateTime(2021, 8, 1, 6, 15, 59, 224, DateTimeKind.Unspecified).AddTicks(414), "Sunt fugit eos quia fugiat.", 48, 39, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 73,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 6, 8, 9, 25, 23, 826, DateTimeKind.Unspecified).AddTicks(5463), "Sunt qui officia dolorum et vitae deleniti nam.", new DateTime(2019, 7, 28, 10, 23, 44, 569, DateTimeKind.Unspecified).AddTicks(5516), "Omnis ut.", 62, 23, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 74,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 12, 5, 20, 53, 39, 238, DateTimeKind.Unspecified).AddTicks(6286), "At in voluptatem ea vitae voluptatem in.", new DateTime(2019, 12, 10, 16, 11, 53, 267, DateTimeKind.Unspecified).AddTicks(7236), "Ad autem corrupti.", 15, 24, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 75,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2020, 7, 22, 20, 46, 16, 765, DateTimeKind.Unspecified).AddTicks(285), "Iste voluptas aut sint excepturi porro quisquam vel facilis amet nihil ullam dolorem.", new DateTime(2021, 3, 28, 9, 44, 49, 776, DateTimeKind.Unspecified).AddTicks(9468), "Voluptatem nulla eum fugit aut.", 23, 5 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 76,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2018, 12, 23, 17, 55, 43, 579, DateTimeKind.Unspecified).AddTicks(728), "Sit quas eum nostrum et vero.", new DateTime(2018, 12, 24, 21, 31, 12, 665, DateTimeKind.Unspecified).AddTicks(7352), "Id iusto eius quasi.", 75, 38, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 77,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 3, 3, 15, 9, 44, 615, DateTimeKind.Unspecified).AddTicks(6827), "Possimus qui veniam neque laboriosam beatae placeat quia voluptatibus.", new DateTime(2021, 3, 4, 13, 51, 36, 139, DateTimeKind.Unspecified).AddTicks(2310), "Placeat rerum excepturi est ea.", 27, 22, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 78,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 5, 17, 21, 4, 48, 17, DateTimeKind.Unspecified).AddTicks(6672), "Molestiae neque repellendus et unde aperiam qui ipsum.", new DateTime(2021, 6, 5, 2, 32, 46, 445, DateTimeKind.Unspecified).AddTicks(7675), "Consequatur et aut.", 80, 11 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 79,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 3, 7, 8, 22, 2, 982, DateTimeKind.Unspecified).AddTicks(1955), "At neque nemo perspiciatis sit debitis in qui doloribus facilis mollitia perspiciatis quod.", new DateTime(2021, 7, 24, 14, 6, 45, 839, DateTimeKind.Unspecified).AddTicks(9605), "Aspernatur laudantium tempore.", 14, 16, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 80,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 12, 24, 9, 42, 0, 82, DateTimeKind.Unspecified).AddTicks(697), "Quis repellat assumenda molestiae eveniet aliquam officiis qui consectetur est.", new DateTime(2020, 3, 11, 10, 59, 28, 234, DateTimeKind.Unspecified).AddTicks(8730), "Sequi minus consequuntur sint voluptates.", 73, 34, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 81,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2020, 2, 10, 6, 5, 37, 373, DateTimeKind.Unspecified).AddTicks(5456), "Mollitia enim rerum dignissimos molestiae quos occaecati.", new DateTime(2020, 2, 21, 19, 51, 32, 67, DateTimeKind.Unspecified).AddTicks(6772), "Qui quae dolor in quo.", 5, 4 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 82,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 8, 2, 13, 26, 31, 532, DateTimeKind.Unspecified).AddTicks(3200), "Sapiente unde id expedita sit ab nisi veritatis ipsam repudiandae vitae laboriosam.", new DateTime(2019, 11, 12, 8, 27, 15, 305, DateTimeKind.Unspecified).AddTicks(7581), "Debitis harum ullam explicabo iure.", 40, 14, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 83,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 6, 4, 10, 19, 16, 512, DateTimeKind.Unspecified).AddTicks(4175), "Adipisci et eum officiis doloremque quia occaecati hic temporibus at facere distinctio vero et.", new DateTime(2021, 6, 24, 14, 0, 10, 157, DateTimeKind.Unspecified).AddTicks(2626), "Qui sit eum et aut.", 17, 13, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 84,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 25, 0, 48, 29, 630, DateTimeKind.Unspecified).AddTicks(104), "Ad autem voluptatum dolores excepturi rerum.", new DateTime(2019, 9, 26, 4, 22, 10, 262, DateTimeKind.Unspecified).AddTicks(3263), "Rerum enim.", 66, 3, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 85,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 11, 3, 9, 53, 32, 307, DateTimeKind.Unspecified).AddTicks(5794), "Odit reprehenderit dolores unde deleniti accusantium.", new DateTime(2020, 11, 10, 1, 1, 8, 413, DateTimeKind.Unspecified).AddTicks(7124), "Nihil eius totam.", 5, 21, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 86,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2020, 1, 27, 14, 22, 42, 268, DateTimeKind.Unspecified).AddTicks(8130), "Error iste beatae iste architecto aut nemo minus sint vero modi ipsa sequi.", new DateTime(2020, 2, 23, 19, 5, 9, 157, DateTimeKind.Unspecified).AddTicks(3177), "A eos rerum minima.", 9, 25 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 87,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 6, 21, 1, 50, 53, 768, DateTimeKind.Unspecified).AddTicks(8090), "Aliquam aperiam qui nemo eum aperiam corrupti sed in eos nam reprehenderit voluptatum doloremque.", new DateTime(2021, 8, 2, 4, 1, 28, 503, DateTimeKind.Unspecified).AddTicks(3331), "Molestias voluptas qui magnam nostrum.", 14, 16, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 88,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 8, 11, 17, 9, 33, 477, DateTimeKind.Unspecified).AddTicks(4755), "Quidem error omnis quia est sed reiciendis corporis repellendus fuga qui reiciendis est.", new DateTime(2021, 9, 25, 8, 43, 44, 438, DateTimeKind.Unspecified).AddTicks(337), "Et est accusamus.", 41, 17, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 89,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 2, 18, 21, 14, 26, 75, DateTimeKind.Unspecified).AddTicks(2946), "Reprehenderit nobis hic dolorum vitae perferendis.", new DateTime(2019, 10, 15, 5, 14, 1, 982, DateTimeKind.Unspecified).AddTicks(1748), "Totam excepturi est sint.", 54, 18, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 90,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 6, 2, 9, 58, 55, 902, DateTimeKind.Unspecified).AddTicks(8150), "Id at non ea aut reprehenderit fugiat ut perspiciatis.", new DateTime(2019, 9, 29, 21, 46, 53, 500, DateTimeKind.Unspecified).AddTicks(1890), "Ad ipsam voluptas blanditiis neque.", 73, 40, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 91,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 12, 20, 16, 31, 29, 661, DateTimeKind.Unspecified).AddTicks(8876), "Modi est consectetur nostrum illum sit facere ipsam perspiciatis.", new DateTime(2021, 3, 20, 20, 2, 29, 677, DateTimeKind.Unspecified).AddTicks(6238), "Est dolor sunt ipsum.", 13, 17, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 92,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 3, 7, 5, 7, 53, 139, DateTimeKind.Unspecified).AddTicks(4245), "Culpa natus temporibus id rem odio vel possimus laboriosam omnis ipsum qui sed tempore.", new DateTime(2019, 3, 9, 8, 30, 30, 519, DateTimeKind.Unspecified).AddTicks(5118), "Veritatis vero.", 53, 12, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 93,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 1, 2, 14, 13, 30, 4, DateTimeKind.Unspecified).AddTicks(2863), "Dolore accusantium aspernatur non autem culpa fugiat.", new DateTime(2019, 3, 9, 12, 57, 43, 509, DateTimeKind.Unspecified).AddTicks(7118), "Officia dicta quidem sint et.", 39, 32 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 94,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 4, 5, 16, 54, 40, 121, DateTimeKind.Unspecified).AddTicks(2311), "Asperiores qui a aut quasi quo quas perferendis quis ea itaque in.", new DateTime(2021, 9, 28, 9, 16, 20, 103, DateTimeKind.Unspecified).AddTicks(9358), "Placeat repudiandae inventore.", 28, 20 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 95,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 11, 12, 13, 16, 33, 32, DateTimeKind.Unspecified).AddTicks(7623), "Ut deserunt facere quod dolorem tempore tempora.", new DateTime(2021, 11, 12, 16, 13, 52, 489, DateTimeKind.Unspecified).AddTicks(3616), "Aliquid distinctio.", 43, 27, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 96,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 5, 7, 18, 58, 31, 685, DateTimeKind.Unspecified).AddTicks(9437), "Dolor amet deserunt velit quas dolor saepe reprehenderit.", new DateTime(2021, 8, 9, 17, 49, 48, 451, DateTimeKind.Unspecified).AddTicks(1375), "Facilis autem nulla.", 77, 1, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 97,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 3, 1, 0, 11, 6, 546, DateTimeKind.Unspecified).AddTicks(8096), "Quia eos et dolorem minima commodi possimus quos non accusamus.", new DateTime(2020, 3, 1, 20, 13, 45, 815, DateTimeKind.Unspecified).AddTicks(9760), "Natus qui unde voluptatem.", 9, 4, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 98,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 3, 29, 21, 40, 15, 686, DateTimeKind.Unspecified).AddTicks(9703), "Totam velit vero aliquid sequi reiciendis molestiae aspernatur in ex ducimus dolores eos fuga.", new DateTime(2021, 5, 9, 5, 53, 6, 366, DateTimeKind.Unspecified).AddTicks(9869), "Dolorem et.", 2, 33, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 99,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2018, 7, 1, 16, 47, 46, 23, DateTimeKind.Unspecified).AddTicks(8498), "Occaecati molestiae quam voluptate laudantium at perspiciatis qui sit et iste qui ut temporibus.", new DateTime(2018, 7, 22, 9, 40, 19, 478, DateTimeKind.Unspecified).AddTicks(5768), "Fugiat maxime est.", 15, 31, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 100,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 2, 15, 22, 13, 32, 479, DateTimeKind.Unspecified).AddTicks(5750), "Et quasi qui alias quod qui placeat.", new DateTime(2020, 3, 7, 21, 44, 19, 821, DateTimeKind.Unspecified).AddTicks(5493), "Atque id.", 67, 4, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 101,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 7, 9, 15, 56, 36, 819, DateTimeKind.Unspecified).AddTicks(3872), "Est consequatur ab et dolore et soluta nostrum ipsam occaecati fugiat.", new DateTime(2021, 7, 29, 13, 29, 38, 374, DateTimeKind.Unspecified).AddTicks(3370), "Natus occaecati.", 39, 39, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 102,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 1, 10, 15, 2, 19, 316, DateTimeKind.Unspecified).AddTicks(8556), "Magnam culpa sint fugiat quam qui a cupiditate et fugiat harum blanditiis voluptas.", new DateTime(2020, 1, 17, 10, 13, 0, 870, DateTimeKind.Unspecified).AddTicks(6095), "Nihil voluptas suscipit perferendis pariatur.", 32, 18, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 103,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 4, 22, 8, 27, 3, 123, DateTimeKind.Unspecified).AddTicks(5583), "Repellendus provident nesciunt dolorem esse ab occaecati autem.", new DateTime(2021, 4, 23, 11, 19, 20, 511, DateTimeKind.Unspecified).AddTicks(9034), "Nobis eius.", 64, 8 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 104,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 11, 17, 20, 15, 40, 504, DateTimeKind.Unspecified).AddTicks(282), "Quo ea repellat quia magni itaque odit tempora.", new DateTime(2019, 11, 28, 18, 45, 54, 743, DateTimeKind.Unspecified).AddTicks(1895), "Minus ut.", 76, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 105,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 19, 9, 27, 9, 498, DateTimeKind.Unspecified).AddTicks(5793), "Aliquam eaque et consequatur fugiat est perferendis qui ipsam quidem quo et.", new DateTime(2019, 11, 25, 16, 2, 24, 647, DateTimeKind.Unspecified).AddTicks(2122), "Dolorem culpa rerum autem maiores.", 18, 14, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 106,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2018, 7, 10, 17, 48, 19, 526, DateTimeKind.Unspecified).AddTicks(7455), "Itaque quis sed temporibus laborum et.", new DateTime(2018, 9, 7, 17, 19, 38, 254, DateTimeKind.Unspecified).AddTicks(6144), "Et deserunt.", 35, 31, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 107,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 11, 26, 23, 27, 45, 499, DateTimeKind.Unspecified).AddTicks(1186), "Autem atque et cupiditate voluptates velit magni quos fugiat vel provident quo ea voluptas.", new DateTime(2020, 2, 3, 14, 29, 13, 874, DateTimeKind.Unspecified).AddTicks(8661), "Error consequatur et qui.", 54, 6 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 108,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 6, 25, 7, 7, 43, 932, DateTimeKind.Unspecified).AddTicks(2433), "Necessitatibus dolor quis dolores repellendus quia occaecati officiis incidunt qui dolorem tempore non.", new DateTime(2019, 8, 25, 21, 19, 47, 631, DateTimeKind.Unspecified).AddTicks(3603), "Numquam eius voluptate dolore.", 63, 2, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 109,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2018, 7, 28, 16, 55, 13, 59, DateTimeKind.Unspecified).AddTicks(2238), "Quis ex rerum est dolores facere.", new DateTime(2019, 1, 28, 16, 59, 25, 775, DateTimeKind.Unspecified).AddTicks(3694), "Incidunt officiis quasi.", 53, 32, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 110,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2018, 10, 24, 21, 41, 56, 737, DateTimeKind.Unspecified).AddTicks(2755), "Quae est aut sapiente consequuntur fugit aut quisquam dicta quis nihil et sint iusto.", new DateTime(2020, 1, 16, 13, 48, 20, 240, DateTimeKind.Unspecified).AddTicks(8736), "Harum nulla.", 62, 40, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 111,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 8, 15, 22, 38, 18, 34, DateTimeKind.Unspecified).AddTicks(7565), "Iure quia blanditiis sunt sunt repellat aut consequatur sit.", new DateTime(2019, 8, 28, 15, 30, 59, 567, DateTimeKind.Unspecified).AddTicks(8977), "Eos voluptas.", 18, 10, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 112,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2018, 6, 27, 19, 48, 54, 843, DateTimeKind.Unspecified).AddTicks(9157), "Molestiae quia eos porro aspernatur expedita est sed et.", new DateTime(2018, 12, 7, 13, 22, 58, 254, DateTimeKind.Unspecified).AddTicks(4360), "Earum eveniet enim harum vero.", 50, 34, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 113,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2018, 11, 26, 21, 13, 24, 609, DateTimeKind.Unspecified).AddTicks(6829), "Autem in laborum cumque et quibusdam sunt debitis.", new DateTime(2018, 12, 6, 14, 51, 10, 437, DateTimeKind.Unspecified).AddTicks(4694), "Libero ut aut quisquam quia.", 3, 35, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 114,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2019, 7, 8, 16, 10, 29, 178, DateTimeKind.Unspecified).AddTicks(1688), "Aut laudantium iure ducimus provident atque mollitia quasi.", new DateTime(2021, 4, 9, 12, 27, 16, 239, DateTimeKind.Unspecified).AddTicks(2269), "Alias mollitia vero doloribus nisi.", 52, 11 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 115,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 3, 10, 1, 40, 9, 819, DateTimeKind.Unspecified).AddTicks(3955), "Perferendis vero vero iste voluptas repellat sequi aliquid.", new DateTime(2021, 4, 6, 10, 24, 58, 781, DateTimeKind.Unspecified).AddTicks(7770), "Illo est.", 6, 9, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 116,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 7, 24, 3, 12, 22, 19, DateTimeKind.Unspecified).AddTicks(6919), "Perferendis neque ipsum possimus sed sit animi molestias quaerat deserunt laboriosam repudiandae ut ad.", new DateTime(2019, 11, 28, 5, 5, 7, 268, DateTimeKind.Unspecified).AddTicks(4929), "Saepe vel reiciendis voluptatum.", 54, 14, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 117,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2020, 4, 5, 15, 25, 31, 948, DateTimeKind.Unspecified).AddTicks(1439), "Quaerat molestiae ut velit cum sed provident libero.", new DateTime(2020, 4, 8, 16, 59, 54, 59, DateTimeKind.Unspecified).AddTicks(4252), "Totam incidunt aspernatur.", 68, 26 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 118,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 12, 26, 13, 36, 57, 296, DateTimeKind.Unspecified).AddTicks(3767), "Quos saepe possimus aut enim voluptas natus est voluptas omnis eveniet nemo.", new DateTime(2020, 1, 3, 9, 5, 7, 195, DateTimeKind.Unspecified).AddTicks(7586), "Et sed.", 69, 40, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 119,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 10, 23, 2, 50, 26, 903, DateTimeKind.Unspecified).AddTicks(1349), "Est veritatis libero ut possimus similique ut asperiores aperiam eum dolores facere aut.", new DateTime(2020, 4, 3, 2, 7, 50, 322, DateTimeKind.Unspecified).AddTicks(7510), "Ad repellendus ea.", 14, 14, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 120,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId" },
                values: new object[] { new DateTime(2021, 9, 26, 21, 53, 44, 854, DateTimeKind.Unspecified).AddTicks(2726), "Facilis cumque molestiae sit doloribus animi autem facere quia.", new DateTime(2021, 10, 11, 1, 25, 42, 823, DateTimeKind.Unspecified).AddTicks(4440), "Voluptatum fugiat est harum.", 4 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 121,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 1, 24, 18, 11, 56, 603, DateTimeKind.Unspecified).AddTicks(1985), "Qui et ipsum dolore totam ducimus quisquam quam unde aut fugit molestiae repellat.", new DateTime(2020, 1, 8, 2, 46, 57, 56, DateTimeKind.Unspecified).AddTicks(5670), "Asperiores praesentium.", 5, 38, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 122,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 8, 30, 23, 58, 55, 246, DateTimeKind.Unspecified).AddTicks(2973), "Et ipsam dolorem et eius voluptates nesciunt cum sit.", new DateTime(2019, 10, 15, 22, 19, 52, 998, DateTimeKind.Unspecified).AddTicks(5424), "Dolore alias et qui.", 32, 40, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 123,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 2, 3, 2, 20, 59, 683, DateTimeKind.Unspecified).AddTicks(575), "Illo voluptatem quidem qui distinctio modi voluptatem voluptates quam rerum aliquid.", new DateTime(2021, 3, 16, 12, 25, 2, 206, DateTimeKind.Unspecified).AddTicks(7881), "Deleniti exercitationem.", 63, 9, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 124,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "State" },
                values: new object[] { new DateTime(2020, 1, 3, 14, 34, 32, 484, DateTimeKind.Unspecified).AddTicks(4390), "Iusto voluptates perferendis et eos aut modi dolorem neque hic et vel consectetur placeat.", new DateTime(2020, 1, 8, 20, 42, 44, 550, DateTimeKind.Unspecified).AddTicks(8735), "Sequi veniam quasi velit rerum.", 25, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 125,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 10, 26, 8, 48, 14, 937, DateTimeKind.Unspecified).AddTicks(8983), "Maxime tenetur voluptate qui architecto velit tempora.", new DateTime(2019, 12, 9, 18, 31, 28, 205, DateTimeKind.Unspecified).AddTicks(580), "Numquam voluptas aut.", 39, 15, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 126,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 10, 12, 23, 35, 1, 566, DateTimeKind.Unspecified).AddTicks(4587), "Amet quaerat esse et nihil eaque omnis.", new DateTime(2020, 11, 13, 8, 41, 56, 422, DateTimeKind.Unspecified).AddTicks(766), "Ipsam ea quibusdam adipisci.", 13, 21, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 127,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 3, 7, 5, 10, 53, 851, DateTimeKind.Unspecified).AddTicks(3362), "Nihil quidem placeat delectus est veritatis delectus debitis unde unde consequatur et voluptatibus excepturi.", new DateTime(2019, 3, 8, 14, 54, 29, 609, DateTimeKind.Unspecified).AddTicks(7188), "Aut aut officiis neque.", 59, 12, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 128,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 1, 5, 13, 3, 35, 399, DateTimeKind.Unspecified).AddTicks(2641), "Atque quaerat sunt sunt eum est sequi dolores explicabo iste.", new DateTime(2020, 2, 28, 3, 5, 39, 13, DateTimeKind.Unspecified).AddTicks(5035), "Provident consequatur aut sunt eos.", 77, 15, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 129,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2020, 11, 13, 17, 23, 44, 475, DateTimeKind.Unspecified).AddTicks(2354), "Autem vero facilis ut iste voluptatem suscipit autem suscipit eum dolor odit.", new DateTime(2021, 2, 6, 21, 56, 19, 992, DateTimeKind.Unspecified).AddTicks(952), "Quos incidunt dolores quo.", 37, 8 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 130,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 24, 4, 15, 0, 632, DateTimeKind.Unspecified).AddTicks(3921), "Esse id est sint perferendis quis officiis maiores debitis in.", new DateTime(2019, 9, 24, 13, 11, 24, 664, DateTimeKind.Unspecified).AddTicks(117), "Ut rem.", 33, 3, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 131,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 8, 7, 11, 1, 41, 230, DateTimeKind.Unspecified).AddTicks(4207), "Qui consectetur sed ut est dolore et doloremque veritatis voluptatem labore numquam.", new DateTime(2021, 8, 7, 20, 15, 9, 478, DateTimeKind.Unspecified).AddTicks(1161), "Quia dolorem fuga illum quia.", 44, 39, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 132,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 12, 20, 14, 52, 8, 474, DateTimeKind.Unspecified).AddTicks(1600), "Nisi rem corporis autem laboriosam laboriosam laborum expedita ea expedita aut provident quibusdam nam.", new DateTime(2020, 1, 2, 3, 28, 22, 262, DateTimeKind.Unspecified).AddTicks(2888), "Laudantium reprehenderit praesentium aliquam aut.", 42, 24, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 133,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 12, 20, 8, 10, 28, 230, DateTimeKind.Unspecified).AddTicks(5065), "Minima nulla ex minus ullam rem.", new DateTime(2020, 1, 13, 21, 53, 48, 55, DateTimeKind.Unspecified).AddTicks(1835), "Voluptates fuga ratione minima alias.", 76, 4, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 134,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2018, 7, 3, 7, 58, 5, 422, DateTimeKind.Unspecified).AddTicks(9472), "Fugit reprehenderit aliquam debitis dolor quo est et.", new DateTime(2020, 9, 2, 1, 10, 55, 188, DateTimeKind.Unspecified).AddTicks(5200), "Eligendi quisquam et qui at.", 26, 30, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 135,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 2, 14, 15, 24, 9, 275, DateTimeKind.Unspecified).AddTicks(3036), "Voluptas ullam et ea sed dignissimos.", new DateTime(2020, 4, 14, 19, 4, 16, 273, DateTimeKind.Unspecified).AddTicks(7899), "Quis enim.", 70, 26, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 136,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 2, 17, 0, 25, 22, 182, DateTimeKind.Unspecified).AddTicks(2511), "Est pariatur dolorem laborum consequatur ea libero sint.", new DateTime(2020, 3, 5, 18, 13, 27, 396, DateTimeKind.Unspecified).AddTicks(5977), "Temporibus sequi.", 10, 4, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 137,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 5, 23, 11, 11, 52, 969, DateTimeKind.Unspecified).AddTicks(9815), "Distinctio necessitatibus veniam tempora earum in rerum dolorum numquam.", new DateTime(2021, 5, 29, 9, 23, 9, 259, DateTimeKind.Unspecified).AddTicks(2660), "Voluptatibus sint dolores.", 76, 36, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 138,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 9, 2, 20, 11, 49, 286, DateTimeKind.Unspecified).AddTicks(1388), "Vero omnis praesentium expedita aspernatur repellendus ut cum et et.", new DateTime(2020, 10, 30, 17, 6, 44, 605, DateTimeKind.Unspecified).AddTicks(1643), "Porro rerum.", 31, 21, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 139,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2018, 5, 16, 2, 55, 10, 47, DateTimeKind.Unspecified).AddTicks(267), "Officia libero amet amet repellendus commodi nemo eius repellendus expedita in suscipit quas veritatis.", new DateTime(2020, 11, 9, 19, 21, 15, 66, DateTimeKind.Unspecified).AddTicks(6890), "Magni ut quae perspiciatis.", 40, 30, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 140,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 4, 26, 20, 57, 19, 238, DateTimeKind.Unspecified).AddTicks(7830), "Similique iure in id qui accusamus libero in error ut explicabo ad rem.", new DateTime(2021, 6, 4, 22, 59, 48, 564, DateTimeKind.Unspecified).AddTicks(6098), "Voluptatem omnis nostrum odit.", 2, 13, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 141,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 8, 15, 21, 53, 45, 994, DateTimeKind.Unspecified).AddTicks(3374), "Consequuntur voluptatum unde eos facilis vel autem vitae ex numquam reiciendis voluptates.", new DateTime(2021, 9, 25, 22, 36, 48, 83, DateTimeKind.Unspecified).AddTicks(5365), "Fugit quae consequatur nihil facilis.", 75, 20, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 142,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2018, 8, 18, 11, 58, 56, 631, DateTimeKind.Unspecified).AddTicks(2974), "Qui consequatur omnis exercitationem doloribus temporibus.", new DateTime(2018, 9, 16, 17, 43, 57, 739, DateTimeKind.Unspecified).AddTicks(4693), "Autem rerum est provident voluptatum.", 56, 31, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 143,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 2, 13, 6, 51, 58, 784, DateTimeKind.Unspecified).AddTicks(742), "Porro consectetur fugit aspernatur dignissimos quia ut ab non eum.", new DateTime(2021, 2, 13, 13, 30, 0, 322, DateTimeKind.Unspecified).AddTicks(6844), "Sed ut et.", 7, 5 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 144,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2020, 9, 11, 5, 26, 38, 950, DateTimeKind.Unspecified).AddTicks(5062), "Beatae sunt error blanditiis ullam sequi.", new DateTime(2020, 10, 22, 5, 57, 15, 273, DateTimeKind.Unspecified).AddTicks(1678), "Alias rem velit fugiat.", 32, 21, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 145,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2018, 9, 3, 13, 19, 22, 430, DateTimeKind.Unspecified).AddTicks(7729), "Esse autem harum consequuntur quo impedit dicta id cum necessitatibus.", new DateTime(2018, 9, 4, 23, 32, 36, 937, DateTimeKind.Unspecified).AddTicks(3065), "Suscipit et.", 32, 31, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 146,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 7, 8, 5, 58, 4, 954, DateTimeKind.Unspecified).AddTicks(3422), "Unde facilis qui ipsum blanditiis dolor.", new DateTime(2021, 8, 17, 12, 36, 21, 882, DateTimeKind.Unspecified).AddTicks(7383), "Magni dicta ea rerum.", 47, 33 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 147,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2020, 5, 20, 11, 20, 14, 175, DateTimeKind.Unspecified).AddTicks(3061), "Quo quis repellat nisi asperiores corrupti necessitatibus error sed ea et.", new DateTime(2020, 5, 28, 14, 17, 53, 301, DateTimeKind.Unspecified).AddTicks(458), "Qui quia.", 75, 7 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 148,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 9, 20, 14, 48, 34, 13, DateTimeKind.Unspecified).AddTicks(1542), "Eum molestiae ut fugiat sapiente eaque sit libero nemo molestiae assumenda consequatur nulla sunt.", new DateTime(2019, 9, 25, 6, 47, 12, 306, DateTimeKind.Unspecified).AddTicks(3154), "Veniam qui accusamus qui quos.", 7, 3, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 149,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2019, 3, 3, 18, 33, 27, 904, DateTimeKind.Unspecified).AddTicks(913), "Qui et et quibusdam amet nemo saepe ut sit laborum esse qui expedita.", new DateTime(2020, 3, 20, 2, 45, 46, 263, DateTimeKind.Unspecified).AddTicks(2122), "Voluptas eum laborum qui.", 26, 38, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 150,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "TaskName", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 4, 3, 13, 1, 39, 189, DateTimeKind.Unspecified).AddTicks(8642), "Voluptas accusamus quidem repellendus quia est aut consectetur iste.", new DateTime(2021, 5, 29, 0, 26, 6, 843, DateTimeKind.Unspecified).AddTicks(2738), "Tempora nesciunt.", 10, 13, 2 });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "TeamName" },
                values: new object[] { new DateTime(2019, 5, 1, 11, 48, 44, 738, DateTimeKind.Unspecified).AddTicks(843), "Albert Rau" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "TeamName" },
                values: new object[] { new DateTime(2021, 2, 15, 10, 52, 4, 365, DateTimeKind.Unspecified).AddTicks(53), "Kole Crona" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "TeamName" },
                values: new object[] { new DateTime(2021, 2, 1, 23, 48, 2, 178, DateTimeKind.Unspecified).AddTicks(9034), "Filomena Osinski" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "TeamName" },
                values: new object[] { new DateTime(2019, 12, 7, 4, 8, 24, 411, DateTimeKind.Unspecified).AddTicks(2556), "Madelyn Fahey" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "TeamName" },
                values: new object[] { new DateTime(2018, 12, 26, 14, 10, 51, 83, DateTimeKind.Unspecified).AddTicks(6758), "Alice Little" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "TeamName" },
                values: new object[] { new DateTime(2021, 1, 2, 7, 34, 15, 14, DateTimeKind.Unspecified).AddTicks(6453), "Deron Kirlin" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "TeamName" },
                values: new object[] { new DateTime(2020, 9, 30, 6, 7, 6, 772, DateTimeKind.Unspecified).AddTicks(6502), "Duncan Yost" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "TeamName" },
                values: new object[] { new DateTime(2019, 12, 23, 17, 8, 52, 818, DateTimeKind.Unspecified).AddTicks(148), "Rodolfo Waelchi" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "TeamName" },
                values: new object[] { new DateTime(2019, 7, 31, 10, 32, 10, 813, DateTimeKind.Unspecified).AddTicks(9008), "Ryder Rohan" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "TeamName" },
                values: new object[] { new DateTime(2020, 7, 16, 11, 25, 7, 332, DateTimeKind.Unspecified).AddTicks(8253), "Deonte Abbott" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1991, 8, 13, 8, 12, 42, 747, DateTimeKind.Unspecified).AddTicks(137), new DateTime(2013, 12, 2, 18, 54, 50, 471, DateTimeKind.Unspecified).AddTicks(7790), "Shanel_Hickle1@yahoo.com", "Julio", "Rath", 9 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2001, 3, 13, 21, 11, 24, 781, DateTimeKind.Unspecified).AddTicks(4320), new DateTime(2010, 5, 19, 11, 54, 23, 638, DateTimeKind.Unspecified).AddTicks(2063), "August7@yahoo.com", "Oswald", "Nienow", 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1988, 2, 18, 8, 37, 47, 716, DateTimeKind.Unspecified).AddTicks(8510), new DateTime(2007, 8, 11, 12, 2, 45, 290, DateTimeKind.Unspecified).AddTicks(2298), "Robin_Denesik72@hotmail.com", "Felix", "Durgan", 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1984, 3, 29, 17, 51, 10, 891, DateTimeKind.Unspecified).AddTicks(8267), new DateTime(2005, 12, 21, 16, 54, 43, 908, DateTimeKind.Unspecified).AddTicks(384), "Johnson17@gmail.com", "Marcelo", "Cronin", 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1990, 10, 14, 7, 37, 30, 597, DateTimeKind.Unspecified).AddTicks(630), new DateTime(2009, 5, 24, 21, 16, 37, 801, DateTimeKind.Unspecified).AddTicks(9027), "Alan.Dooley@gmail.com", "Gladys", "Muller", 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1977, 10, 4, 5, 24, 13, 643, DateTimeKind.Unspecified).AddTicks(4315), new DateTime(2020, 8, 16, 19, 8, 44, 882, DateTimeKind.Unspecified).AddTicks(6795), "Charlene.Labadie17@yahoo.com", "Kavon", "Turner", 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1978, 7, 27, 15, 44, 42, 397, DateTimeKind.Unspecified).AddTicks(6625), new DateTime(1990, 12, 5, 4, 50, 15, 525, DateTimeKind.Unspecified).AddTicks(4601), "Berta_Kreiger@hotmail.com", "Aric", "Douglas", 9 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2013, 11, 7, 12, 9, 36, 248, DateTimeKind.Unspecified).AddTicks(5258), new DateTime(2021, 8, 16, 15, 10, 21, 540, DateTimeKind.Unspecified).AddTicks(6172), "Vesta_McCullough@hotmail.com", "Geoffrey", "Murazik", 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1993, 9, 4, 0, 22, 28, 287, DateTimeKind.Unspecified).AddTicks(6188), new DateTime(2003, 12, 2, 18, 24, 20, 675, DateTimeKind.Unspecified).AddTicks(2886), "Kasey_Runolfsdottir0@gmail.com", "Deon", "Weimann", 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2002, 3, 14, 9, 50, 50, 199, DateTimeKind.Unspecified).AddTicks(4020), new DateTime(2011, 3, 27, 12, 38, 1, 431, DateTimeKind.Unspecified).AddTicks(8235), "Emely_Kassulke@gmail.com", "Buster", "Kunde", 10 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2011, 1, 12, 9, 8, 21, 601, DateTimeKind.Unspecified).AddTicks(292), new DateTime(2019, 5, 26, 20, 37, 20, 121, DateTimeKind.Unspecified).AddTicks(9393), "Celia.Gottlieb@yahoo.com", "Dorothy", "Heaney", 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1985, 6, 5, 17, 18, 50, 324, DateTimeKind.Unspecified).AddTicks(6940), new DateTime(2014, 7, 23, 16, 17, 26, 515, DateTimeKind.Unspecified).AddTicks(151), "Alicia15@yahoo.com", "Clay", "Senger", 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1998, 9, 6, 19, 12, 43, 456, DateTimeKind.Unspecified).AddTicks(6502), new DateTime(2009, 1, 13, 2, 33, 50, 422, DateTimeKind.Unspecified).AddTicks(6664), "Sofia72@gmail.com", "Kiara", "Olson", 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1986, 9, 12, 15, 25, 12, 515, DateTimeKind.Unspecified).AddTicks(950), new DateTime(2007, 10, 5, 13, 40, 3, 686, DateTimeKind.Unspecified).AddTicks(1488), "Heidi_Wolff@gmail.com", "Stuart", "Gibson", 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1974, 10, 13, 13, 22, 22, 418, DateTimeKind.Unspecified).AddTicks(6804), new DateTime(1989, 8, 17, 7, 2, 55, 982, DateTimeKind.Unspecified).AddTicks(747), "Shanny84@yahoo.com", "Mozell", "Bogan", 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1996, 7, 18, 13, 32, 45, 480, DateTimeKind.Unspecified).AddTicks(2034), new DateTime(2020, 6, 6, 19, 18, 31, 524, DateTimeKind.Unspecified).AddTicks(1462), "Maryam.Hartmann@hotmail.com", "Emilio", "Maggio", 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1972, 8, 24, 7, 27, 45, 537, DateTimeKind.Unspecified).AddTicks(3078), new DateTime(2002, 9, 14, 13, 21, 16, 638, DateTimeKind.Unspecified).AddTicks(4287), "Bennie_Hansen22@hotmail.com", "Rosalind", "Marquardt", 6 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1973, 10, 24, 23, 41, 29, 816, DateTimeKind.Unspecified).AddTicks(6123), new DateTime(2018, 1, 27, 0, 23, 41, 822, DateTimeKind.Unspecified).AddTicks(4477), "Marcelina12@yahoo.com", "Oswaldo", "Stokes", 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1973, 12, 10, 20, 0, 5, 122, DateTimeKind.Unspecified).AddTicks(9955), new DateTime(1996, 5, 17, 12, 29, 40, 249, DateTimeKind.Unspecified).AddTicks(1614), "Mittie56@hotmail.com", "Levi", "Koss", 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1994, 9, 10, 3, 37, 6, 378, DateTimeKind.Unspecified).AddTicks(7794), new DateTime(2019, 3, 15, 14, 43, 32, 497, DateTimeKind.Unspecified).AddTicks(7296), "Elwyn27@yahoo.com", "Wilford", "Kessler", 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2007, 8, 2, 0, 19, 50, 548, DateTimeKind.Unspecified).AddTicks(7276), new DateTime(2017, 7, 29, 6, 24, 12, 458, DateTimeKind.Unspecified).AddTicks(3742), "Monserrat.Leuschke2@yahoo.com", "Lester", "Beahan", 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1998, 6, 24, 8, 8, 56, 557, DateTimeKind.Unspecified).AddTicks(2112), new DateTime(2021, 6, 14, 10, 24, 55, 430, DateTimeKind.Unspecified).AddTicks(6362), "Dante.Braun98@hotmail.com", "Johnathon", "Bailey", 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1977, 5, 10, 11, 41, 23, 200, DateTimeKind.Unspecified).AddTicks(454), new DateTime(1985, 5, 24, 11, 25, 50, 217, DateTimeKind.Unspecified).AddTicks(9647), "Everette50@yahoo.com", "Maud", "Goldner", 6 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1998, 5, 11, 12, 57, 18, 226, DateTimeKind.Unspecified).AddTicks(7551), new DateTime(2020, 3, 25, 14, 19, 51, 523, DateTimeKind.Unspecified).AddTicks(4175), "Kenyatta18@hotmail.com", "Cordell", "Crona", 6 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1972, 6, 28, 4, 59, 48, 482, DateTimeKind.Unspecified).AddTicks(8999), new DateTime(1998, 2, 1, 4, 26, 57, 665, DateTimeKind.Unspecified).AddTicks(8888), "Jammie.Johns@gmail.com", "Dee", "Barrows", 9 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1978, 7, 23, 3, 41, 12, 158, DateTimeKind.Unspecified).AddTicks(4256), new DateTime(1993, 1, 30, 19, 22, 0, 873, DateTimeKind.Unspecified).AddTicks(4813), "Jared.Hermann@hotmail.com", "Keon", "Ferry", 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1982, 3, 10, 16, 52, 19, 269, DateTimeKind.Unspecified).AddTicks(7084), new DateTime(2013, 9, 21, 3, 32, 37, 444, DateTimeKind.Unspecified).AddTicks(6925), "Lisette.Gutmann66@yahoo.com", "Jordan", "Parisian", 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2011, 5, 1, 5, 50, 6, 989, DateTimeKind.Unspecified).AddTicks(2348), new DateTime(2019, 5, 25, 3, 33, 39, 706, DateTimeKind.Unspecified).AddTicks(1409), "Sophie11@yahoo.com", "Favian", "Reilly", 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1979, 7, 18, 5, 16, 56, 708, DateTimeKind.Unspecified).AddTicks(3228), new DateTime(2003, 5, 22, 22, 17, 18, 539, DateTimeKind.Unspecified).AddTicks(7703), "Ada.Wisozk@gmail.com", "Ivory", "Rippin", 10 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1994, 6, 5, 10, 40, 37, 192, DateTimeKind.Unspecified).AddTicks(9619), new DateTime(2008, 4, 5, 17, 2, 54, 118, DateTimeKind.Unspecified).AddTicks(5424), "Alexandro.Mann16@hotmail.com", "Zella", "Klein", 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2000, 8, 1, 11, 8, 18, 344, DateTimeKind.Unspecified).AddTicks(1610), new DateTime(2010, 8, 14, 20, 38, 15, 127, DateTimeKind.Unspecified).AddTicks(5870), "Daija57@hotmail.com", "Oswaldo", "Pagac", 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1984, 10, 19, 9, 49, 51, 939, DateTimeKind.Unspecified).AddTicks(5562), new DateTime(2006, 4, 6, 5, 31, 56, 711, DateTimeKind.Unspecified).AddTicks(6291), "Angie62@hotmail.com", "Diana", "Crona", 10 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2013, 4, 20, 13, 2, 52, 515, DateTimeKind.Unspecified).AddTicks(9518), new DateTime(2021, 6, 15, 20, 57, 2, 854, DateTimeKind.Unspecified).AddTicks(6173), "Clotilde.Tromp33@hotmail.com", "Isabell", "Schoen", 10 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1990, 4, 23, 9, 21, 50, 578, DateTimeKind.Unspecified).AddTicks(2967), new DateTime(2000, 5, 1, 4, 4, 46, 230, DateTimeKind.Unspecified).AddTicks(1207), "Marcellus_Beer@hotmail.com", "Gennaro", "Reilly", 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1990, 9, 4, 14, 36, 56, 557, DateTimeKind.Unspecified).AddTicks(7632), new DateTime(2017, 5, 14, 1, 27, 31, 846, DateTimeKind.Unspecified).AddTicks(8628), "Moshe_Greenholt62@hotmail.com", "Julie", "Hagenes", 9 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1997, 5, 8, 8, 52, 36, 989, DateTimeKind.Unspecified).AddTicks(6351), new DateTime(2019, 10, 8, 7, 20, 2, 208, DateTimeKind.Unspecified).AddTicks(8106), "Wellington.Waelchi@hotmail.com", "Arlie", "Hansen", 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2009, 5, 9, 5, 7, 7, 630, DateTimeKind.Unspecified).AddTicks(2138), new DateTime(2020, 10, 6, 0, 7, 23, 149, DateTimeKind.Unspecified).AddTicks(978), "Shawn39@gmail.com", "Micah", "Hudson", 9 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2013, 1, 12, 20, 29, 4, 49, DateTimeKind.Unspecified).AddTicks(7470), new DateTime(2021, 3, 30, 10, 15, 40, 110, DateTimeKind.Unspecified).AddTicks(277), "Tess.Kuphal60@yahoo.com", "Selena", "Homenick", 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName" },
                values: new object[] { new DateTime(2010, 4, 3, 11, 33, 4, 934, DateTimeKind.Unspecified).AddTicks(4396), new DateTime(2019, 10, 8, 9, 22, 46, 187, DateTimeKind.Unspecified).AddTicks(8573), "Elinore_Jacobi13@gmail.com", "Joey", "White" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1995, 9, 1, 23, 16, 19, 886, DateTimeKind.Unspecified).AddTicks(5007), new DateTime(2016, 10, 26, 23, 15, 33, 887, DateTimeKind.Unspecified).AddTicks(6059), "Tess_Terry81@hotmail.com", "Cleo", "Hoeger", 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 41,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1971, 8, 13, 22, 11, 45, 108, DateTimeKind.Unspecified).AddTicks(8975), new DateTime(2009, 8, 9, 19, 48, 9, 438, DateTimeKind.Unspecified).AddTicks(7005), "Walker.Beer@hotmail.com", "Winnifred", "Will", 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 42,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1984, 3, 9, 14, 39, 32, 270, DateTimeKind.Unspecified).AddTicks(4500), new DateTime(2012, 8, 27, 22, 24, 14, 766, DateTimeKind.Unspecified).AddTicks(2265), "Ila.Wisozk@gmail.com", "Irwin", "Lesch", 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 43,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2010, 12, 7, 10, 38, 8, 201, DateTimeKind.Unspecified).AddTicks(2698), new DateTime(2020, 7, 8, 14, 53, 49, 940, DateTimeKind.Unspecified).AddTicks(711), "Cicero57@gmail.com", "Terrance", "Hilll", 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 44,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1989, 1, 13, 12, 28, 20, 533, DateTimeKind.Unspecified).AddTicks(9686), new DateTime(2015, 2, 14, 20, 29, 43, 598, DateTimeKind.Unspecified).AddTicks(8705), "Samir60@hotmail.com", "Camron", "Strosin", 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 45,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2010, 5, 16, 19, 42, 18, 976, DateTimeKind.Unspecified).AddTicks(2954), new DateTime(2019, 3, 15, 14, 30, 31, 764, DateTimeKind.Unspecified).AddTicks(161), "Clementine_Rohan47@hotmail.com", "Adrain", "Stroman", 9 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 46,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2012, 5, 25, 12, 35, 34, 504, DateTimeKind.Unspecified).AddTicks(7574), new DateTime(2021, 5, 22, 12, 15, 55, 572, DateTimeKind.Unspecified).AddTicks(4849), "Mohammad_Mosciski99@hotmail.com", "Jerrod", "Thiel", 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 47,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2006, 1, 3, 20, 4, 12, 310, DateTimeKind.Unspecified).AddTicks(8210), new DateTime(2021, 5, 14, 8, 31, 32, 940, DateTimeKind.Unspecified).AddTicks(15), "Kieran.MacGyver@yahoo.com", "Heather", "Wiza", 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 48,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1992, 3, 13, 7, 22, 36, 549, DateTimeKind.Unspecified).AddTicks(6856), new DateTime(2002, 5, 14, 9, 17, 40, 805, DateTimeKind.Unspecified).AddTicks(7535), "Amani.Mosciski13@hotmail.com", "Korbin", "Kshlerin", 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 49,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1980, 8, 3, 7, 26, 41, 477, DateTimeKind.Unspecified).AddTicks(6256), new DateTime(1991, 8, 2, 3, 47, 34, 859, DateTimeKind.Unspecified).AddTicks(8406), "Zachary34@yahoo.com", "Felton", "Von", 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 50,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1981, 11, 20, 13, 30, 39, 764, DateTimeKind.Unspecified).AddTicks(6814), new DateTime(2021, 6, 28, 8, 5, 15, 834, DateTimeKind.Unspecified).AddTicks(2180), "Ismael.Sanford28@hotmail.com", "Pamela", "Hammes", 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 51,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1973, 4, 6, 5, 0, 38, 978, DateTimeKind.Unspecified).AddTicks(7949), new DateTime(2015, 12, 9, 18, 28, 50, 181, DateTimeKind.Unspecified).AddTicks(4179), "Granville_Gerlach8@hotmail.com", "Easton", "Ernser", 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 52,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1978, 11, 20, 5, 10, 28, 296, DateTimeKind.Unspecified).AddTicks(719), new DateTime(1991, 6, 11, 22, 46, 41, 531, DateTimeKind.Unspecified).AddTicks(4687), "Anahi_Spencer@gmail.com", "Pete", "Dicki", 6 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 53,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1976, 4, 29, 17, 16, 29, 486, DateTimeKind.Unspecified).AddTicks(2613), new DateTime(1994, 1, 28, 20, 6, 42, 799, DateTimeKind.Unspecified).AddTicks(3733), "Carmen_Walker@hotmail.com", "Sigrid", "D'Amore", 9 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 54,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1990, 8, 28, 15, 58, 14, 647, DateTimeKind.Unspecified).AddTicks(2333), new DateTime(2006, 7, 29, 1, 50, 0, 0, DateTimeKind.Unspecified).AddTicks(1897), "Ansel_Powlowski72@hotmail.com", "Dario", "Brakus", 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 55,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2008, 2, 15, 3, 40, 1, 898, DateTimeKind.Unspecified).AddTicks(7318), new DateTime(2019, 4, 25, 22, 37, 22, 156, DateTimeKind.Unspecified).AddTicks(1373), "Judy_Morissette@gmail.com", "Ahmed", "Marquardt", 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 56,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1984, 12, 1, 14, 2, 26, 174, DateTimeKind.Unspecified).AddTicks(2114), new DateTime(1995, 2, 5, 6, 19, 47, 888, DateTimeKind.Unspecified).AddTicks(1648), "Giuseppe23@yahoo.com", "Annamae", "Stroman", 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 57,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1986, 1, 26, 18, 11, 27, 481, DateTimeKind.Unspecified).AddTicks(8301), new DateTime(1996, 12, 3, 14, 42, 59, 786, DateTimeKind.Unspecified).AddTicks(1754), "Dameon99@gmail.com", "Trey", "Schroeder", 10 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 58,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName" },
                values: new object[] { new DateTime(1998, 3, 13, 19, 26, 13, 664, DateTimeKind.Unspecified).AddTicks(1259), new DateTime(2010, 11, 24, 12, 19, 47, 597, DateTimeKind.Unspecified).AddTicks(123), "Harvey.Swift@hotmail.com", "Ryann", "Rice" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 59,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2000, 2, 19, 15, 35, 27, 122, DateTimeKind.Unspecified).AddTicks(862), new DateTime(2016, 10, 15, 7, 44, 29, 478, DateTimeKind.Unspecified).AddTicks(8130), "Calista38@hotmail.com", "Blake", "Grant", 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 60,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1974, 11, 29, 14, 34, 12, 825, DateTimeKind.Unspecified).AddTicks(7087), new DateTime(2015, 1, 9, 13, 0, 22, 301, DateTimeKind.Unspecified).AddTicks(7007), "Cicero56@yahoo.com", "Filiberto", "Howell", 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 61,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1974, 12, 26, 7, 4, 44, 19, DateTimeKind.Unspecified).AddTicks(3181), new DateTime(2002, 12, 21, 14, 53, 59, 49, DateTimeKind.Unspecified).AddTicks(5296), "Lelah8@hotmail.com", "Gerardo", "Hessel", 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 62,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2008, 5, 26, 0, 6, 4, 448, DateTimeKind.Unspecified).AddTicks(7200), new DateTime(2016, 9, 11, 3, 39, 35, 382, DateTimeKind.Unspecified).AddTicks(6863), "Ryann.Batz@gmail.com", "Malcolm", "Moen", 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 63,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1996, 8, 12, 10, 16, 32, 808, DateTimeKind.Unspecified).AddTicks(3202), new DateTime(2007, 12, 16, 6, 6, 47, 305, DateTimeKind.Unspecified).AddTicks(1643), "Jennifer.Nader@hotmail.com", "Sierra", "Hammes", 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 64,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1983, 8, 17, 8, 37, 27, 344, DateTimeKind.Unspecified).AddTicks(5152), new DateTime(2016, 6, 28, 11, 46, 42, 720, DateTimeKind.Unspecified).AddTicks(5608), "Vivianne_Skiles6@hotmail.com", "Aliyah", "Heller", 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 65,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1976, 7, 20, 10, 7, 45, 883, DateTimeKind.Unspecified).AddTicks(1456), new DateTime(2005, 5, 10, 11, 24, 28, 912, DateTimeKind.Unspecified).AddTicks(5445), "Ursula_Rogahn@gmail.com", "Dorthy", "Crona", 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 66,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2013, 3, 18, 11, 56, 2, 329, DateTimeKind.Unspecified).AddTicks(2892), new DateTime(2021, 4, 27, 0, 20, 34, 797, DateTimeKind.Unspecified).AddTicks(9382), "Isidro.Bergnaum@yahoo.com", "Max", "Bechtelar", 10 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 67,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1982, 6, 20, 13, 35, 52, 857, DateTimeKind.Unspecified).AddTicks(418), new DateTime(1994, 3, 24, 2, 51, 31, 861, DateTimeKind.Unspecified).AddTicks(5266), "Gwen.Haley2@hotmail.com", "Jazmin", "Volkman", 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 68,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2009, 6, 1, 0, 9, 36, 231, DateTimeKind.Unspecified).AddTicks(4974), new DateTime(2017, 11, 15, 19, 31, 54, 485, DateTimeKind.Unspecified).AddTicks(5226), "Lester_Crooks@gmail.com", "Kaylie", "Fahey", 9 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 69,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName" },
                values: new object[] { new DateTime(2004, 3, 25, 17, 51, 50, 654, DateTimeKind.Unspecified).AddTicks(9516), new DateTime(2013, 4, 15, 4, 29, 12, 539, DateTimeKind.Unspecified).AddTicks(4379), "Evelyn_Gulgowski@yahoo.com", "Itzel", "Nicolas" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 70,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1998, 4, 14, 2, 57, 1, 465, DateTimeKind.Unspecified).AddTicks(1038), new DateTime(2020, 8, 25, 16, 34, 18, 337, DateTimeKind.Unspecified).AddTicks(9815), "Manuel79@hotmail.com", "Augusta", "Weber", 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 71,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2010, 2, 18, 18, 55, 44, 533, DateTimeKind.Unspecified).AddTicks(4546), new DateTime(2018, 7, 31, 7, 19, 33, 594, DateTimeKind.Unspecified).AddTicks(9385), "Wyatt.Connelly83@yahoo.com", "Joanne", "Stroman", 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 72,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1970, 9, 19, 10, 10, 52, 808, DateTimeKind.Unspecified).AddTicks(6684), new DateTime(1994, 10, 19, 18, 24, 25, 559, DateTimeKind.Unspecified).AddTicks(9816), "Alexis6@gmail.com", "Arvid", "Gerlach", 6 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 73,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1981, 3, 31, 22, 6, 45, 57, DateTimeKind.Unspecified).AddTicks(6933), new DateTime(2016, 11, 13, 14, 26, 51, 530, DateTimeKind.Unspecified).AddTicks(4538), "Winnifred_Mueller@hotmail.com", "Derrick", "Cummerata", 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 74,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1978, 12, 25, 9, 46, 10, 360, DateTimeKind.Unspecified).AddTicks(704), new DateTime(2003, 3, 31, 22, 5, 36, 445, DateTimeKind.Unspecified).AddTicks(345), "Elza61@hotmail.com", "Susie", "Weissnat", 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 75,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1973, 9, 1, 6, 50, 54, 80, DateTimeKind.Unspecified).AddTicks(6813), new DateTime(1983, 11, 16, 2, 11, 20, 798, DateTimeKind.Unspecified).AddTicks(9965), "Kathlyn_Adams63@hotmail.com", "Marlee", "Stroman", 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 76,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName" },
                values: new object[] { new DateTime(1993, 2, 5, 12, 49, 3, 956, DateTimeKind.Unspecified).AddTicks(9223), new DateTime(2010, 4, 2, 20, 34, 50, 839, DateTimeKind.Unspecified).AddTicks(1283), "Delores.Prohaska@hotmail.com", "Lila", "Mante" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 77,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1990, 1, 11, 21, 31, 43, 778, DateTimeKind.Unspecified).AddTicks(1166), new DateTime(2000, 1, 21, 14, 11, 5, 587, DateTimeKind.Unspecified).AddTicks(1502), "Rashawn_Conroy@yahoo.com", "Kristin", "Bayer", 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 78,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2013, 7, 6, 14, 1, 42, 515, DateTimeKind.Unspecified).AddTicks(4470), new DateTime(2021, 7, 6, 9, 16, 58, 851, DateTimeKind.Unspecified).AddTicks(225), "Russ29@gmail.com", "Sallie", "Little", 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 79,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(1972, 11, 20, 3, 36, 26, 435, DateTimeKind.Unspecified).AddTicks(7554), new DateTime(2016, 10, 2, 15, 2, 31, 234, DateTimeKind.Unspecified).AddTicks(8366), "Zackary.Grant@hotmail.com", "Addie", "Dach", 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 80,
                columns: new[] { "Birthday", "CreatedAt", "Email", "FirstName", "LastName", "TeamId" },
                values: new object[] { new DateTime(2008, 6, 12, 18, 6, 25, 483, DateTimeKind.Unspecified).AddTicks(9240), new DateTime(2017, 8, 12, 9, 52, 53, 238, DateTimeKind.Unspecified).AddTicks(1866), "Trever_Brakus73@gmail.com", "Lester", "Parker", 9 });
        }
    }
}
