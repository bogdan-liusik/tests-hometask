﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProjectStructure.DAL.Entities;

namespace ProjectStructure.DAL.Context.ModelConfigurations
{
    public class TaskEntityTypeConfiguration : IEntityTypeConfiguration<Task>
    {
        public void Configure(EntityTypeBuilder<Task> builder)
        {
            builder
                .Property(t => t.Name)
                .HasMaxLength(70)
                .IsRequired();
            
            builder
                .Property(t => t.ProjectId)
                .IsRequired();
        }
    }
}