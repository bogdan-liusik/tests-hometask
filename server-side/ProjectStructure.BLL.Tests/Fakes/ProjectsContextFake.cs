﻿using Microsoft.EntityFrameworkCore;
using ProjectStructure.DAL.Context;

namespace ProjectStructure.BLL.Tests.Fakes
{
    public sealed class ProjectsContextFake : ProjectsContext
    {
        public ProjectsContextFake(DbContextOptions<ProjectsContext> options) : base(options)
        {
            Database.EnsureDeleted();
            Database.EnsureCreated();
        }
            
        protected override void OnModelCreating(ModelBuilder modelBuilder) { }
    }
}