﻿using System;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using ProjectStructure.BLL.MappingProfiles;
using ProjectStructure.BLL.Services;
using ProjectStructure.BLL.Tests.Fakes;
using ProjectStructure.Common.DTO.Task;
using ProjectStructure.DAL.Context;
using ProjectStructure.DAL.Entities.Enums;
using Xunit;

namespace ProjectStructure.BLL.Tests.Tests
{
    public class TaskServiceTests
    {
        private readonly IMapper _mapper;
        private readonly DbContextOptions<ProjectsContext> _contextOptions;
        
        private TaskService _taskService;

        public TaskServiceTests()
        {
            _contextOptions = new DbContextOptionsBuilder<ProjectsContext>()
                .UseInMemoryDatabase("TestProjectsDB_TaskService")
                .Options;
            
            _mapper = new Mapper(new MapperConfiguration(configuration =>
            {
                configuration.AddProfile<TaskProfile>();
            }));
        }
        
        [Fact]
        public void UpdateTask_WhenUpdateTaskStateToFinished_ThenUpdatedTaskStateFinished_AndFinishedAtDateTimeNow()
        {
            using var context = new ProjectsContextFake(_contextOptions);
            _taskService = new TaskService(context, _mapper);
            
            var taskCreate = new TaskCreateDTO() 
            { 
                Name = "Nameaaaaa",
                Description = "TaskDescription",
                State = TaskState.InProgress,
                CreatedAt = DateTime.Now.AddMonths(-1)
            };
            
            var createdTask = _taskService.CreateTask(taskCreate);
            
            var taskUpdate = new TaskUpdateDTO()
            {
                Id = createdTask.Id,
                Name = "Nameaaaaa",
                Description = "TaskDescription",
                State = TaskState.Done,
            };
            
            var updatedTask = _taskService.UpdateTask(taskUpdate);
            
            Assert.NotEqual(createdTask.State, updatedTask.State);
            Assert.Equal(TaskState.Done, updatedTask.State);
            Assert.Equal(DateTime.Now.Second, updatedTask.FinishedAt.Second);
        }
        
        [Fact]
        public void UpdateTask_WhenUpdateTaskState_ThanTaskStateDoesNotMatch()
        {
            using var context = new ProjectsContextFake(_contextOptions);
            _taskService = new TaskService(context, _mapper);
            
            var taskCreate = new TaskCreateDTO() 
            { 
                Name = "Nameaaaaa",
                Description = "TaskDescription",
                State = TaskState.ToDo,
                CreatedAt = DateTime.Now.AddMonths(-1)
            };
            
            var createdTask = _taskService.CreateTask(taskCreate);
            
            var taskUpdate = new TaskUpdateDTO()
            {
                Id = createdTask.Id,
                Name = "Nameaaaaa",
                Description = "TaskDescription",
                State = TaskState.InProgress,
            };
            
            var updatedTask = _taskService.UpdateTask(taskUpdate);
            
            Assert.NotEqual(createdTask.State, updatedTask.State);
        }
    }
}