﻿using Xunit;
using System;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using ProjectStructure.BLL.MappingProfiles;
using ProjectStructure.BLL.Services;
using ProjectStructure.BLL.Tests.Fakes;
using ProjectStructure.Common.DTO.Team;
using ProjectStructure.Common.DTO.User;
using ProjectStructure.DAL.Context;

namespace ProjectStructure.BLL.Tests.Tests
{
    public class TeamServiceTests
    {
        private readonly IMapper _mapper;
        private readonly DbContextOptions<ProjectsContext> _contextOptions;

        private TeamService _teamService;
        private UserService _userService;

        public TeamServiceTests()
        {
            _contextOptions = new DbContextOptionsBuilder<ProjectsContext>()
                .UseInMemoryDatabase("TestProjectsDB_TeamService")
                .Options;
            
            _mapper = new Mapper(new MapperConfiguration(configuration =>
            {
                configuration.AddProfile<TeamProfile>();
                configuration.AddProfile<UserProfile>();
            }));
        }

        [Fact]
        public void CreateTeam_WhenAddUsersToTeam_ThenCheckTeamMembers()
        {
            using var context = new ProjectsContextFake(_contextOptions);
            _teamService = new TeamService(context, _mapper);
            _userService = new UserService(context, _mapper);
            
            var team = new TeamCreateDTO()
            {
                Name = "SuperTeam",
                CreatedAt = DateTime.Now
            };

            var createdTeam = _teamService.CreateTeam(team);

            var member1 = new UserCreateDTO()
            {
                FirstName = "Bogdan",
                LastName = "Liusik",
                TeamId = createdTeam.Id,
                Email = "bogdanliusik1@gmail.com",
                BirthDay = DateTime.Parse("31/12/2002"),
                CreatedAt = DateTime.Now,
            };

            var member2 = new UserCreateDTO()
            {
                FirstName = "Yaroslav",
                LastName = "Liusik",
                TeamId = createdTeam.Id,
                Email = "yaroslavliusik1@gmail.com",
                BirthDay = DateTime.Parse("31/12/2006"),
                CreatedAt = DateTime.Now
            };

            var createdMember1 = _userService.CreateUser(member1);
            var createdMember2 = _userService.CreateUser(member2);
            
            Assert.Equal(createdTeam.Id, _userService.GetUserById(createdMember1.Id).TeamId);
            Assert.Equal(createdTeam.Id, _userService.GetUserById(createdMember2.Id).TeamId);
        }
    }
}