﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using ProjectStructure.BLL.Exceptions;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.BLL.Services.Abstract;
using ProjectStructure.Common.DTO.Task;
using ProjectStructure.Common.DTO.User;
using ProjectStructure.DAL.Context;
using ProjectStructure.DAL.Entities;
using ProjectStructure.DAL.Entities.Enums;

namespace ProjectStructure.BLL.Services
{
    public class UserService : BaseService, IUserService
    {
        public UserService(ProjectsContext context, IMapper mapper) : base(context, mapper) { }

        public ICollection<UserDTO> GetAllUsers()
        {
            return GetAllEntities<User>()
                .Select(user => _mapper.Map<UserDTO>(user))
                .ToList();
        }

        public UserDTO GetUserById(int id)
        {
            var userEntity = GetEntityById<User>(id);
            return _mapper.Map<UserDTO>(userEntity);
        }
        
        public ICollection<TaskDTO> GetAllUnfinishedTasks(int id)
        {
            if (!_context.Users.Any(u => u.Id == id))
            {
                throw new NotFoundException(typeof(User).ToString(), id);
            }
            
            var user = _context
                .Users
                .Include(u => u.Tasks)
                .SingleOrDefault(u => u.Id == id);
            
            if (user == null)
            {
                throw new NotFoundException(typeof(User).ToString(), id);
            }
            
            return user.Tasks
                .Where(task => task.PerformerId == id && (task.State == TaskState.InProgress 
                                                          || task.State == TaskState.ToDo
                                                          || task.FinishedAt == null))
                .Select(task => _mapper.Map<TaskDTO>(task))
                .ToList();
        }

        public UserDTO CreateUser(UserCreateDTO userCreateDto)
        {
            var userEntity = _mapper.Map<User>(userCreateDto);
            var createdUser = _context.Users.Add(userEntity).Entity;
            _context.SaveChanges();
            return _mapper.Map<UserDTO>(createdUser);
        }

        public UserDTO UpdateUser(UserUpdateDTO userUpdateDto)
        {
            var userEntity = GetEntityById<User>(userUpdateDto.Id);
            
            userEntity.FirstName = userUpdateDto.FirstName;
            userEntity.LastName = userUpdateDto.LastName;
            userEntity.Email = userUpdateDto.Email;
            
            _context.SaveChanges();
            return _mapper.Map<UserDTO>(userEntity);
        }

        public void DeleteUser(int id)
        {
            var userEntity = GetEntityById<User>(id);
            _context.Users.Remove(userEntity);
            _context.SaveChanges();
        }
    }
}