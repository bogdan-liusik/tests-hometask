﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.BLL.Services.Abstract;
using ProjectStructure.Common.DTO.Project;
using ProjectStructure.DAL.Context;
using ProjectStructure.DAL.Entities;

namespace ProjectStructure.BLL.Services
{
    public class ProjectService : BaseService, IProjectService
    {
        public ProjectService(ProjectsContext contex, IMapper mapper) : base(contex, mapper) { }

        public ICollection<ProjectDTO> GetAllProjects()
        {
            return GetAllEntities<Project>()
                .Select(project => _mapper.Map<ProjectDTO>(project))
                .ToList();
        }

        public ProjectDTO GetProjectById(int id)
        {
            var projectEntity = GetEntityById<Project>(id);
            return _mapper.Map<ProjectDTO>(projectEntity);
        }
        
        public ProjectDTO CreateProject(ProjectCreateDTO projectCreateDto)
        {
            var projectEntity = _mapper.Map<Project>(projectCreateDto);

            var createdProject = _context.Projects.Add(projectEntity).Entity;
            
            _context.SaveChanges();
            
            return _mapper.Map<ProjectDTO>(createdProject);
        }

        public ProjectDTO UpdateProject(ProjectUpdateDTO projectUpdateDto)
        {
            var projectEntity = GetEntityById<Project>(projectUpdateDto.Id);

            projectEntity.Name = projectUpdateDto.Name;
            projectEntity.Description = projectUpdateDto.Description;
            
            _context.SaveChanges();
            
            return _mapper.Map<ProjectDTO>(projectEntity);
        }
        
        public void DeleteProject(int id)
        {
            var entityProject = GetEntityById<Project>(id);
            _context.Projects.Remove(entityProject);
            _context.SaveChanges();
        }
    }
}