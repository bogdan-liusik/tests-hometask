﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using ProjectStructure.BLL.Interfaces;
using ProjectStructure.BLL.Services.Abstract;
using ProjectStructure.Common.DTO.Task;
using ProjectStructure.DAL.Context;
using ProjectStructure.DAL.Entities;
using ProjectStructure.DAL.Entities.Enums;

namespace ProjectStructure.BLL.Services
{
    public class TaskService : BaseService, ITaskService
    {
        public TaskService(ProjectsContext context, IMapper mapper) : base(context, mapper) { }

        public ICollection<TaskDTO> GetAllTasks()
        {
            return GetAllEntities<Task>()
                .Select(task => _mapper.Map<TaskDTO>(task))
                .ToList();
        }

        public TaskDTO GetTaskById(int id)
        {
            var taskEntity = GetEntityById<Task>(id);
            return _mapper.Map<TaskDTO>(taskEntity);
        }

        public TaskDTO CreateTask(TaskCreateDTO taskCreateDto)
        {
            var taskEntity = _mapper.Map<Task>(taskCreateDto);
            var createdTask = _context.Tasks.Add(taskEntity).Entity;
            _context.SaveChanges();
            return _mapper.Map<TaskDTO>(createdTask);
        }

        public TaskDTO UpdateTask(TaskUpdateDTO taskUpdateDto)
        {
            var taskEntity = GetEntityById<Task>(taskUpdateDto.Id);

            taskEntity.Name = taskUpdateDto.Name;
            taskEntity.Description = taskUpdateDto.Description;
            taskEntity.State = taskUpdateDto.State;
            if (taskUpdateDto.State == TaskState.Done)
            {
                taskEntity.FinishedAt = DateTime.Now;
            }

            _context.SaveChanges();
            return _mapper.Map<TaskDTO>(taskEntity);
        }

        public void DeleteTask(int id)
        {
            var taskEntity = GetEntityById<Task>(id);
            _context.Tasks.Remove(taskEntity);
            _context.SaveChanges();
        }
    }
}