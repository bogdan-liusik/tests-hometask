﻿using System.Collections.Generic;
using ProjectStructure.Common.DTO.Task;
using ProjectStructure.Common.DTO.User;

namespace ProjectStructure.BLL.Interfaces
{
    public interface IUserService
    {
        ICollection<UserDTO> GetAllUsers();
        UserDTO GetUserById(int projectId);
        ICollection<TaskDTO> GetAllUnfinishedTasks(int id);
        UserDTO CreateUser(UserCreateDTO project);
        UserDTO UpdateUser(UserUpdateDTO project);
        void DeleteUser(int projectId);
    }
}