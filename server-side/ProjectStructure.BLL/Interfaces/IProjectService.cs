﻿using System.Collections.Generic;
using ProjectStructure.Common.DTO.Project;

namespace ProjectStructure.BLL.Interfaces
{
    public interface IProjectService
    {
        ICollection<ProjectDTO> GetAllProjects();
        ProjectDTO GetProjectById(int projectId);
        ProjectDTO CreateProject(ProjectCreateDTO project);
        ProjectDTO UpdateProject(ProjectUpdateDTO project);
        void DeleteProject(int projectId);
    }
}