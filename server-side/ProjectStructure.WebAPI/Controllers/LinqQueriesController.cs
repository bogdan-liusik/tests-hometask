﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.Services;
using ProjectStructure.Common.DTO.LinqQueryResults;
using ProjectStructure.Common.DTO.Task;

namespace ProjectStructure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LinqQueriesController : ControllerBase
    {
        private readonly LinqQueriesService _linqQueriesService;
        
        public LinqQueriesController(LinqQueriesService linqQueriesService)
        {
            _linqQueriesService = linqQueriesService;
        }

        [HttpGet("query1/{id:int}")]
        public ActionResult<ICollection<Query1StructureDTO>> Query1(int id)
        {
            var result = _linqQueriesService.Query1(id);
            if (result.Count == 0)
            {
                return NoContent();
            }
            return Ok(result);
        }
        
        [HttpGet("query2/{id:int}")]
        public ActionResult<ICollection<TaskDTO>> Query2(int id)
        {
            return Ok(_linqQueriesService.Query2(id));
        }

        [HttpGet("query3/{id:int}")]
        public ActionResult<ICollection<Query3StructureDTO>> Query3(int id)
        {
            return Ok(_linqQueriesService.Query3(id));
        }

        [HttpGet("query4")]
        public ActionResult<ICollection<Query4StructureDTO>> Query4()
        {
            return Ok(_linqQueriesService.Query4());
        }

        [HttpGet("query5")]
        public ActionResult<ICollection<Query5StructureDTO>> Query5()
        {
            return Ok(_linqQueriesService.Query5());
        }

        [HttpGet("query6/{id:int}")]
        public ActionResult<Query6StructureDTO> Query6(int id)
        {
            return Ok(_linqQueriesService.Query6(id));
        }

        [HttpGet("query7")]
        public ActionResult<ICollection<Query7StructureDTO>> Query7()
        {
            return Ok(_linqQueriesService.Query7());
        }
    }
}