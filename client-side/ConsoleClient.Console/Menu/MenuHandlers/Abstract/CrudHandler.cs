﻿using System.Threading.Tasks;
using ConsoleClient.BLL.Services;

namespace ConsoleClient.Console._Menu.MenuHandlers.Abstract
{
    public abstract class CrudHandler
    {
        private protected readonly CrudService CrudService;

        protected CrudHandler(CrudService crudService)
        {
            CrudService = crudService;
        }

        public abstract Task Handle();
    }
}