﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ConsoleClient.BLL.Services;
using ConsoleClient.Console._Menu.MenuHandlers;
using ConsoleClient.Console._Menu.MenuHandlers.Abstract;
using ConsoleClient.Console._Menu.MenuHandlers.CRUD_Handlers.User;
using ConsoleClient.Console._Menu.Enums;

namespace ConsoleClient.Console._Menu
{
    using static MenuHelper;
    
     public class Menu
     {
         private bool _running = true;
         readonly Dictionary<int, BaseHandler<LinqQueriesService>> _linqHandlers;
         readonly Dictionary<int, BaseHandler<CrudService>> _crudHandlers;
    
         public Menu(LinqQueriesService linqQueriesService, CrudService crudService)
         {
             _linqHandlers = new Dictionary<int, BaseHandler<LinqQueriesService>>()
             {
                 {1, new Query1Handler(linqQueriesService)},
                 {2, new Query2Handler(linqQueriesService)},
                 {3, new Query3Handler(linqQueriesService)},
                 {4, new Query4Handler(linqQueriesService)},
                 {5, new Query5Handler(linqQueriesService)},
                 {6, new Query6Handler(linqQueriesService)},
                 {7, new Query7Handler(linqQueriesService)},
             };

             _crudHandlers = new Dictionary<int, BaseHandler<CrudService>>()
             {
                 {1, new CreateUserHandler(crudService)},
                 {2, new UpdateUserHandler(crudService)},
                 {3, new GetUserHandler(crudService)},
                 {4, new DeleteUserHandler(crudService)}
             };
         }
         
         public void Start(MenuChoise choise)
         {
             while (_running)
             {
                 if (choise == MenuChoise.CrudService)
                 {
                     ShowCrudOperationCommands();
                     ChooseCommand(_crudHandlers).Wait();
                 }
                 else
                 {
                     ShowLinqQueriesCommands();
                     ChooseCommand(_linqHandlers).Wait();
                 }
             }
         }

         private void ShowCrudOperationCommands()
         {
             WriteColorLine("┌" + new string('─', 13) + "CRUD SERVICE" + new string('─', 11) + "┐", ConsoleColor.Green);
             System.Console.ForegroundColor = ConsoleColor.Cyan;
             System.Console.WriteLine("| 1. Create user.                    |");
             System.Console.WriteLine("| 2. Update user.                    |");
             System.Console.WriteLine("| 3. Get user by id.                 |");
             System.Console.WriteLine("| 4. Delete user.                    |");
             WriteColorLine("| 0. Exit                            |", ConsoleColor.Red);
             WriteColorLine("└" + new string('─', 36) + "┘", ConsoleColor.Green);
         }
            
         private void ShowLinqQueriesCommands()
         {
             WriteColorLine("┌" + new string('─', 13) + "QUERY SERVICE MENU" + new string('─', 11) + "┐", ConsoleColor.Green);
             System.Console.ForegroundColor = ConsoleColor.Cyan;
             System.Console.WriteLine("| 1. Result of Query 1                     |");
             System.Console.WriteLine("| 2. Result of Query 2                     |");
             System.Console.WriteLine("| 3. Result of Query 3                     |");
             System.Console.WriteLine("| 4. Result of Query 4                     |");
             System.Console.WriteLine("| 5. Result of Query 5                     |");
             System.Console.WriteLine("| 6. Result of Query 6                     |");
             System.Console.WriteLine("| 7. Result of Query 7                     |");
             WriteColorLine("| 0. Exit                                  |", ConsoleColor.Red);
             WriteColorLine("└" + new string('─', 42) + "┘", ConsoleColor.Green);
         }
    
         private async Task ChooseCommand<TService>(Dictionary<int, BaseHandler<TService>> handlers)
         {
             try
             {
                 System.Console.Write("Enter your choice: ");
                 
                 if(Int32.TryParse(System.Console.ReadLine(), out var menuItem))
                 {
                     if (menuItem <= handlers.Count && menuItem > 0)
                     {
                         await handlers[menuItem].Handle();
                     }
                     else if(menuItem == 0)
                     {
                         _running = false;
                     }
                     else
                     {
                         throw new InvalidOperationException($"Item number must be in range 1 - {handlers.Count}! Try Again.");
                     }
                 }
                 else
                 {
                     throw new InvalidOperationException("Invalid character! Try Again.");
                 }
             }
             catch (InvalidOperationException ex)
             {
                 WriteErrorLine(ex.Message);
             }
    
             WriteColorLine("\n" + new string('─', 81) + "\n", ConsoleColor.DarkGray);
         }
    }
}