﻿using System;

namespace ConsoleClient.Console._Menu
{
    public static class MenuHelper
    {
        public static void WriteColorLine(string @string, ConsoleColor color)
        {
            PrintColorLine(@string, color);
        }

        public static void WriteSuccessLine(string @string)
        {
            PrintColorLine(@string, ConsoleColor.Green);
        }

        public static void WriteErrorLine(string @string)
        {
            PrintColorLine(@string, ConsoleColor.Red);
        }

        public static void WriteEnterAnyKeyToContinue()
        {
            System.Console.WriteLine("Please, enter any key to continue...");
            System.Console.ReadKey();
        }
        
        private static void PrintColorLine(string @string, ConsoleColor color)
        {
            System.Console.ForegroundColor = color;
            System.Console.WriteLine(@string);
            System.Console.ResetColor();
        }
    }
}