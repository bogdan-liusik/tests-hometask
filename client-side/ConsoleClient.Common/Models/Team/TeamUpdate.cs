﻿namespace ConsoleClient.Common.Models.Team
{
    public class TeamUpdate
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}