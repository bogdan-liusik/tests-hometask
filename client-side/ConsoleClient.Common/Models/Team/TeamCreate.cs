﻿using System;

namespace ConsoleClient.Common.Models.Team
{
    public class TeamCreate
    {
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; } = DateTime.Now;
    }
}