﻿using System;

namespace ConsoleClient.Common.Models.User
{
    public class UserModel
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public int? TeamId { get; set; }
        public DateTime Birthday { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}